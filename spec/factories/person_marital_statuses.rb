# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :person_marital_status do
    association           :person
    association           :marital_status_type
  end
end

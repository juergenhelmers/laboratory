# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :telecommunications_number do
    contact_number  Faker::Number.number(8)
    country_code    (1..1876).to_a.sample             # generates a random number between 1 (USA) and 1876 (Jamaica)
    area_code       (100..999).to_a.sample            # generates a random 3 digit number between 100 and 999
  end
end

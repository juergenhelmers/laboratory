# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :postal_address do
    address1  Faker::Address.street_address
    address2  Faker::Address.secondary_address
    address3  Faker::Address.street_suffix
    longitude -0.075322
    laditude  51.505625599999
    association :postal_address_city_boundary
    association :postal_address_country_boundary
  end
end

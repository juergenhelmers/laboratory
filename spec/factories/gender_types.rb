# Read about factories at https://github.com/thoughtbot/factory_girl

require 'faker'

FactoryGirl.define do
  factory :gender_type do
    sequence(:name) { |n| "#{Faker::Name.name}"+"#{n}" }
    description   Faker::Lorem.sentence
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :child_family_relationship do
    child_role_id 1
    family_role_id 1
    from_date "2013-10-02"
    thru_date "2013-10-02"
  end
end

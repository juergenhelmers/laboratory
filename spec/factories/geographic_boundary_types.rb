# Read about factories at https://github.com/thoughtbot/factory_girl

require 'faker'

FactoryGirl.define do
  factory :geographic_boundary_type do
    sequence(:name) {|n| "#{Faker::Name.name}_#{n}" }
    description Faker::Lorem.sentence
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :doctor_role do
    medical_id    Faker::Lorem.sentence
    association   :person
  end
end

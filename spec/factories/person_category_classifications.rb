# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :person_category_classification do
    person_id 1
    category_id 1
  end
end

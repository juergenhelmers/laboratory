# Read about factories at https://github.com/thoughtbot/factory_girl

require 'faker'

FactoryGirl.define do
  factory :category do
    name              Faker::Lorem.sentence(2)
    description       Faker::Lorem.sentence
    association       :category_type
  end
end

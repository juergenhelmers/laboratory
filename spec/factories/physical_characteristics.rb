# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :physical_characteristic do
    association                     :patient_role
    association                     :physical_characteristic_type
    association                     :unit_of_measure
    from_date                       (Time.now.to_date - 5.days)
    thru_date                       Time.now.to_date
    #value                           1
    #person_id                       1
  end
end

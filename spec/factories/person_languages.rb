# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :person_language do
    association       :person
    association       :language
    from_date         Time.now.to_date
    competency_level  %w{ beginner basic good fluent perfect native }.sample
  end
end

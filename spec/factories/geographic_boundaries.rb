# Read about factories at https://github.com/thoughtbot/factory_girl

require 'faker'

FactoryGirl.define do
  factory :geographic_boundary do
    name          Faker::Lorem.sentence
    geo_code      "#{Faker::Address.latitude},#{Faker::Address.longitude}"
    abbreviation  Faker::Lorem.word
    association   :geographic_boundary_type
  end

  factory :geographic_boundary_city, class: "GeographicBoundary" do
    name          Faker::Lorem.sentence
    geo_code      "#{Faker::Address.latitude},#{Faker::Address.longitude}"
    abbreviation  Faker::Lorem.word
    association   :geographic_boundary_type
  end
end

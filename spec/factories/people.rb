# Read about factories at https://github.com/thoughtbot/factory_girl

require 'faker'

FactoryGirl.define do
  factory :person do
    current_first_name      Faker::Name.first_name
    current_middle_name     Faker::Name.first_name
    current_last_name       Faker::Name.last_name
    current_personal_title  Faker::Name.prefix
    current_suffix          Faker::Name.suffix
    current_nickname        Faker::Lorem.word
    birth_date              "2013-05-30"
    citizen_number          Faker::PhoneNumber.phone_number
    comment                 Faker::Lorem.sentence
    ethnicity               %w{ white coloured asian black }.sample
    passport_nationality    Faker::Address.country
    mothers_maiden_name     Faker::Name.last_name

    association             :gender_type
  end
end

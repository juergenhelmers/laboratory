# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :doctor_contact_relationship do
    association :contact_role
    association :doctor_role
    from_date   Time.now.to_date

  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :postal_address_suburb_boundary do
    association :postal_address
    association :geographic_boundary
  end
end

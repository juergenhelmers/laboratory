# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :marital_status_type do
    name          %w{ single married divorced widowed }.sample
    description   Faker::Lorem.sentence
  end
end

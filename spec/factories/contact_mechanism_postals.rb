# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact_mechanism_postal do
    association :contact_mechanism_type
    association :postal_address
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :category_type do
    name            Faker::Lorem.sentence
    description     Faker::Lorem.sentence
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :patient_genetic_counselor_relationship do
    association :patient_role
    association :genetic_counselor_role
    from_date   Time.now.to_date

  end
end
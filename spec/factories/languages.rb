# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :language do
    name          Faker::Name.first_name
    abbreviation  { "#{name}"[0..1].upcase }
    description   Faker::Lorem.sentence
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :parent_child_relationship do
    association :parent_role
    association :child_role
    from_date   Time.now.to_date

  end
end

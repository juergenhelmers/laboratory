# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :family_role do
    association :organization
  end
end

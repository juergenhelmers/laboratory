# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :geographic_boundary_association do
    parent_id 1
    child_id 1
    association :geographic_boundary_association_type
  end
end

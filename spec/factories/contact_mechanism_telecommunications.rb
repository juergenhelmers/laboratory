# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact_mechanism_telecommunication do
    association :contact_mechanism_type
    association :telecommunications_number
  end
end

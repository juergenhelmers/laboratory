# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :child_role do
    association :person
  end
end

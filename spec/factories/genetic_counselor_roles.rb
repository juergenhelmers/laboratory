# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :genetic_counselor_role do
    association :person
  end
end

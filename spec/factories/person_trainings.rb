# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :person_training do
    association :person
    association :training_class_type
    from_date Time.now.strftime("%Y/%m/%d")
  end
end

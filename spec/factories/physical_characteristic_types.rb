# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :physical_characteristic_type do
    name        Faker::Lorem.sentence
    description Faker::Lorem.sentence(2)
  end
end

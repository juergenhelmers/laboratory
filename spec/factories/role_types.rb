# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :role_type do
    name              Faker::Lorem.sentence
    description       Faker::Lorem.sentence
    parent_id         %w{ 1 2 }.sample
  end

end

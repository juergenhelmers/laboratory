# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :practice_role do
    association :organization
  end
end

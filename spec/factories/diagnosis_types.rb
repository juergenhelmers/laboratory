# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :diagnosis_type do
    name        Faker::Lorem.sentence
    description Faker::Lorem.paragraph
    code        Faker::Address.zip
  end
end

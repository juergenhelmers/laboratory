# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :application_user_role do
    name        Faker::Lorem.sentence
    description Faker::Lorem.sentence
  end
end

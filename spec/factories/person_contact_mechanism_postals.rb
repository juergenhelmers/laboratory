# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :person_contact_mechanism_postal do
    association           :person
    association           :contact_mechanism_postal
    association           :role_type
    thru_date             Time.now.to_date
    non_solicitation_ind  false
    comment               Faker::Lorem.paragraph
  end
end

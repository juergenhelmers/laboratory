# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :organization do
    current_name  Faker::Lorem.sentence
  end
end

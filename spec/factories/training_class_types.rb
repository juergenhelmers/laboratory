# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :training_class_type do
    name          Faker::Name.first_name
    description   Faker::Lorem.sentence
  end
end

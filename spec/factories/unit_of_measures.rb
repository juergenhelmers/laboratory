# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :unit_of_measure do
    name          Faker::Lorem.sentence
    description   Faker::Lorem.sentence(2)
    abbreviation  Faker::Lorem.word
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

require 'faker'

FactoryGirl.define do
  factory :user do
    firstname               Faker::Name.first_name
    lastname                Faker::Name.last_name
    email                   Faker::Internet.email
    company_id              "123123123"
    password                "secret123"
    password_confirmation   "secret123"
    date_of_birth           (Time.now.to_date - 20.years)
    association             :application_user_role
  end
end

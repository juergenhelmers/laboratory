# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :person_contact_mechanism_telecommunication do
    association           :person
    association           :contact_mechanism_telecommunication
    association           :role_type
    thru_date             Time.now.to_date
    non_solicitation_ind  false
    extension             Faker::Number.number(4)
    comment               Faker::Lorem.paragraph
  end
end

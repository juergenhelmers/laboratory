# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact_mechanism_electronic_address do
    association :contact_mechanism_type
    association :electronic_address
  end
end

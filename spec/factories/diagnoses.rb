# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :diagnosis do
    association             :patient_role
    association             :diagnosis_type
    diagnosis_date          Time.now.to_date
    health_care_episode_id  1
  end
end

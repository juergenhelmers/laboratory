# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :doctor_practice_relationship do
    association :doctor_role
    association :practice_role
    from_date Time.now.to_date
  end
end

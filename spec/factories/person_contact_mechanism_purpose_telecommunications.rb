# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :person_contact_mechanism_purpose_telecommunication do
    association :person_contact_mechanism_telecommunication
    association :contact_mechanism_purpose_type
    from_date   (Time.now.to_date - 6.weeks)
    thru_date   Time.now.to_date
  end
end

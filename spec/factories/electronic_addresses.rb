# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'
FactoryGirl.define do
  factory :electronic_address do
    electronic_address_string Faker::Internet.safe_email
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :person_status do
    association         :person
    association         :person_status_type
    person_status_date  "#{Time.now.to_date}"
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :patient_doctor_relationship do
    association :patient_role
    association :doctor_role
    from_date   Time.now.to_date

  end
end
require 'spec_helper'

describe "DoctorRoles" do
  describe "GET /doctor_roles" do
    it "does return code 200 when accessing the index page" do
      visit(doctor_roles_path)
      expect(page.status_code).to be(200)
    end

    it "lists the doctor_role name on index page" do
      doctor_role = create(:doctor_role)
      visit(doctor_roles_path)
      expect(page).to have_content(doctor_role.medical_id)
    end

  end

  describe "GET /doctor_role/new" do
    it "can create a new doctor_role record" do
      visit(doctor_roles_path)
      click_link("New #{I18n.t('doctor_role.model_name')}")
      fill_in I18n.t('doctor_role.medical_id'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Doctor Role was successfully created")
      }
      expect(page).to have_content("MyString")
    end

    it "cannot create a new doctor_role record without a medical_id" do
      visit(doctor_roles_path)
      click_link("New #{I18n.t('doctor_role.model_name')}")
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /doctor_role/edit" do
    it "can update an existing doctor_role" do
      doctor_role = create(:doctor_role)
      visit(edit_doctor_role_path(doctor_role))
      fill_in I18n.t('doctor_role.medical_id'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Doctor Role was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /doctor_role/:id" do
    it "can display the details of a doctor_role" do
      doctor_role = create(:doctor_role)
      visit(doctor_role_path(doctor_role))
      expect(page).to have_content(doctor_role.medical_id)
    end

    it "Deletes a doctor_role" do
      DatabaseCleaner.clean
      doctor_role = create(:doctor_role)
      visit(doctor_roles_path)
      expect{
        within "tr#doctor_role_#{doctor_role.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(DoctorRole,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Doctor Role was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('doctor_role.index_heading')}"
      expect(page).to_not have_content doctor_role.medical_id
    end

  end

end

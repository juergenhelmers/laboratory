require 'spec_helper'

describe "People" do
  before(:all) do
    RoleType.destroy_all
    create(:role_type, name: I18n.t('role_type.person_role'), parent_id: nil)
    create(:role_type, name: I18n.t('role_type.organization_role'), parent_id: nil)
    create(:role_type)
    sleep 2
  end

  describe "no javascript" do
    describe "GET /people" do
      it "does return code 200 when accessing the index page" do
        visit(people_path)
        expect(page.status_code).to be(200)
      end

      it "lists the person name on index page" do
        person = create(:person)
        visit(people_path)
        expect(page).to have_content(person.current_full_name)
      end

    end

    describe "GET /person/new" do
      it "can create a new person record" do
        gt = create(:gender_type)
        visit(people_path)
        click_link("New #{I18n.t('person.model_name')}")
        fill_in I18n.t('person.current_first_name'), with: "John"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content("John")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
      end

      it "cannot create a new person record without a name" do
        visit(people_path)
        click_link("New #{I18n.t('person.model_name')}")
        click_button I18n.t('actions.save')
        within('.alert.alert-error') do
          expect(page).to have_content("Please review the problems below:")
        end
      end
    end


    describe "GET /person/edit" do
      it "can update an existing person" do
        person = create(:person)
        visit(edit_person_path(person))
        fill_in I18n.t('person.current_first_name'), with: "Klaus"
        click_button I18n.t('actions.save')
        expect(page).to have_content("Klaus")
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully updated.")
        }
      end
    end

    describe "GET /person/:id" do
      it "can display the details of a person" do
        person = create(:person)
        visit(person_path(person))
        expect(page).to have_content(person.current_first_name)
        expect(page).to have_content(person.current_last_name)
        expect(page).to have_content(person.birth_date)
      end

      it "Deletes a person" do
        Person.destroy_all
        person = create(:person)
        visit(people_path)
        expect{
          within "tr#person_#{person.id}" do
            click_link I18n.t('actions.destroy')
          end
        }.to change(Person,:count).by(-1)
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully deleted.")
        }
        expect(page).to have_content "#{I18n.t('person.index_heading')}"
        expect(page).to_not have_content person.current_first_name
        expect(page).to_not have_content person.current_last_name
      end

    end
  end

  describe "dynamic addition of nested resources", js: true do

    describe "Nurse role" do
      it "can create a person with a nurse role" do
        gt = create(:gender_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_nurse_role')
        within('div.person_nurse_role_medical_id') do
          first('input').set('REGID213123')
        end
        fill_in I18n.t('person.current_first_name'), with: "Jane"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content("Jane")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content("REGID213123")
      end
    end
    describe "Doctor Role" do
      it "can create a person with a doctor role" do
        gt = create(:gender_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_doctor_role')
        within('div.person_doctor_role_medical_id') do
          first('input').set('REGID2asdas3')
        end
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content("REGID2asdas3")
      end

      it "can create a person with a doctor role and a contact added" do
        gt = create(:gender_type)
        contact = create(:contact_role)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_doctor_role')
        within('div.person_doctor_role_medical_id') do
          first('input').set('REGID2asdas3')
        end
        click_link I18n.t('doctor_role.link_label.add_contact')
        within('.js-contact_contact_relationship'){
          first('select').find(:option, contact.person.current_full_name).select_option
        }
        within('.js-datepicker.from_date') {
          first('input').set("#{Time.now.to_date - 2.years}")
        }
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content(contact.person.current_full_name)
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content("REGID2asdas3")
      end
    end
    describe "Genetic Counselor Role" do
      it "can create a person with a genetic_counselor role" do
        gt = create(:gender_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_genetic_counselor_role')
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('genetic_counselor_role.is_genetic_counselor'))
      end
    end
    describe "Child Role" do
      it "can create a person with a child role" do
        gt = create(:gender_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_child_role')
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('child_role.is_child'))
      end

      it "can create a person with a child role and a parent" do
        gt = create(:gender_type)
        parent = create(:parent_role)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_child_role')
        click_link I18n.t('child_role.link_label.add_parent')
        within('.js-parent_child_relationship'){
          first('select').find(:option, parent.person.current_full_name).select_option
        }
        within('.js-datepicker.from_date') {
          first('input').set("#{Time.now.to_date - 2.years}")
        }
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content(parent.person.current_full_name)
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('child_role.is_child'))
      end
    end
    describe "Parent Role" do
      it "can create a person with a parent role" do
        gt = create(:gender_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_parent_role')
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('parent_role.is_parent'))
      end
    end
    describe "Contact Role" do
      it "can create a person with a contact role" do
        gt = create(:gender_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_contact_role')
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('contact_role.is_contact'))
      end
    end
    describe "Payor Role" do
      it "can create a person with a payor role" do
        gt = create(:gender_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_payor_role')
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('payor_role.is_payor'))
      end
    end
    describe "Customer Role" do
      it "can create a person with a customer role" do
        gt = create(:gender_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_customer_role')
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('customer_role.is_customer'))
      end
    end
    describe "Employee Role" do
      it "can create a person with a employee role" do
        gt = create(:gender_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_employee_role')
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('employee_role.is_employee'))
      end
    end
    describe "Patient Role" do
      it "can create a person with a patient role with a doctor associated" do
        gt = create(:gender_type)
        doctor = create(:doctor_role)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_patient_role')
        within('.links.patient_doctor_relationship'){
          click_link I18n.t('patient_role.link_label.add_doctor')
        }
        within('.js-patient_doctor_relationship'){
          first('select').find(:option, doctor.person.current_full_name).select_option
        }
        within('.js-datepicker.from_date'){
          first('input').set("#{Time.now.to_date}")
        }
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content(doctor.person.current_full_name)
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('patient_role.is_patient'))
      end

      # TODO clashed with tests for patient_nurse_relationship...
      #it "cannot create a person with a patient role but without a doctor associated" do
      #  gt = create(:gender_type)
      #  doctor = create(:doctor_role)
      #  visit(new_person_path)
      #  click_link I18n.t('person.link_label.add_patient_role')
      #  fill_in I18n.t('person.current_first_name'), with: "Mary"
      #  fill_in I18n.t('person.current_last_name'), with: "Doe"
      #  fill_in I18n.t('person.birth_date'), with: Time.now.to_date
      #  choose gt.name
      #  click_button I18n.t('actions.save')
      #  within('div.alert.alert-error') {
      #    expect(page).to have_content("Please review the problems below:")
      #  }
      #end
      it "can create a person with a patient role with a doctor and nurse associated" do
        gt = create(:gender_type)
        doctor = create(:doctor_role)
        nurse = create(:nurse_role)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_patient_role')
        within('.links.patient_doctor_relationship'){
          click_link I18n.t('patient_role.link_label.add_doctor')
        }
        within('.js-patient_doctor_relationship'){
          first('select').find(:option, doctor.person.current_full_name).select_option
        }
        within('.js-datepicker.from_date'){
          first('input').set("#{Time.now.to_date}")
        }
        within('.links.patient_nurse_relationship'){
          click_link I18n.t('patient_role.link_label.add_nurse')
        }
        within('.js-patient_nurse_relationship'){
          first('select').find(:option, doctor.person.current_full_name).select_option
        }
        within('.js-datepicker.nurse.from_date'){
          first('input').set("#{Time.now.to_date}")
        }
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content(doctor.person.current_full_name)
        expect(page).to have_content(nurse.person.current_full_name)
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('patient_role.is_patient'))
      end
      it "can create a person with a patient role and a diagnosis" do
        gt = create(:gender_type)
        diagnosis_type = create(:diagnosis_type, name: "Tuberculosis")
        doctor = create(:doctor_role)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_patient_role')
        click_link I18n.t('patient_role.link_label.add_diagnosis')
        within('.links.patient_doctor_relationship'){
          click_link I18n.t('patient_role.link_label.add_doctor')
        }
        within('.js-patient_doctor_relationship'){
          first('select').find(:option, doctor.person.current_full_name).select_option
        }
        within('.js-datepicker.from_date'){
          first('input').set("#{Time.now.to_date}")
        }
        within(first('.js-diagnosis_type')) do
          find('select').find(:option, diagnosis_type.name).select_option
        end

        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content(doctor.person.current_full_name)
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('patient_role.is_patient'))
        expect(page).to have_content(diagnosis_type.name)
      end

      it "can create a person with a patient role and a physical characteristic" do
        gt = create(:gender_type)
        pct = create(:physical_characteristic_type, name: "Height")
        doctor = create(:doctor_role)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_patient_role')
        within('.links.patient_doctor_relationship'){
          click_link I18n.t('patient_role.link_label.add_doctor')
        }
        within('.js-patient_doctor_relationship'){
          first('select').find(:option, doctor.person.current_full_name).select_option
        }
        within('.js-datepicker.from_date'){
          first('input').set("#{Time.now.to_date}")
        }
        click_link I18n.t('patient_role.link_label.add_physical_characteristic_type')
        within(first('.js-physical_characteristic_type')) do
          find('select').find(:option, pct.name).select_option
        end

        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content(doctor.person.current_full_name)
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('patient_role.is_patient'))
        expect(page).to have_content(pct.name)
      end

      it "can create a person with a patient role and a physical characteristic with a Unit of Measure" do
        gt = create(:gender_type)
        pct = create(:physical_characteristic_type, name: "Weight")
        uom = create(:unit_of_measure, abbreviation: "kg", name: "kilogram")
        doctor = create(:doctor_role)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_patient_role')
        within('.links.patient_doctor_relationship'){
          click_link I18n.t('patient_role.link_label.add_doctor')
        }
        within('.js-patient_doctor_relationship'){
          first('select').find(:option, doctor.person.current_full_name).select_option
        }
        within('.js-datepicker.from_date'){
          first('input').set("#{Time.now.to_date}")
        }
        click_link I18n.t('patient_role.link_label.add_physical_characteristic_type')
        within(first('.js-physical_characteristic_type')) do
          find('select').find(:option, pct.name).select_option
        end
        within(first('.js-physical_characteristic_value')) do
          find('input').set('65')
        end
        within(first('.js-unit_of_measure')) do
          find('select').find(:option, uom.name).select_option
        end

        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created.")
        }
        expect(page).to have_content(doctor.person.current_full_name)
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
        expect(page).to have_content(I18n.t('patient_role.is_patient'))
        expect(page).to have_content("#{pct.name} - 65 #{uom.abbreviation}")

      end
    end
    # TODO fix spec with tokeninput fields
    # describe "Patient with Postal Address" do
    #  before(:each) do
    #    @city = create(:geographic_boundary_type, name: 'City')
    #    @country = create(:geographic_boundary_type, name: 'Country')
    #    @postal_code = create(:geographic_boundary_type, name: 'Postal Code')
    #    @suburb = create(:geographic_boundary_type, name: 'Suburb')
    #  end
    #  it "can add a person with a postal address" do
    #    gt = create(:gender_type)
    #    za = create(:geographic_boundary, name: "South Africa", abbreviation: "ZA", geographic_boundary_type_id: @country.id)
    #    pc = create(:geographic_boundary, name: "0081", abbreviation: "0081", geographic_boundary_type_id: @postal_code.id)
    #    fg = create(:geographic_boundary, name: "Faerie Glen", geographic_boundary_type_id: @suburb.id)
    #    pta = create(:geographic_boundary, name: "Pretoria", geographic_boundary_type_id: @city.id)
    #    patient = create(:role_type, name: "Patient", parent_id: 1)
    #    visit(new_person_path)
    #    click_link I18n.t('person.link_label.add_postal_address')
    #    within('.js-role_type'){
    #      first('select').find(:option, patient.name).select_option
    #    }
    #    within(first('.address1')) do
    #      find('input').set("123 Test Street")
    #    end
    #    within(first('.js-postal_country')) do
    #      find('input').set(za.id)
    #    end
    #    within(first('.js-postal_city')) do
    #      find('input').set(pta.id)
    #    end
    #    within(first('.js-postal_code')) do
    #      token_input 'input', with: '0081', class: 'postal_code'
    #    end
    #    within(first('.js-postal_suburb')) do
    #      find('input').set(fg.id)
    #    end
    #    fill_in I18n.t('person.current_first_name'), with: "Mary"
    #    fill_in I18n.t('person.current_last_name'), with: "Doe"
    #    fill_in I18n.t('person.birth_date'), with: Time.now.to_date
    #    choose gt.name
    #    click_button I18n.t('actions.save')
    #    within('div.alert.alert-success') {
    #      expect(page).to have_content("Person was successfully created")
    #    }
    #    expect(page).to have_content("123 Test Street")
    #    expect(page).to have_content(za.name)
    #    expect(page).to have_content(pc.name)
    #    expect(page).to have_content(fg.name)
    #    expect(page).to have_content(pta.name)
    #    expect(page).to have_content("Mary")
    #    expect(page).to have_content("Doe")
    #    expect(page).to have_content("#{Time.now.to_date}")
    #    expect(page).to have_content(gt.name)
    #  end
    #end
    describe "person with a telecommunications_number" do
      it "should create a person with a phone number" do
        gt = create(:gender_type)
        patient = create(:role_type, name: "Patient", parent_id: 1)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_telecommunications_number')
        within('.js-role_type'){
          first('select').find(:option, patient.name).select_option
        }
        within(first('.js-country_code')) do
          find('input').set("49")
        end
        within(first('.js-area_code')) do
          find('input').set("030")
        end
        within(first('.js-contact_number')) do
          find('input').set("37301306")
        end
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created")
        }
        expect(page).to have_content("+49(0)30 37301306")
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
      end
    end

    describe "person with an electronic address" do
      it "should create a person with an email address" do
        gt = create(:gender_type)
        patient = create(:role_type, name: "Patient", parent_id: 1)
        cmt_email = create(:contact_mechanism_type, name: "Email")
        cmt_phone = create(:contact_mechanism_type, name: "Phone")
        cmt_postal = create(:contact_mechanism_type, name: "Postal Address")
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_electronic_address')
        within('.js-role_type'){
          first('select').find(:option, patient.name).select_option
        }
        within('.js-contact_mechanism_type'){
          first('select').find(:option, cmt_email.name).select_option
        }
        within(first('.js-electronic_address_string')) do
          find('input').set("test@test.com")
        end
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created")
        }
        expect(page).to have_content("test@test.com")
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
        expect(page).to have_content(gt.name)
      end
    end
    describe "person_statuses" do
      it "can create a person with a person_status" do
        gt = create(:gender_type)
        person_status_type = create(:person_status_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_person_status')
        within(first('.js-person_status_type')) do
          find('select').find(:option, person_status_type.name).select_option
        end
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Person was successfully created")
        }
        expect(page).to have_content(person_status_type.name)
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
      end
    end
    describe "person_category_classifications" do
       it "can create a person with a category" do
         gt = create(:gender_type)
         category = create(:category)
         visit(new_person_path)
         click_link I18n.t('person.link_label.add_category')
         within(first('.js-category')) do
           find('select').find(:option, category.name).select_option
         end
         fill_in I18n.t('person.current_first_name'), with: "Mary"
         fill_in I18n.t('person.current_last_name'), with: "Doe"
         fill_in I18n.t('person.birth_date'), with: Time.now.to_date
         choose gt.name
         click_button I18n.t('actions.save')
         within('div.alert.alert-success') {
           expect(page).to have_content("Person was successfully created")
         }
         expect(page).to have_content(category.name)
         expect(page).to have_content("Mary")
         expect(page).to have_content("Doe")
         expect(page).to have_content("#{Time.now.to_date}")
       end
     end
    describe "person_marital_statuses" do
       it "can create a new party with a marital status" do
         gt = create(:gender_type)
         marital_status = create(:marital_status_type)
         visit(new_person_path)
         click_link I18n.t('person.link_label.add_marital_status')
         within(first('.js-marital_status_type')) do
           find('select').find(:option, marital_status.name).select_option
         end
         fill_in I18n.t('person.current_first_name'), with: "Mary"
         fill_in I18n.t('person.current_last_name'), with: "Doe"
         fill_in I18n.t('person.birth_date'), with: Time.now.to_date
         choose gt.name
         click_button I18n.t('actions.save')
         within('div.alert.alert-success') {
           expect(page).to have_content("Person was successfully created")
         }
         expect(page).to have_content(marital_status.name)
         expect(page).to have_content("Mary")
         expect(page).to have_content("Doe")
         expect(page).to have_content("#{Time.now.to_date}")
      end
    end
    describe "person_languages" do
       it "can create a new party with a language" do
         gt = create(:gender_type)
         language = create(:language)
         visit(new_person_path)
         click_link I18n.t('person.link_label.add_language')
         within(first('.js-language')) do
           find('select').find(:option, language.name).select_option
         end
         fill_in I18n.t('person.current_first_name'), with: "Mary"
         fill_in I18n.t('person.current_last_name'), with: "Doe"
         fill_in I18n.t('person.birth_date'), with: Time.now.to_date
         choose gt.name
         click_button I18n.t('actions.save')
         within('div.alert.alert-success') {
           expect(page).to have_content("Person was successfully created")
         }
         expect(page).to have_content(language.name)
         expect(page).to have_content("Mary")
         expect(page).to have_content("Doe")
         expect(page).to have_content("#{Time.now.to_date}")
      end
    end
    describe "person_trainings" do
      it "can create a new party with a training" do
        gt = create(:gender_type)
        training = create(:training_class_type)
        visit(new_person_path)
        click_link I18n.t('person.link_label.add_training')
        within(first('.js-training_class_type')) do
          find('select').find(:option, training.name).select_option
        end
        within(first('.js-training_from_date')) do
          fill_in "Start Date", with: "2013-06-11"
        end
        fill_in I18n.t('person.current_first_name'), with: "Mary"
        fill_in I18n.t('person.current_last_name'), with: "Doe"
        fill_in I18n.t('person.birth_date'), with: Time.now.to_date
        choose gt.name
        click_button I18n.t('actions.save')
        #within('div.alert.alert-success') {
        #  expect(page).to have_content("Person was successfully created")
        #}
        expect(page).to have_content(training.name)
        expect(page).to have_content("2013-06-11")
        expect(page).to have_content("Mary")
        expect(page).to have_content("Doe")
        expect(page).to have_content("#{Time.now.to_date}")
      end
    end
  end
end

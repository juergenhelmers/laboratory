require 'spec_helper'

describe "ContactMechanismPurposeTypes" do
  describe "GET /contact_mechanism_purpose_types" do
    
    it "returns OK when accessing the index page" do
      visit(contact_mechanism_purpose_types_path)
      expect(page.status_code).to be(200)
    end

    it "lists the contact_mechanism_purpose_type name on index page" do
      contact_mechanism_purpose_type = create(:contact_mechanism_purpose_type)
      visit(contact_mechanism_purpose_types_path)
      expect(page).to have_content(contact_mechanism_purpose_type.name)
    end

  end

  describe "GET /contact_mechanism_purpose_type/new" do
    it "can create a new contact_mechanism_purpose_type record" do
      visit(contact_mechanism_purpose_types_path)
      click_link("New #{I18n.t('contact_mechanism_purpose_type.model_name')}")
      fill_in I18n.t('contact_mechanism_type.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Contact Mechanism Purpose Type was successfully created")
      }
      expect(page).to have_content("MyString")
    end

    it "cannot create a new contact_mechanism_purpose_type record without a name" do
      visit(contact_mechanism_purpose_types_path)
      click_link("New #{I18n.t('contact_mechanism_purpose_type.model_name')}")
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /contact_mechanism_purpose_type/edit" do
    it "can update an existing contact_mechanism_purpose_type" do
      contact_mechanism_purpose_type = create(:contact_mechanism_purpose_type)

      visit(edit_contact_mechanism_purpose_type_path(contact_mechanism_purpose_type))
      fill_in I18n.t('contact_mechanism_type.name'), with: "MyString"
      click_button 'Save'
      within('div.alert.alert-success') {
        expect(page).to have_content("Contact Mechanism Purpose Type was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end
  describe "GET /contact_mechanism_purpose_type/:id" do
    it "can display the details of a contact_mechanism_purpose_type" do
      contact_mechanism_purpose_type = create(:contact_mechanism_purpose_type)
      visit(contact_mechanism_purpose_type_path(contact_mechanism_purpose_type))
      expect(page).to have_content(contact_mechanism_purpose_type.name)
    end

    it "Deletes a contact_mechanism_purpose_type" do
      DatabaseCleaner.clean
      contact_mechanism_purpose_type = create(:contact_mechanism_purpose_type)
      visit(contact_mechanism_purpose_types_path)
      expect{
        within "tr#contact_mechanism_purpose_type_#{contact_mechanism_purpose_type.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(ContactMechanismPurposeType,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Contact Mechanism Purpose Type was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('contact_mechanism_purpose_type.index_heading')}"
      expect(page).to_not have_content contact_mechanism_purpose_type.name
    end

  end

end

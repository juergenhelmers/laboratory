require 'spec_helper'

describe "RoleTypes" do

  before(:all) do
    RoleType.destroy_all
    create(:role_type, name: I18n.t('role_type.person_role'), parent_id: nil)
    create(:role_type, name: I18n.t('role_type.organization_role'), parent_id: nil)
    sleep 2
  end

  it "can list all role_types" do
    visit role_types_path
    expect(page.status_code).to be(200)
  end

  it "can create a new role_type" do
    visit(new_role_type_path)
    fill_in I18n.t('role_type.name'), with: "Valid"
    choose("#{I18n.t('role_type.person_role')}")
    click_button 'Save'
    within('div.alert.alert-success') {
      expect(page).to have_content("Role Type was successfully created")
    }
    expect(page).to have_content("Valid")
    expect(page).to have_content("Role")
  end

  it "cannot create a new role_type without name" do
    visit(new_role_type_path)
    click_button 'Save'
    within('.alert.alert-error') do
      expect(page).to have_content("Please review the problems below:")
    end
    expect(page).to have_content("can't be blank")
  end

  it "cannot create a new role_type without a parent" do
    visit(new_role_type_path)
    fill_in I18n.t('role_type.name'), with: "Valid"
    click_button 'Save'
    within('.alert.alert-error') do
      expect(page).to have_content("Please review the problems below:")
    end
    expect(page).to have_content("can't be blank")
  end

  it "can update an existing role_types" do
    role_type = create(:role_type)
    visit(edit_role_type_path(role_type))
    fill_in I18n.t('role_type.name'), with: "New Role"
    click_button 'Save'
    within('div.alert.alert-success') {
      expect(page).to have_content("Role Type was successfully updated")
    }
    expect(page).to have_content("New Role")
    expect(page).to have_content(role_type.parent.name)
  end

  it "can display the details of a role_types" do
    role_type = create(:role_type)
    visit(role_type_path(role_type))
    expect(page).to have_content(role_type.name)
    expect(page).to have_content(role_type.parent.name)
  end

  it "can read the record details from the index page following a link" do
    role_type = create(:role_type)
    visit(role_types_path)
    within("tr#role_type_#{role_type.id}") do
      click_link(role_type.name)
    end
    expect(page).to have_content("Details - #{role_type.name}")
    expect(page).to have_content(role_type.description)
  end

  it "Deletes a role_type record" do
    DatabaseCleaner.clean
    role_type = create(:role_type)
    visit(role_types_path)
    expect{
      within "tr#role_type_#{role_type.id}" do
        click_link I18n.t('actions.destroy')
      end
      }.to change(RoleType,:count).by(-1)
    within('div.alert.alert-success') {
      expect(page).to have_content("Role Type was successfully deleted")
    }
    expect(page).to have_content I18n.t('role_type.index_heading')
    expect(page).to_not have_content role_type.name
    expect(page).to_not have_content role_type.description
  end


end

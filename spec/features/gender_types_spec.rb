require 'spec_helper'

describe "GenderTypes" do
  describe "GET /gender_types" do
    it "does return code 200 when accessing the index page" do
      visit(gender_types_path)
      expect(page.status_code).to be(200)
    end

    it "lists the gender_type name on index page" do
      gender_type = create(:gender_type)
      visit(gender_types_path)
      expect(page).to have_content(gender_type.name)
    end

  end

  describe "GET /gender_type/new" do
    it "can create a new gender_type record" do
      visit(gender_types_path)
      click_link("New #{I18n.t('gender_type.model_name')}")
      fill_in I18n.t('gender_type.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Gender Type was successfully created")
      }
      expect(page).to have_content("MyString")
    end

    it "cannot create a new gender_type record without a name" do
      visit(gender_types_path)
      click_link("New #{I18n.t('gender_type.model_name')}")
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /gender_type/edit" do
    it "can update an existing gender_type" do
      gender_type = create(:gender_type)
      visit(edit_gender_type_path(gender_type))
      fill_in I18n.t('gender_type.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Gender Type was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /gender_type/:id" do
    it "can display the details of a gender_type" do
      gender_type = create(:gender_type)
      visit(gender_type_path(gender_type))
      expect(page).to have_content(gender_type.name)
    end

    it "Deletes a gender_type" do
      DatabaseCleaner.clean
      gender_type = create(:gender_type)
      visit(gender_types_path)
      expect{
        within "tr#gender_type_#{gender_type.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(GenderType,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Gender Type was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('gender_type.index_heading')}"
      expect(page).to_not have_content gender_type.name
    end

  end

end

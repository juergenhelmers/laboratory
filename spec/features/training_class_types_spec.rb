require 'spec_helper'

describe "TrainingClassTypes" do
  describe "GET /training_class_types" do
    it "lists the training_class types" do
      visit(training_class_types_path)
      expect(page.status_code).to be(200)
    end

    it "shows the name and description on the index page" do
      type = create(:training_class_type)
      visit(training_class_types_path)
      expect(page).to have_content(type.name)
      expect(page).to have_content(type.description)
    end
  end

  describe "GET /training_class_types/new" do
    it "can create a new training_class_type record" do
      visit(new_training_class_type_path)
      fill_in I18n.t('training_class_type.name'), with: "test"
      fill_in "Description", with: "Lorem Ipsum lskjdslad aslkdjlsakdj asldj asldkj"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Training Class Type was successfully created")
      }
      expect(page).to have_content("test")
      expect(page).to have_content("Lorem Ipsum lskjdslad aslkdjlsakdj asldj asldkj")
    end

    it "cannot create a new training_class_type without name" do
      visit(new_training_class_type_path)
      click_button 'Save'
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
      expect(page).to have_content("can't be blank")
    end

  end

  describe "GET /training_class_types/edit" do
    it "can update an existing training_class_type" do
      training_class_type = create(:training_class_type)
      visit(edit_training_class_type_path(training_class_type))
      fill_in I18n.t('training_class_type.name'), with: "New Type"
      click_button 'Save'
      within('div.alert.alert-success') {
        expect(page).to have_content("Training Class Type was successfully updated")
      }
      expect(page).to have_content("New Type")
    end
  end

  describe "GET /training_class_types/:id" do
    it "can display the details of a training_class_type" do
      training_class_type = create(:training_class_type)
      visit(training_class_type_path(training_class_type))
      expect(page).to have_content(training_class_type.name)
    end

    it "can reach the record details from the index page following a link" do
      training_class_type = create(:training_class_type)
      visit(training_class_types_path)
      within("tr#training_class_type_#{training_class_type.id}") do
        click_link(training_class_type.name)
      end
      expect(page).to have_content("Details - #{training_class_type.name}")
      expect(page).to have_content(training_class_type.description)
    end

    it "Deletes a party_status_type" do
      DatabaseCleaner.clean
      training_class_type = create(:training_class_type)
      visit(training_class_types_path)
      expect{
        within "tr#training_class_type_#{training_class_type.id}" do
        click_link I18n.t('actions.destroy')
      end
      #alert = page.driver.browser.switch_to.alert            # not supported by poltergeist
      #alert.accept                                           # always returns true
      }.to change(TrainingClassType,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Training Class Type was successfully deleted")
      }
      page.should have_content "Listing Training Class Types"
      page.should_not have_content training_class_type.name
      page.should_not have_content training_class_type.description
    end

  end

end

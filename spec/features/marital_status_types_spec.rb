require 'spec_helper'

describe "MaritalStatusTypes" do
  describe "GET /marital_status_types" do
      it "lists the training_class types" do
        visit(marital_status_types_path)
        expect(page.status_code).to be(200)
      end
  
      it "shows the name and description on the index page" do
        type = create(:marital_status_type)
        visit(marital_status_types_path)
        expect(page).to have_content(type.name)
        expect(page).to have_content(type.description)
      end
    end
  
    describe "GET /marital_status_types/new" do
      it "can create a new marital_status_type record" do
        visit(new_marital_status_type_path)
        fill_in I18n.t('marital_status_type.name'), with: "test"
        fill_in "Description", with: "Lorem Ipsum lskjdslad aslkdjlsakdj asldj asldkj"
        click_button I18n.t('actions.save')
        within('div.alert.alert-success') {
          expect(page).to have_content("Marital Status Type was successfully created")
        }
        expect(page).to have_content("test")
        expect(page).to have_content("Lorem Ipsum lskjdslad aslkdjlsakdj asldj asldkj")
      end

      it "cannot create a new marital_status_type without a name" do
        visit(new_marital_status_type_path)
        fill_in "Description", with: "Lorem Ipsum"
        click_button 'Save'
        expect(page).to have_content("can't be blank")
        within('.alert.alert-error') do
          expect(page).to have_content("Please review the problems below:")
        end
      end
    end
  
    describe "GET /marital_status_types/edit" do
      it "can update an existing marital_status_type" do
        marital_status_type = create(:marital_status_type)
        visit(edit_marital_status_type_path(marital_status_type))
        fill_in I18n.t('marital_status_type.name'), with: "New Type"
        click_button 'Save'
        within('div.alert.alert-success') {
          expect(page).to have_content("Marital Status Type was successfully updated")
        }
        expect(page).to have_content("New Type")
      end
    end
  
    describe "GET /marital_status_types/:id" do
      it "can display the details of a marital_status_type" do
        marital_status_type = create(:marital_status_type)
        visit(marital_status_type_path(marital_status_type))
        expect(page).to have_content(marital_status_type.name)
      end

      it "can reach the record details from the index page following a link" do
        marital_status_type = create(:marital_status_type)
        visit(marital_status_types_path)
        within("tr#marital_status_type_#{marital_status_type.id}") do
          click_link(marital_status_type.name)
        end
        expect(page).to have_content("Details - #{marital_status_type.name}")
        expect(page).to have_content(marital_status_type.description)
      end

      it "can delete a marital_status_type record" do
        marital_status_type = create(:marital_status_type)
        visit(marital_status_types_path)
        expect{
          within "tr#marital_status_type_#{marital_status_type.id}" do
            click_link I18n.t('actions.destroy')
          end
        }.to change(MaritalStatusType,:count).by(-1)
        within('div.alert.alert-success') {
          expect(page).to have_content("Marital Status Type was successfully deleted")
        }
        expect(page).to have_content "Listing Marital Status Types"
        expect(page).to_not have_content marital_status_type.name
      end
    end
end

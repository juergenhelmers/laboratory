require 'spec_helper'

describe "Languages" do
  describe "GET /languages" do
    it "can list all languages" do
      visit(languages_path)
      expect(page.status_code).to be(200)
    end

    it "shows the name, description and abbreviation on the index page" do
      lang = create(:language)
      visit(languages_path)
      expect(page).to have_content(lang.name)
      expect(page).to have_content(lang.abbreviation)
      expect(page).to have_content(lang.description)
    end
  end

  describe "GET /languages/new" do
    it "can create a new language record" do
      visit(new_language_path)
      fill_in I18n.t('language.name'), with: "French"
      fill_in I18n.t('language.abbreviation'), with: "FR"
      fill_in I18n.t('language.description'), with: "Lorem Ipsum lskjdslad aslkdjlsakdj asldj asldkj"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Language was successfully created")
      }
      expect(page).to have_content("French")
      expect(page).to have_content("FR")
      expect(page).to have_content("Lorem Ipsum lskjdslad aslkdjlsakdj asldj asldkj")
    end
    it "cannot create a new language without a name" do
      visit(new_language_path)
      fill_in I18n.t('language.abbreviation'), with: "FR"
      fill_in I18n.t('language.description'), with: "Lorem Ipsum"
      click_button I18n.t('actions.save')
      expect(page).to have_content("can't be blank")
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /languages/edit" do
    it "can update an existing language" do
      lang = create(:language)
      visit(edit_language_path(lang))
      fill_in I18n.t('language.name'), with: "German"
      fill_in I18n.t('language.abbreviation'), with: "DE"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Language was successfully updated")
      }
      expect(page).to have_content("German")
      expect(page).to have_content("DE")
    end
  end

  describe "GET /languages/:id" do
    it "can display the details of a language" do
      language = create(:language)
      visit(language_path(language))
      expect(page).to have_content(language.name)
      expect(page).to have_content(language.abbreviation)
    end

    it "can reach the record details from the index page following a link" do
      language = create(:language)
      visit(languages_path)
      within("tr#language_#{language.id}") do
        click_link(language.name)
      end
      expect(page).to have_content("Details - #{language.name}")
      expect(page).to have_content(language.abbreviation)
    end

    it "can delete a language record" do
      language = create(:language)
      visit(languages_path)
      expect{
        within "tr#language_#{language.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(Language,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Language was successfully deleted")
      }
      expect(page).to have_content I18n.t('language.index_heading')
      expect(page).to_not have_content language.name
    end
  end
end

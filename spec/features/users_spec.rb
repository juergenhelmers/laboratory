require 'spec_helper'

describe "Users" do
  describe "GET /admin/users" do
    it "does return code 200 when accessing the index page" do
      visit(admin_users_path)
      expect(page.status_code).to be(200)
    end

    it "lists the users details on index page" do
      user = create(:user)
      visit(admin_users_path)
      expect(page).to have_content(user.firstname)
      expect(page).to have_content(user.lastname)
      expect(page).to have_content(user.email)
    end

    it "displays the admin menu only when user is an admin", js: true do
      user_role = create(:application_user_role, name: "Admin")
      user = create(:user, application_user_role_id: user_role.id, password: "secret12345", password_confirmation: "secret12345")
      visit(new_user_session_path)
      fill_in "Email", with: user.email
      fill_in "Password", with: user.password
      click_button "Sign in"
      visit(admin_users_path)
      within('div.nav-collapse') {
        expect(page).to have_content("Admin")
      }
    end
  end

  describe "GET /admin/users/new" do
    it "can create a new user record" do
      user_role = create(:application_user_role, name: "Admin")
      visit(admin_users_path)
      click_link("New #{I18n.t('user.model_name')}")
      select user_role.name, from: "#{I18n.t('user.application_user_role_id')}"
      fill_in I18n.t('user.firstname'), with: "Simone"
      fill_in I18n.t('user.lastname'), with: "deBouvoir"
      fill_in I18n.t('user.email'), with: "sdb@zettagenomics.com"
      fill_in I18n.t('user.company_id'), with: "123123123123"
      fill_in I18n.t('user.date_of_birth'), with: (Time.now.to_date - 21.years)
      fill_in I18n.t('user.password'), with: "secret213123"
      fill_in I18n.t('user.password_confirmation'), with: "secret213123"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Application User was successfully created")
      }
      expect(page).to have_content("Simone")
      expect(page).to have_content("deBouvoir")
      expect(page).to have_content("sdb@zettagenomics.com")
      expect(page).to have_content("#{(Time.now.to_date - 21.years)}")
    end

    it "cannot create a new user record without a firstname" do
     visit(admin_users_path)
     click_link("New #{I18n.t('user.model_name')}")
     fill_in I18n.t('user.lastname'), with: "deBouvoir"
     fill_in I18n.t('user.email'), with: "sdb@zettagenomics.com"
     fill_in I18n.t('user.company_id'), with: "123123123123"
     fill_in I18n.t('user.date_of_birth'), with: (Time.now.to_date - 21.years)
     fill_in I18n.t('user.password'), with: "secret213123"
     fill_in I18n.t('user.password_confirmation'), with: "secret213123"
     click_button I18n.t('actions.save')
     within('.alert.alert-error') do
       expect(page).to have_content("Please review the problems below:")
     end
   end
  end
  describe "GET /admin/user/edit" do
    it "can update an existing user" do
      user = create(:user)
      visit(edit_admin_user_path(user))
      fill_in I18n.t('user.firstname'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Application User was successfully updated")
      }
      expect(page).to have_content("MyString")

    end
  end
  
  describe "GET /admin/user/:id" do
    it "can display the details of a user" do
      ApplicationUserRole.destroy_all
      user = create(:user)
      visit(admin_user_path(user))
      expect(page).to have_content(user.firstname)
    end

    it "Deletes a user" do
      DatabaseCleaner.clean
      user = create(:user)
      visit(admin_users_path)
      expect{
        within "tr#user_#{user.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(User,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Application User was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('user.index_heading')}"
      expect(page).to_not have_content user.firstname
    end

  end
  
end
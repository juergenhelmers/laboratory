require 'spec_helper'

describe "DiagnosisTypes" do
  describe "GET /diagnosis_types" do
    it "does return code 200 when accessing the index page" do
      visit(diagnosis_types_path)
      expect(page.status_code).to be(200)
    end

    it "lists the diagnosis_type name on index page" do
      diagnosis_type = create(:diagnosis_type)
      visit(diagnosis_types_path)
      expect(page).to have_content(diagnosis_type.name)
    end

  end

  describe "GET /diagnosis_type/new" do
    it "can create a new diagnosis_type record" do
      visit(diagnosis_types_path)
      click_link("New #{I18n.t('diagnosis_type.model_name')}")
      fill_in I18n.t('diagnosis_type.name'), with: "MyString"
      fill_in I18n.t('diagnosis_type.code'), with: "9999"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Diagnosis Type was successfully created")
      }
      expect(page).to have_content("MyString")
      expect(page).to have_content("9999")
    end

    it "cannot create a new diagnosis_type record without a name" do
      visit(diagnosis_types_path)
      click_link("New #{I18n.t('diagnosis_type.model_name')}")
      fill_in I18n.t('diagnosis_type.code'), with: "9999"
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
    it "cannot create a new diagnosis_type record without a code" do
      visit(diagnosis_types_path)
      click_link("New #{I18n.t('diagnosis_type.model_name')}")
      fill_in I18n.t('diagnosis_type.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /diagnosis_type/edit" do
    it "can update an existing diagnosis_type" do
      diagnosis_type = create(:diagnosis_type)
      visit(edit_diagnosis_type_path(diagnosis_type))
      fill_in I18n.t('diagnosis_type.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Diagnosis Type was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /diagnosis_type/:id" do
    it "can display the details of a diagnosis_type" do
      diagnosis_type = create(:diagnosis_type)
      visit(diagnosis_type_path(diagnosis_type))
      expect(page).to have_content(diagnosis_type.name)
    end

    it "Deletes a diagnosis_type" do
      DatabaseCleaner.clean
      diagnosis_type = create(:diagnosis_type)
      visit(diagnosis_types_path)
      expect{
        within "tr#diagnosis_type_#{diagnosis_type.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(DiagnosisType,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Diagnosis Type was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('diagnosis_type.index_heading')}"
      expect(page).to_not have_content diagnosis_type.name
    end

  end

end

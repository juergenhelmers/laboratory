require 'spec_helper'

describe "PersonStatusTypes" do
  describe "GET /person_status_types" do
    it "can list all person_status_types" do
      visit person_status_types_path
      expect(page.status_code).to be(200)
    end

    it "can create a new person_status_type" do
      visit(new_person_status_type_path)
      fill_in I18n.t('person_status_type.name'), with: "Valid"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Person Status Type was successfully created")
      }
      expect(page).to have_content("Valid")
    end

    it "cannot create a new person_status_types without name" do
      visit(new_person_status_type_path)
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
      expect(page).to have_content("can't be blank")
    end

    it "can update an existing person_status_type" do
      person_status_type = create(:person_status_type)
      visit(edit_person_status_type_path(person_status_type))
      fill_in I18n.t('person_status_type.name'), with: "New Status"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Person Status Type was successfully updated")
      }
      expect(page).to have_content("New Status")
    end

    it "can display the details of a person_status_type" do
      person_status_type = create(:person_status_type)
      visit(person_status_type_path(person_status_type))
      expect(page).to have_content(person_status_type.name)
    end

    it "can reach the record details from the index page following a link" do
      person_status_type = create(:person_status_type)
      visit(person_status_types_path)
      within("tr#person_status_type_#{person_status_type.id}") do
        click_link(person_status_type.name)
      end
      expect(page).to have_content("Details - #{person_status_type.name}")
      expect(page).to have_content(person_status_type.description)
    end

    it "Deletes a person_status_type record" do
      DatabaseCleaner.clean
      person_status_type = create(:person_status_type)
      visit(person_status_types_path)
      expect{
        within "tr#person_status_type_#{person_status_type.id}" do
          click_link I18n.t('actions.destroy')
        end
        #alert = page.driver.browser.switch_to.alert            # not supported by poltergeist
        #alert.accept                                           # always returns true
      }.to change(PersonStatusType,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Person Status Type was successfully deleted")
      }
      expect(page).to have_content I18n.t('person_status_type.index_heading')
      expect(page).to_not have_content person_status_type.name
      expect(page).to_not have_content person_status_type.description
    end

  end
end

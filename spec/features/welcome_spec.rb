require 'spec_helper'

describe "Welcome" do
  it "can access the welcome index view" do
    visit(welcome_index_path)
    expect(page.status_code).to be(200)
  end

  it "should have 'Zetta Genomics' in the page " do
    visit(welcome_index_path)
    expect(page).to have_content("Zetta Genomics")
  end
end
require 'spec_helper'

describe "Organizations" do
  describe "GET /organizations" do
    it "does return code 200 when accessing the index page" do
      visit(organizations_path)
      expect(page.status_code).to be(200)
    end

    it "lists the organization name on index page" do
      organization = create(:organization)
      visit(organizations_path)
      expect(page).to have_content(organization.current_name)
    end

  end

  describe "GET /organization/new" do
    it "can create a new organization record" do
      visit(organizations_path)
      click_link("New #{I18n.t('organization.model_name')}")
      fill_in I18n.t('organization.current_name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Organization was successfully created")
      }
      expect(page).to have_content("MyString")
    end

    it "cannot create a new organization record without a name" do
      visit(organizations_path)
      click_link("New #{I18n.t('organization.model_name')}")
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /organization/edit" do
    it "can update an existing organization" do
      organization = create(:organization)
      visit(edit_organization_path(organization))
      fill_in I18n.t('organization.current_name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Organization was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /organization/:id" do
    it "can display the details of a organization" do
      organization = create(:organization)
      visit(organization_path(organization))
      expect(page).to have_content(organization.current_name)
    end

    it "Deletes a organization" do
      DatabaseCleaner.clean
      organization = create(:organization)
      visit(organizations_path)
      expect{
        within "tr#organization_#{organization.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(Organization,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Organization was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('organization.index_heading')}"
      expect(page).to_not have_content organization.current_name
    end

  end

end

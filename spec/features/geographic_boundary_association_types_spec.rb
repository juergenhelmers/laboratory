require 'spec_helper'

describe "GeographicBoundaryAssociationTypes" do
  describe "GET /geographic_boundary_association_types" do
    it "does return code 200 when accessing the index page" do
      visit(geographic_boundary_association_types_path)
      expect(page.status_code).to be(200)
    end

    it "lists the geographic_boundary_association_type name on index page" do
      geographic_boundary_association_type = create(:geographic_boundary_association_type)
      visit(geographic_boundary_association_types_path)
      expect(page).to have_content(geographic_boundary_association_type.name)
    end

  end

  describe "GET /geographic_boundary_association_type/new" do
    it "can create a new geographic_boundary_association_type record" do
      visit(geographic_boundary_association_types_path)
      click_link("New #{I18n.t('geographic_boundary_association_type.model_name')}")
      fill_in I18n.t('geographic_boundary_association_type.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Geographic Boundary Association Type was successfully created")
      }
      expect(page).to have_content("MyString")
    end

    it "cannot create a new geographic_boundary_association_type record without a name" do
      visit(geographic_boundary_association_types_path)
      click_link("New #{I18n.t('geographic_boundary_association_type.model_name')}")
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
      expect(page).to have_content("can't be blank")
    end
  end

  describe "GET /geographic_boundary_association_type/edit" do
    it "can update an existing geographic_boundary_association_type" do
      geographic_boundary_association_type = create(:geographic_boundary_association_type)
      visit(edit_geographic_boundary_association_type_path(geographic_boundary_association_type))
      fill_in I18n.t('geographic_boundary_association_type.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Geographic Boundary Association Type was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /geographic_boundary_association_type/:id" do
    it "can display the details of a geographic_boundary_association_type" do
      geographic_boundary_association_type = create(:geographic_boundary_association_type)
      visit(geographic_boundary_association_type_path(geographic_boundary_association_type))
      expect(page).to have_content(geographic_boundary_association_type.name)
    end

    it "Deletes a geographic_boundary_association_type" do
      DatabaseCleaner.clean
      geographic_boundary_association_type = create(:geographic_boundary_association_type)
      visit(geographic_boundary_association_types_path)
      expect{
        within "tr#geographic_boundary_association_type_#{geographic_boundary_association_type.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(GeographicBoundaryAssociationType,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Geographic Boundary Association Type was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('geographic_boundary_association_type.index_heading')}"
      expect(page).to_not have_content geographic_boundary_association_type.name
    end
  end
end

require 'spec_helper'

describe "Categories" do
  before(:all) do
    DatabaseCleaner.clean
  end

  describe "GET /categories" do
    it "does return code 200 when accessing the index page" do
      visit(categories_path)
      expect(page.status_code).to be(200)
    end

    it "lists the category name, description and type-name on index page" do
      category = create(:category)
      visit(categories_path)
      expect(page).to have_content(category.name)
      expect(page).to have_content(category.description)
      expect(page).to have_content(category.category_type.name)
    end
  end

  describe "GET /category/new" do
    it "can create a new category record" do
      category_type = create(:category_type)
      visit(categories_path)
      click_link "New #{I18n.t('category.model_name')}"
      fill_in I18n.t('category.name'), with: "test category"
      fill_in I18n.t('category.description'), with: "test category description"
      select category_type.name, from: I18n.t('category.category_type')
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Category was successfully created")
      }
      expect(page).to have_content("test category")
      expect(page).to have_content("test category description")
      expect(page).to have_content(category_type.name)
    end


    it "cannot create a new category record without a name" do
      category_type = create(:category_type)
      visit(categories_path)
      click_link "New #{I18n.t('category.model_name')}"
      fill_in I18n.t('category.description'), with: "test category description"
      select category_type.name, from: I18n.t('category.category_type')
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /category/edit" do
    it "can update an existing category" do
      category = create(:category)
      visit(edit_category_path(category))
      fill_in I18n.t('category.name'), with: "catcat"
      fill_in I18n.t('category.description'), with: "Description for this category"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Category was successfully updated")
      }
      expect(page).to have_content("catcat")
      expect(page).to have_content("Description for this category")
    end
  end

  describe "GET /category/:id" do
    it "can display the details of a category" do
      category = create(:category)
      visit(category_path(category))
      expect(page).to have_content(category.name)
      expect(page).to have_content(category.description)
      expect(page).to have_content(category.category_type.name)
    end

    it "can read the record details from the index page following a link" do
      category = create(:category)
      visit(categories_path)
      within("tr#category_#{category.id}") do
        click_link(category.name)
      end
      expect(page).to have_content("Details - #{category.name}")
      expect(page).to have_content(category.description)
      expect(page).to have_content(category.category_type.name)
    end


    it "Deletes a category" do
      category = create(:category)
      visit(categories_path)
      expect{
        within "tr#category_#{category.id}" do
          click_link I18n.t('actions.destroy')
        end
        #alert = page.driver.browser.switch_to.alert            # not supported by poltergeist
        #alert.accept                                           # always returns true
      }.to change(Category,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Category was successfully deleted")
      }
      expect(page).to have_content I18n.t('category.index_heading')
      expect(page).to_not have_content category.name
      expect(page).to_not have_content category.description
    end

  end
end

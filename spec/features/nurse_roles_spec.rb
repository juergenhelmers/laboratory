require 'spec_helper'

describe "NurseRoles" do
  describe "GET /nurse_roles" do
    it "does return code 200 when accessing the index page" do
      visit(nurse_roles_path)
      expect(page.status_code).to be(200)
    end

    it "lists the nurse_role name on index page" do
      nurse_role = create(:nurse_role)
      visit(nurse_roles_path)
      expect(page).to have_content(nurse_role.medical_id)
    end

  end

  describe "GET /nurse_role/new" do
    it "can create a new nurse_role record" do
      visit(nurse_roles_path)
      click_link("New #{I18n.t('nurse_role.model_name')}")
      fill_in I18n.t('nurse_role.medical_id'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Nurse Role was successfully created")
      }
      expect(page).to have_content("MyString")
    end

    it "cannot create a new nurse_role record without a medical ID" do
      visit(nurse_roles_path)
      click_link("New #{I18n.t('nurse_role.model_name')}")
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /nurse_role/edit" do
    it "can update an existing nurse_role" do
      nurse_role = create(:nurse_role)
      visit(edit_nurse_role_path(nurse_role))
      fill_in I18n.t('nurse_role.medical_id'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Nurse Role was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /nurse_role/:id" do
    it "can display the details of a nurse_role" do
      nurse_role = create(:nurse_role)
      visit(nurse_role_path(nurse_role))
      expect(page).to have_content(nurse_role.medical_id)
    end

    it "Deletes a nurse_role" do
      DatabaseCleaner.clean
      nurse_role = create(:nurse_role)
      visit(nurse_roles_path)
      expect{
        within "tr#nurse_role_#{nurse_role.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(NurseRole,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Nurse Role was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('nurse_role.index_heading')}"
      expect(page).to_not have_content nurse_role.medical_id
    end

  end

end

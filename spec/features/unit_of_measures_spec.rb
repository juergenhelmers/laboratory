require 'spec_helper'

describe "UnitOfMeasures" do
  describe "GET /unit_of_measures" do
    it "does return code 200 when accessing the index page" do
      visit(unit_of_measures_path)
      expect(page.status_code).to be(200)
    end

    it "lists the unit_of_measure name on index page" do
      unit_of_measure = create(:unit_of_measure)
      visit(unit_of_measures_path)
      expect(page).to have_content(unit_of_measure.name)
    end

  end

  describe "GET /unit_of_measure/new" do
    it "can create a new unit_of_measure record" do
      visit(unit_of_measures_path)
      click_link("New #{I18n.t('unit_of_measure.model_name')}")
      fill_in I18n.t('unit_of_measure.name'), with: "Ampere"
      fill_in I18n.t('unit_of_measure.abbreviation'), with: "A"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Unit Of Measure was successfully created")
      }
      expect(page).to have_content("Ampere")
      expect(page).to have_content("A")
    end

    it "cannot create a new unit_of_measure record without a name" do
      visit(unit_of_measures_path)
      click_link("New #{I18n.t('unit_of_measure.model_name')}")
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /unit_of_measure/edit" do
    it "can update an existing unit_of_measure" do
      unit_of_measure = create(:unit_of_measure)
      visit(edit_unit_of_measure_path(unit_of_measure))
      fill_in I18n.t('unit_of_measure.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Unit Of Measure was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /unit_of_measure/:id" do
    it "can display the details of a unit_of_measure" do
      unit_of_measure = create(:unit_of_measure)
      visit(unit_of_measure_path(unit_of_measure))
      expect(page).to have_content(unit_of_measure.name)
    end

    it "Deletes a unit_of_measure" do
      DatabaseCleaner.clean
      unit_of_measure = create(:unit_of_measure)
      visit(unit_of_measures_path)
      expect{
        within "tr#unit_of_measure_#{unit_of_measure.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(UnitOfMeasure,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Unit Of Measure was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('unit_of_measure.index_heading')}"
      expect(page).to_not have_content unit_of_measure.name
    end

  end

end

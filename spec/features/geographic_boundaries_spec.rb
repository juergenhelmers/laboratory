require 'spec_helper'

describe "GeographicBoundaries" do
  describe "GET /geographic_boundaries" do
    it "works! (now write some real specs)" do
      visit(geographic_boundaries_path)
      expect(page.status_code).to be(200)
    end

    it "lists the geographic_boundary name on index page" do
      geographic_boundary = create(:geographic_boundary)
      visit(geographic_boundaries_path)
      expect(page).to have_content(geographic_boundary.name)
    end

  end

  describe "GET /geographic_boundary/new" do
    it "can create a new geographic_boundary record" do
      geo_bound_type = create(:geographic_boundary_type)
      visit(geographic_boundaries_path)
      click_link("New #{I18n.t('geographic_boundary.model_name')}")
      fill_in I18n.t('geographic_boundary.name'), with: "MyString"
      select "#{geo_bound_type.name}", from: "Geographic Boundary Type"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Geographic Boundary was successfully created")
      }
      expect(page).to have_content("MyString")
    end

    it "cannot create a new geographic_boundary record without a name" do
      geo_bound_type = create(:geographic_boundary_type)
      visit(geographic_boundaries_path)
      click_link("New #{I18n.t('geographic_boundary.model_name')}")
      select "#{geo_bound_type.name}", from: "Geographic Boundary Type"
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end

    it "cannot create a new geographic_boundary record without a type" do
      visit(geographic_boundaries_path)
      click_link("New #{I18n.t('geographic_boundary.model_name')}")
      fill_in I18n.t('geographic_boundary.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /geographic_boundary/edit" do
    it "can update an existing geographic_boundary" do
      geographic_boundary = create(:geographic_boundary)

      visit(edit_geographic_boundary_path(geographic_boundary))
      fill_in I18n.t('geographic_boundary.name'), with: "MyString"
      click_button 'Save'
      within('div.alert.alert-success') {
        expect(page).to have_content("Geographic Boundary was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /geographic_boundary/:id" do
    it "can display the details of a geographic_boundary" do
      geographic_boundary = create(:geographic_boundary)
      visit(geographic_boundary_path(geographic_boundary))
      expect(page).to have_content(geographic_boundary.name)
    end

    it "Deletes a geographic_boundary" do
      DatabaseCleaner.clean
      geographic_boundary = create(:geographic_boundary)
      visit(geographic_boundaries_path)
      expect{
        within "tr#geographic_boundary_#{geographic_boundary.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(GeographicBoundary,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Geographic Boundary was successfully deleted")
      }
      expect(page).to have_content I18n.t('geographic_boundary.index_heading')
      expect(page).to_not have_content geographic_boundary.name
    end

  end

end

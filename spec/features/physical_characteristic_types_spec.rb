require 'spec_helper'

describe "PhysicalCharacteristicTypes" do
  describe "GET /physical_characteristic_types" do
    it "does return code 200 when accessing the index page" do
      visit(physical_characteristic_types_path)
      expect(page.status_code).to be(200)
    end

    it "lists the physical_characteristic_type name on index page" do
      physical_characteristic_type = create(:physical_characteristic_type)
      visit(physical_characteristic_types_path)
      expect(page).to have_content(physical_characteristic_type.name)
    end

  end

  describe "GET /physical_characteristic_type/new" do
    it "can create a new physical_characteristic_type record" do
      visit(physical_characteristic_types_path)
      click_link("New #{I18n.t('physical_characteristic_type.model_name')}")
      fill_in I18n.t('physical_characteristic_type.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Physical Characteristic Type was successfully created")
      }
      expect(page).to have_content("MyString")
    end

    it "cannot create a new physical_characteristic_type record without a name" do
      visit(physical_characteristic_types_path)
      click_link("New #{I18n.t('physical_characteristic_type.model_name')}")
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /physical_characteristic_type/edit" do
    it "can update an existing physical_characteristic_type" do
      physical_characteristic_type = create(:physical_characteristic_type)
      visit(edit_physical_characteristic_type_path(physical_characteristic_type))
      fill_in I18n.t('physical_characteristic_type.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Physical Characteristic Type was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /physical_characteristic_type/:id" do
    it "can display the details of a physical_characteristic_type" do
      physical_characteristic_type = create(:physical_characteristic_type)
      visit(physical_characteristic_type_path(physical_characteristic_type))
      expect(page).to have_content(physical_characteristic_type.name)
    end

    it "Deletes a physical_characteristic_type" do
      DatabaseCleaner.clean
      physical_characteristic_type = create(:physical_characteristic_type)
      visit(physical_characteristic_types_path)
      expect{
        within "tr#physical_characteristic_type_#{physical_characteristic_type.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(PhysicalCharacteristicType,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Physical Characteristic Type was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('physical_characteristic_type.index_heading')}"
      expect(page).to_not have_content physical_characteristic_type.name
    end

  end

end

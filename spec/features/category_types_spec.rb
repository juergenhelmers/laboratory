require 'spec_helper'

describe "CategoryTypes" do
  describe "GET /category_types" do
    it "the index page return code 200" do
      visit(category_types_path)
      expect(page.status_code).to be(200)
    end

    it "lists the name, type_class and description on the index page" do
      ct = create(:category_type)
      visit(category_types_path)
      expect(page).to have_content(ct.name)
      expect(page).to have_content(ct.description)
    end
  end
  describe "GET /category_types/new" do
    it "can create a new category_type record" do
      visit(category_types_path)
      click_link("New #{I18n.t('category_type.model_name')}")
      fill_in I18n.t('category_type.name'), with: "test"
      fill_in I18n.t('category_type.description'), with: "Lorem Ipsum lskjdslad aslkdjlsakdj asldj asldkj"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Category Type was successfully created")
      }
      expect(page).to have_content("test")
      expect(page).to have_content("Lorem Ipsum lskjdslad aslkdjlsakdj asldj asldkj")
    end

    it "cannot create a category_type with a duplicate name" do
      ct = create(:category_type)
      visit(new_category_type_path)
      fill_in I18n.t('category_type.name'), with: ct.name
      click_button I18n.t('actions.save')
      expect(page).to have_content("has already been taken")
    end
  end

  describe "GET /category_types/edit" do
    it "can update an existing category_type" do
      category_type = create(:category_type)
      visit(edit_category_type_path(category_type))
      fill_in I18n.t('category_type.name'), with: "New Type"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Category Type was successfully updated")
      }
      expect(page).to have_content("New Type")
    end
  end

  describe "GET /category_types/:id" do
    it "can display the details of a category_type" do
      category_type = create(:category_type)
      visit(category_type_path(category_type))
      expect(page).to have_content(category_type.name)
    end

    it "can reach the record details from the index page following a link" do
      category_type = create(:category_type)
      visit(category_types_path)
      within("tr#category_type_#{category_type.id}") do
        click_link(category_type.name)
      end
      expect(page).to have_content("Details - #{category_type.name}")
      expect(page).to have_content(category_type.description)
    end

    it "Deletes a category_type" do
      DatabaseCleaner.clean
      category_type = create(:category_type)
      visit(category_types_path)
      expect{
        within "tr#category_type_#{category_type.id}" do
          click_link I18n.t('actions.destroy')
        end
        #alert = page.driver.browser.switch_to.alert            # not supported by poltergeist
        #alert.accept                                           # always returns true
      }.to change(CategoryType,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Category Type was successfully deleted")
      }
      expect(page).to have_content "Listing Category Type"
      expect(page).to_not have_content category_type.name
      expect(page).to_not have_content category_type.description
    end
  end
end

require 'spec_helper'

describe PersonContactMechanismElectronicAddress do
  it "has a valid fixture" do
    expect(create(:person_contact_mechanism_electronic_address)).to be_valid
  end

  it "requires a role_type" do
   expect(build(:person_contact_mechanism_electronic_address, role_type_id: nil)).to_not be_valid
  end

  it "requires a contact_mechanism" do
   expect(build(:person_contact_mechanism_electronic_address, contact_mechanism_electronic_address_id: nil)).to_not be_valid
  end

  it "should have the current date assigned as from_date" do
    pcmea = create(:person_contact_mechanism_electronic_address)
    expect(pcmea.from_date).to eq(Time.now.to_date)
  end

end

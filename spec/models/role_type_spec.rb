require 'spec_helper'

describe RoleType do

  before(:each) do
    RoleType.destroy_all
    create(:role_type, name: "Person Role", parent_id: nil)
    create(:role_type, name: "Organization Role", parent_id: nil)
  end

  it "has a valid factory" do
    expect(create(:role_type)).to be_valid
  end

  it "cannot have a duplicate" do
    prt = create(:role_type, name: "admin")
    prt2 = build(:role_type, name: "admin")
    expect(prt2).to_not be_valid
  end

  it "is not valid without a name" do
    expect(build(:role_type, name: nil)).to_not be_valid
  end

  it "is not valid without a parent_id" do
    expect(build(:role_type, parent_id: nil)).to_not be_valid
  end

  it "is not valid with an invalid parent_id" do
    expect(build(:role_type, parent_id: 3)).to_not be_valid
  end
end

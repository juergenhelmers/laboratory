require 'spec_helper'

describe PersonContactMechanismTelecommunication do
  it "has a valid fixture" do
    expect(create(:person_contact_mechanism_telecommunication)).to be_valid
  end

  it "requires a role_type" do
   expect(build(:person_contact_mechanism_telecommunication, role_type_id: nil)).to_not be_valid
  end

  it "requires a contact_mechanism" do
   expect(build(:person_contact_mechanism_telecommunication, contact_mechanism_telecommunication_id: nil)).to_not be_valid
  end

  it "should have the current date assigned as from_date" do
    pcmp = create(:person_contact_mechanism_telecommunication)
    expect(pcmp.from_date).to eq(Time.now.to_date)
  end

end

require 'spec_helper'

describe CategoryType do
  it "has a valid fixture" do
    expect(create(:category_type)).to be_valid
  end

  it "has a unique name" do
    create(:category_type, name: "test")
    expect(build(:category_type, name: "test")).to_not be_valid
  end

  it "is not valid without a name" do
    expect(build(:category_type, name: nil )).to_not be_valid
  end
end

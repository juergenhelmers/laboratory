require 'spec_helper'

describe MaritalStatusType do
  it "has a valid factory" do
    expect(create(:marital_status_type)).to be_valid
  end

  it "requires a name to be valid" do
    expect(build(:marital_status_type, name: nil)).to_not be_valid
  end

  it "cannot have a duplicate" do
    mst = create(:marital_status_type, name: "single")
    mst2 = build(:marital_status_type, name: "single")
    expect(mst2).to_not be_valid
  end
end

require 'spec_helper'

describe ContactMechanismType do
  it "has a valid fixture" do
    expect(create(:contact_mechanism_type)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:contact_mechanism_type, name: nil)).to_not be_valid
  end

  it "cannot have a duplicate" do
    cmt = create(:contact_mechanism_type, name: "phone")
    cmt2 = build(:contact_mechanism_type, name: "phone")
    expect(cmt2).to_not be_valid
  end

  it "excludes Phone and Telecommunications with electronic scope", focus: true do
    @phone = create(:contact_mechanism_type, name: "#{I18n.t('contact_mechanism_type.phone')}")
    @postal = create(:contact_mechanism_type, name: "#{I18n.t('contact_mechanism_type.postal_address')}")
    @email = create(:contact_mechanism_type, name: "#{I18n.t('contact_mechanism_type.email')}")
    expect(ContactMechanismType.electronic).to eq([@email])
  end
end

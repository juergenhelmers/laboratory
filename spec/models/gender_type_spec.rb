require 'spec_helper'

describe GenderType do
  it "has a valid fixture" do
    expect(create(:gender_type)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:gender_type, name: nil)).to_not be_valid
  end

  it "is not valid with a duplicate name" do
  	gt1 = create(:gender_type, name: "female")
  	gt2 = build(:gender_type, name: "female")
  	expect(gt2).to_not be_valid
  end
end

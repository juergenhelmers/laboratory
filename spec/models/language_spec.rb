require 'spec_helper'

describe Language do
  it "has a valid factory" do
    expect(create(:language)).to be_valid
  end

  it " is not valid without a name" do
    language = build(:language, name: nil)
    expect(language).to_not be_valid
  end

  it" cannot have a duplicate" do
    language = create(:language, name: "English", abbreviation: "EN")
    language2 = build(:language, name: "English", abbreviation: "EN")
    expect(language2).to_not be_valid
  end


  it " is not valid without an abbreviation code" do
    language = build(:language, abbreviation: nil)
    expect(language).to_not be_valid
  end
  it " is not valid with an too long abbreviation code" do
    language = build(:language, abbreviation: "ENG")
    expect(language).to_not be_valid
  end

  it " is not valid with an too short abbreviation code" do
    language = build(:language, abbreviation: "E")
    expect(language).to_not be_valid
  end
  it "all abbreviation code is upcased before save" do
    language = create(:language, abbreviation: "af")
    expect(language.abbreviation).to eq("AF")
  end

end

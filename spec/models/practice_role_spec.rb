require 'spec_helper'

describe PracticeRole do
  it "has a valid fixture" do
    expect(create(:practice_role)).to be_valid
  end

  it "is not valid without an organization_id" do
    expect(build(:practice_role, organization_id: nil)).to_not be_valid
  end

end

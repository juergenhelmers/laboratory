require 'spec_helper'

describe ApplicationUserRole do
  it "has a valid fixture" do
    expect(create(:application_user_role)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:application_user_role, name: nil)).to_not be_valid
  end

  it "is not valid with an existing name" do
    role = create(:application_user_role)
    expect(build(:application_user_role, name: role.name)).to_not be_valid
  end

end

require 'spec_helper'

describe PhysicalCharacteristicType do
  it "has a valid fixture" do
    expect(create(:physical_characteristic_type)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:physical_characteristic_type, name: nil)).to_not be_valid
  end

  it "is not valid with a dup name" do
    create(:physical_characteristic_type, name: "tall")
    expect(build(:physical_characteristic_type, name: "tall")).to_not be_valid
  end
end

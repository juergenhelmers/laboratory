require 'spec_helper'

describe DoctorContactRelationship do
  it "has a valid fixture" do
    expect(create(:doctor_contact_relationship)).to be_valid
  end

  it "not valid w/o a from_date" do
    expect(build(:doctor_contact_relationship, from_date: nil)).to_not be_valid
  end
end

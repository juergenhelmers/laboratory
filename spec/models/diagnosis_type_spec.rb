require 'spec_helper'

describe DiagnosisType do
  it "has a valid fixture" do
    expect(create(:diagnosis_type)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:diagnosis_type, name: nil)).to_not be_valid
  end

  it "is not valid without a code" do
    expect(build(:diagnosis_type, code: nil)).to_not be_valid
  end

  it "is not valid with a duplicate code" do
    c1 = create(:diagnosis_type, code: "9999")
    c2 = build(:diagnosis_type, code: "9999")
    expect(c2).to_not be_valid
  end
end

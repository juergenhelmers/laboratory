require 'spec_helper'

describe GeographicBoundary do
  it "has a valid fixture" do
    expect(create(:geographic_boundary)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:geographic_boundary, name: nil)).to_not be_valid
  end

  it "is not valid without a geographic_boundary_type" do
    expect(build(:geographic_boundary, geographic_boundary_type_id: nil)).to_not be_valid
  end
end

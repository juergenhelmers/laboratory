require 'spec_helper'

describe ElectronicAddress do
  it "has a valid fixture" do
    expect(create(:electronic_address)).to be_valid
  end

  it "is not valid without an electronic_address_string" do
    expect(build(:electronic_address, electronic_address_string: nil)).to_not be_valid
  end
end

require 'spec_helper'

describe Person do

  it "has a valid fixture" do
    expect(create(:person)).to be_valid
  end

  it "to not create a person without a first name" do
    person = build(:person, current_first_name: nil)
    expect(person).to_not be_valid
  end
  it "to not create a person without a last name" do
    person = build(:person, current_last_name: nil)
    expect(person).to_not be_valid
  end
  it "to not create a person without a birth date" do
    person = build(:person, birth_date: nil)
    expect(person).to_not be_valid
  end

  it "to not create a person without a gender" do
    person = build(:person, gender_type_id: nil)
    expect(person).to_not be_valid
  end

  it "returns the full name" do
    person = create(:person)
    expect(person.current_full_name).to eq("#{person.current_first_name} #{person.current_middle_name[0]}. #{person.current_last_name}")
  end

  it "does not create a South African person with an invalid SA ID" do
    person = build(:person, passport_nationality: "South Africa", citizen_number: "12345")
    expect(person).to_not be_valid
  end

  it "requires a SA ID number present when passport nationality is South Africa" do
    person = build(:person, passport_nationality: "South Africa", citizen_number: nil)
    expect(person).to_not be_valid
  end

  it "creates a person with nationality of South Africa with a valid citizen ID" do
    Person.destroy_all
    person = build(:person, passport_nationality: "South Africa",citizen_number: "1305230123087")
    expect(person).to be_valid
  end
end

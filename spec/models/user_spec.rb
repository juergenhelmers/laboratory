require 'spec_helper'

describe User do

  it "has a valid fixture" do
    expect(create(:user)).to be_valid
  end

  it "is not valid without a firstname" do
    expect(build(:user, firstname: nil)).to_not be_valid
  end

  it "is not valid without a lastname" do
    expect(build(:user, lastname: nil)).to_not be_valid
  end

  it "is not valid without an email address" do
    expect(build(:user, email: nil)).to_not be_valid
  end

  it "is not valid without a password" do
    expect(build(:user, password: nil)).to_not be_valid
  end

end

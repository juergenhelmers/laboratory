require 'spec_helper'

describe TrainingClassType do
  it "has a valid factory" do
    expect(create(:training_class_type)).to be_valid
  end

  it "requires a name to be valid" do
    type = build(:training_class_type, name: nil)
    expect(type).to_not be_valid
  end

  it "cannot have a duplicate" do
    tct = create(:training_class_type, name: "sequencer")
    tct2 = build(:training_class_type, name: "sequencer")
    expect(tct2).to_not be_valid
  end
end

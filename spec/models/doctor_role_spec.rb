require 'spec_helper'

describe DoctorRole do
  it "has a valid fixture" do
    expect(create(:doctor_role)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:doctor_role, medical_id: nil)).to_not be_valid
  end

end

require 'spec_helper'

describe Category do
  it "has a valid fixture" do
    expect(create(:category)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:category, name: nil )).to_not be_valid
  end

  it "is not valid without a category_type" do
    expect(build(:category, category_type_id: nil )).to_not be_valid
  end

end

require 'spec_helper'

describe UnitOfMeasure do
  it "has a valid fixture" do
    expect(create(:unit_of_measure)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:unit_of_measure, name: nil)).to_not be_valid
  end

  it "is not valid with a dup name" do
    create(:unit_of_measure, name: "Hour")
    expect(build(:unit_of_measure, name: "Hour")).to_not be_valid
  end

  it "is not valid without an abbreviation" do
    expect(build(:unit_of_measure, abbreviation: nil)).to_not be_valid
  end

  it "is not valid with a dup abbreviation" do
    create(:unit_of_measure, abbreviation: "h")
    expect(build(:unit_of_measure, abbreviation: "h")).to_not be_valid
  end
end

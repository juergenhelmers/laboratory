require 'spec_helper'

describe FamilyRole do
  it "has a valid fixture" do
    expect(create(:family_role)).to be_valid
  end

  it "is not valid without an organization_id" do
    expect(build(:family_role, organization_id: nil)).to_not be_valid
  end

end

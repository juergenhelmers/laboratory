require 'spec_helper'

describe TelecommunicationsNumber do
  it "has a valid fixture" do
    expect(create(:telecommunications_number)).to be_valid
  end

  it "is not valid without a contact_number" do
    expect(build(:telecommunications_number, contact_number: nil)).to_not be_valid
  end

  it "is not valid with a country code longer than 4 digits" do
    expect(build(:telecommunications_number, country_code: "12345")).to_not be_valid
  end

end

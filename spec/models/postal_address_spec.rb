require 'spec_helper'

describe PostalAddress do
  it "has a valid fixture" do
    expect(create(:postal_address)).to be_valid
  end

  it "is not valid without an address1" do
    expect(build(:postal_address, address1: nil)).to_not be_valid
  end

end

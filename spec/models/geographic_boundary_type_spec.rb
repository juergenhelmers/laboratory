require 'spec_helper'

describe GeographicBoundaryType do
  it "has a valid fixture" do
    expect(create(:geographic_boundary_type)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:geographic_boundary_type, name: nil)).to_not be_valid
  end
  it "cannot have a duplicate" do
    gbt = create(:geographic_boundary_type, name: "City")
    gbt2 = build(:geographic_boundary_type, name: "City")
    expect(gbt2).to_not be_valid
  end
end

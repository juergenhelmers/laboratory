require 'spec_helper'

describe Organization do
  it "has a valid fixture" do
    expect(create(:organization)).to be_valid
  end

  it "is not valid without a name", focus: true do
    expect(build(:organization, current_name: nil)).to_not be_valid
  end
end

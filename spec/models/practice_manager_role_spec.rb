require 'spec_helper'

describe PracticeManagerRole do
  it "has a valid fixture" do
    expect(create(:practice_manager_role)).to be_valid
  end

  it "is not valid without a person_id" do
    expect(build(:practice_manager_role, person_id: nil)).to_not be_valid
  end

end

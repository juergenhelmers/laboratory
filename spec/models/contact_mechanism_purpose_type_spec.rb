require 'spec_helper'

describe ContactMechanismPurposeType do
  it "has a valid fixture" do
    expect(create(:contact_mechanism_purpose_type)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:contact_mechanism_purpose_type, name: nil)).to_not be_valid
  end

  it "cannot have a duplicate" do
    cmpt = create(:contact_mechanism_purpose_type, name: "private")
    cmpt2 = build(:contact_mechanism_purpose_type, name: "private")
    expect(cmpt2).to_not be_valid
  end

end

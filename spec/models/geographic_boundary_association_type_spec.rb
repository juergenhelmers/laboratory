require 'spec_helper'

describe GeographicBoundaryAssociationType do
  it "has a valid fixture" do
    expect(create(:geographic_boundary_association_type)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:geographic_boundary_association_type, name: nil)).to_not be_valid
  end

  it "is not valid with an existing name" do
    gcat1 = create(:geographic_boundary_association_type, name: 'test')
    gcat2 = build(:geographic_boundary_association_type, name: 'test')
    expect(gcat2).to_not be_valid
  end

end

require 'spec_helper'

describe NurseRole do
  it "has a valid fixture" do
    expect(create(:nurse_role)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:nurse_role, medical_id: nil)).to_not be_valid
  end

end

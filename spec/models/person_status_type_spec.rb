require 'spec_helper'

describe PersonStatusType do
  it "has a valid fixture" do
    expect(create(:person_status_type)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:person_status_type, name: nil)).to_not be_valid
  end

  it "is not valid with a duplicate name" do
    pst = create(:person_status_type, name: "alive")
    pst2 = build(:person_status_type, name: "alive")
    expect(pst2).to_not be_valid
  end

end

require 'spec_helper'

describe ContactMechanismElectronicAddress do

  it "has a valid fixture" do
    expect(create(:contact_mechanism_electronic_address)).to be_valid
  end

  it "is not valid without a contact_mechanism_type" do
    expect(build(:contact_mechanism_electronic_address, contact_mechanism_type_id: nil)).to_not be_valid
  end

end

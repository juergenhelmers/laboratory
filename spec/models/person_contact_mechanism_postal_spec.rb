require 'spec_helper'

describe PersonContactMechanismPostal do
  it "has a valid fixture" do
    expect(create(:person_contact_mechanism_postal)).to be_valid
  end

  it "requires a role_type" do
   expect(build(:person_contact_mechanism_postal, role_type_id: nil)).to_not be_valid
  end

  it "requires a contact_mechanism" do
   expect(build(:person_contact_mechanism_postal, contact_mechanism_postal_id: nil)).to_not be_valid
  end

  it "should have the current date assigned as from_date" do
    pcmp = create(:person_contact_mechanism_postal)
    expect(pcmp.from_date).to eq(Time.now.to_date)
  end

end

module TokenInputHelpers
  THEME = 'facebook'  # set to 'facebook' or 'mac' if using themes

  def token_input(locator, options)
    raise "Must pass a hash containing 'with'" unless options.is_a?(Hash) && options.has_key?(:with)

    theme = THEME.present? ? "-#{THEME}" : ""

    field = _find_fillable_field(options[:class]) # find the field that will ultimately be sent to the server, the one the user intends to fill in

    # Delete the existing token, if present
    begin
      # This xpath is finds a <ul class='token-input-list'/> followed by a <input id="ID"/>
      within(:xpath, "//ul[@class='token-input-list#{theme}' and following-sibling::input[@id='#{field[:id]}']]") do
        find(:css, ".token-input-delete-token").click
      end
    rescue Capybara::ElementNotFound
      # no-op
    end

    ti_field = _find_fillable_field(options[:class]) # now find the token-input
    ti_field.set(options[:with]) # 'type' in the value

    within(:css, ".token-input-dropdown#{theme}") do
      find(:xpath, "//li[contains(text(),'#{options[:with]}')]").click
    end
  end

  protected
    def _find_fillable_field(locator)
      find(:xpath, "//input*[class~='#{locator}']", visible: false)
    end
end

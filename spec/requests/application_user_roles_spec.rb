require 'spec_helper'

describe "ApplicationUserRoles" do
  describe "GET /application_user_roles" do
    it "does return code 200 when accessing the index page" do
      visit(application_user_roles_path)
      expect(page.status_code).to be(200)
    end

    it "lists the application_user_role name on index page" do
      application_user_role = create(:application_user_role)
      visit(application_user_roles_path)
      expect(page).to have_content(application_user_role.name)
    end

  end

  describe "GET /application_user_role/new" do
    it "can create a new application_user_role record" do
      visit(application_user_roles_path)
      click_link("New #{I18n.t('application_user_role.model_name')}")
      fill_in I18n.t('application_user_role.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Application User Role was successfully created")
      }
      expect(page).to have_content("MyString")
    end

    it "cannot create a new application_user_role record without a name" do
      visit(application_user_roles_path)
      click_link("New #{I18n.t('application_user_role.model_name')}")
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /application_user_role/edit" do
    it "can update an existing application_user_role" do
      application_user_role = create(:application_user_role)
      visit(edit_application_user_role_path(application_user_role))
      fill_in I18n.t('application_user_role.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("Application User Role was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /application_user_role/:id" do
    it "can display the details of a application_user_role" do
      application_user_role = create(:application_user_role)
      visit(application_user_role_path(application_user_role))
      expect(page).to have_content(application_user_role.name)
    end

    it "Deletes a application_user_role" do
      DatabaseCleaner.clean
      application_user_role = create(:application_user_role)
      visit(application_user_roles_path)
      expect{
        within "tr#application_user_role_#{application_user_role.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(ApplicationUserRole,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("Application User Role was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('application_user_role.index_heading')}"
      expect(page).to_not have_content application_user_role.name
    end

  end

end

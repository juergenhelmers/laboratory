# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


jQuery ->

  $('.js-datepicker input').datepicker ->
    format: 'yyyy/mm/dd'

  $(".form-inputs").bind('cocoon:after-insert', ->
    $('.js-datepicker input').datepicker ->
      format: 'yyyy/mm/dd')

#  call tokenInput on existing electronic_address fields and assign the css class "processed" to mark them
  $(".js-electronic_address .token_input").each ->
    el = $(this)
    el.addClass("processed")
    el.tokenInput el.data('url'),
      theme: "facebook",
      prePopulate: [el.data('pre')],
      propertyToSearch: "electronic_address_string"
      preventDuplicates: true,
      allowCustomEntry: true,
      tokenValue: "electronic_address_string"
      tokenLimit: '1'
      hintText: el.data('hint-text')
      noResultsText: el.data('no-result-text')
      searchingText: el.data('searching-text')

  $("#person_contact_mechanism_electronic_addresses").bind('cocoon:after-insert', ->
    $('.js-datepicker input').datepicker ->
      format: 'yyyy/mm/dd'
    $(".js-electronic_address .token_input").each ->
      el = $(this)
      unless (el.hasClass('processed'))
        el.tokenInput el.data('url'),
          theme: "facebook",
          propertyToSearch: "electronic_address_string"
          preventDuplicates: true,
          allowCustomEntry: true,
          tokenValue: "electronic_address_string"
          tokenLimit: '1'
          hintText: el.data('hint-text')
          noResultsText: el.data('no-result-text')
          searchingText: el.data('searching-text')
      )

  #  call tokenInput on existing diagnosis_type fields and assign the css class "processed" to mark them
  $(".token_input.diagnosis_type").each ->
    el = $(this)
    el.addClass("processed")
    el.tokenInput el.data('url'),
      theme: "facebook",
      prePopulate: [el.data('pre')],
      preventDuplicates: true,
      tokenLimit: '1'
      hintText: el.data('hint-text')
      noResultsText: el.data('no-result-text')
      searchingText: el.data('searching-text')
      resultsFormatter: (item) ->
        "<li><i>" + item.code + "</i> - " + item.name + "</li>"
      tokenFormatter: (item) ->
        "<li><p><i>" + item.code + "</i> - " + item.name + "</p></li>"
  # Use the after-insert hook cocoon provides to call tokenInput on new diagnosis_type fields
  # prevent a second call to tokenInput if they have the css class "processed"
  $('#patient_role').bind('cocoon:after-insert', ->
    $('.js-datepicker input').datepicker ->
      format: 'yyyy/mm/dd'
    $(".token_input.diagnosis_type").each ->
      el = $(this)
      unless (el.hasClass('processed'))
        el.tokenInput el.data('url'),
          theme: "facebook",
          prePopulate: [el.data('pre')],
          preventDuplicates: true,
          tokenLimit: '1'
          hintText: el.data('hint-text')
          noResultsText: el.data('no-result-text')
          searchingText: el.data('searching-text')
          resultsFormatter: (item) ->
            "<li><i>" + item.code + "</i> - " + item.name + "</li>"
          tokenFormatter: (item) ->
            "<li><p><i>" + item.code + "</i> - " + item.name + "</p></li>"
        )
  $(".token_input.postal").each ->
    el = $(this)
    el.addClass("processed")
    el.tokenInput el.data('url'),
      theme: "facebook",
      prePopulate: [el.data('pre')],
      preventDuplicates: true,
      tokenLimit: '1'
      hintText: el.data('hint-text')
      noResultsText: el.data('no-result-text')
      searchingText: el.data('searching-text')


  $('#person_contact_mechanisms_postal_address').bind('cocoon:after-insert', ->
    $('.js-datepicker input').datepicker ->
      format: 'yyyy/mm/dd'
    $(".token_input.postal_code").each ->
      el = $(this)
      unless (el.hasClass('processed'))
        el.tokenInput el.data('url'),
          theme: "facebook",
          prePopulate: [el.data('pre')],
          preventDuplicates: true,
          tokenLimit: '1'
          hintText: el.data('hint-text')
          noResultsText: el.data('no-result-text')
          searchingText: el.data('searching-text')
        )

  $("#uuid").popover({ html : true}) ->
    placement: 'right'
    animation: true


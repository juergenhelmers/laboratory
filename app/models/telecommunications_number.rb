class TelecommunicationsNumber < ActiveRecord::Base

  belongs_to :contact_mechanism_telecommunication


  validates :contact_number, presence: true
  validates :country_code, presence: true, length: { in: 1..4 } # might consider a more elaborate validation to check for it to be within 1..1876

end

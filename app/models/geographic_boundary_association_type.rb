class GeographicBoundaryAssociationType < ActiveRecord::Base

  has_many :geographic_boundary_association

  validates :name, presence: true, uniqueness: true

end

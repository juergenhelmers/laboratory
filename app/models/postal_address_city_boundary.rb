class PostalAddressCityBoundary < ActiveRecord::Base

  belongs_to :postal_address
  belongs_to :geographic_boundary

  validates :geographic_boundary_id, presence: true


end

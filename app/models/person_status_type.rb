class PersonStatusType < ActiveRecord::Base

  has_many :person_statuses
  has_many :people, -> { uniq }, through: :person_statuses

  validates :name, presence: true, uniqueness: true
end

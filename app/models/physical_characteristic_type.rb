class PhysicalCharacteristicType < ActiveRecord::Base

  has_many :physical_characteristics
  has_many :patient_roles, -> { uniq }, through: :physical_characteristics

  validates :name, presence: true, uniqueness: true

end

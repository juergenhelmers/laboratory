class PostalAddressSuburbBoundary < ActiveRecord::Base
  belongs_to :postal_address
  belongs_to :geographic_boundary
end

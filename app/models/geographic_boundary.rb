class GeographicBoundary < ActiveRecord::Base

  belongs_to :geographic_boundary_type
  #
  #has_many :postal_address_boundaries
  #has_many :postal_addresses, through: :postal_address_boundaries

  has_many :parent_child_relationships,
           class_name:   "GeographicBoundaryAssociation",
           foreign_key:  :child_id,
           dependent:    :destroy

  has_many :parents,  -> { uniq },
           through:   :parent_child_relationships,
           source:    :parent


  has_many :child_parent_relationships,
           :class_name            => "GeographicBoundaryAssociation",
           :foreign_key           => :parent_id,
           :dependent             => :destroy

  has_many :children, -> { uniq },
           through:     :child_parent_relationships,
           source:      :child


  #has_one :postal_address_suburb_boundary
  #has_one :postal_address, -> { uniq }, through: :postal_address_suburb_boundary

  accepts_nested_attributes_for :parent_child_relationships
  accepts_nested_attributes_for :child_parent_relationships

  validates :name,                        presence: true
  validates :geographic_boundary_type_id, presence: true

  scope :suburbs,       -> { joins(:geographic_boundary_type).where(geographic_boundary_types: {name: "Suburb"}) }
  scope :postal_codes,  -> { joins(:geographic_boundary_type).where(geographic_boundary_types: {name: "Postal Code"}) }


  def parent
    self.parents.first if self.parents.length > 0
  end

  def grandparent
    self.parents.first.parents.first if self.parents.length > 0 && self.parents.first.parents.length > 0
  end

  def grandchildren
    grandchildren = []
    self.children.each do |child|
      child.children.each do |gc|
        grandchildren.push(gc)
      end
    end
    return grandchildren
  end
end

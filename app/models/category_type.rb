class CategoryType < ActiveRecord::Base

  # a category_type has many categories
  has_many :categories

  # validations
  validates :name, presence: true, uniqueness: true

end

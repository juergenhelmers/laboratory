class GeographicBoundaryType < ActiveRecord::Base


  has_many :geographic_boundaries

  validates :name, presence: true, uniqueness: true

end

class ContactMechanismElectronicAddress < ActiveRecord::Base

  belongs_to :contact_mechanism_type
  has_one :electronic_address

  has_many :person_contact_mechanism_electronic_addresses
  has_many :people, -> { uniq }, through: :person_contact_mechanism_electronic_addresses


  validates :contact_mechanism_type_id, presence: true

  accepts_nested_attributes_for :electronic_address,  reject_if: proc { |attribute| attribute['electronic_address_string'].blank? }, allow_destroy: true

  after_initialize :add_child

   def add_child
     if self.new_record?
       self.electronic_address ||= self.build_electronic_address
     end
   end

   def autosave_associated_records_for_electronic_address
     if new_electronic_address = ElectronicAddress.where(electronic_address_string: electronic_address.electronic_address_string ).first_or_initialize
      self.electronic_address = new_electronic_address
     else
       self.electronic_address.save!
     end
   end

end

class GenderType < ActiveRecord::Base

  has_many :people

  validates :name, presence: true, uniqueness: true

end

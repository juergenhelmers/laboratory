class PhysicalCharacteristic < ActiveRecord::Base

  belongs_to :patient_role
  belongs_to :physical_characteristic_type
  belongs_to :unit_of_measure

  validates :from_date, presence: true

  before_validation :set_from_date


  def set_from_date
    self.from_date = Time.now.to_date
  end


end

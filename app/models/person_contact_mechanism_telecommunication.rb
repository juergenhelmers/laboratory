class PersonContactMechanismTelecommunication < ActiveRecord::Base


  belongs_to :person
  belongs_to :contact_mechanism_telecommunication
  belongs_to :role_type
  has_many   :person_contact_mechanism_purpose_telecommunications
  has_many   :contact_mechanism_pupose_types, through: :person_contact_mechanism_purpose_telecommunications

  validates :from_date, presence: true
  validates :role_type_id, presence: true

  accepts_nested_attributes_for :contact_mechanism_telecommunication, :reject_if => :all_blank
  accepts_nested_attributes_for :person_contact_mechanism_purpose_telecommunications, :reject_if => :all_blank, allow_destroy: true

  after_initialize :add_contact_mechanism_telecommunication
  before_validation :set_from_date

  def add_contact_mechanism_telecommunication
    if self.new_record?
      self.contact_mechanism_telecommunication ||= self.build_contact_mechanism_telecommunication
    end
  end

private
  def set_from_date
    self.from_date = Time.now.to_date unless self.from_date.present?
  end

end

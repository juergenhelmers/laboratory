class TrainingClassType < ActiveRecord::Base

  # see category model
  has_many :person_trainings
  has_many :people, through: :person_trainings

  # validations
  validates :name, presence: true, uniqueness: true

end

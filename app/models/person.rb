class Person < ActiveRecord::Base
  belongs_to :gender_type

  has_one :nurse_role, dependent: :destroy
  has_one :doctor_role, dependent: :destroy
  has_one :parent_role, dependent: :destroy
  has_one :payor_role, dependent: :destroy
  has_one :patient_role, dependent: :destroy
  has_one :child_role, dependent: :destroy
  has_one :customer_role, dependent: :destroy
  has_one :employee_role, dependent: :destroy
  has_one :contact_role, dependent: :destroy
  has_one :genetic_counselor_role, dependent: :destroy
  has_one :practice_manager_role, dependent: :destroy

  has_many :person_category_classifications
  has_many :categories, -> { uniq }, through: :person_category_classifications

  has_many :person_statuses
  has_many :person_status_types, -> { uniq }, through: :person_statuses

  has_many :person_marital_statuses
  has_many :marital_status_types, through: :person_marital_statuses

  has_many :person_languages
  has_many :languages, -> { uniq }, through: :person_languages

  has_many :person_trainings
  has_many :training_class_types, -> { uniq }, through: :person_trainings

  has_many :person_contact_mechanism_postals
  has_many :contact_mechanism_postals, -> { uniq }, through: :person_contact_mechanism_postals

  has_many :person_contact_mechanism_telecommunications
  has_many :contact_mechanism_telecommunications, -> { uniq }, through: :person_contact_mechanism_telecommunications

  has_many :person_contact_mechanism_electronic_addresses
  has_many :contact_mechanism_electronic_addresses, -> { uniq }, through: :person_contact_mechanism_electronic_addresses

  # validations
  validates_presence_of :current_first_name
  validates_presence_of :current_last_name
  validates_presence_of :birth_date
  validates_presence_of :gender_type_id
  # call a custom validator if the private function "is_south_african?" in this model returns true
  # validator "sa_id_number" is defined in app/validators/sa_id_number_validator.rb
  validates :citizen_number, :presence => true, uniqueness: true, :sa_id_number => true, if: :is_south_african?


  accepts_nested_attributes_for :doctor_role,  reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :child_role, allow_destroy: true
  accepts_nested_attributes_for :contact_role, allow_destroy: true
  accepts_nested_attributes_for :customer_role, allow_destroy: true
  accepts_nested_attributes_for :employee_role, allow_destroy: true
  accepts_nested_attributes_for :parent_role, allow_destroy: true
  accepts_nested_attributes_for :payor_role, allow_destroy: true
  accepts_nested_attributes_for :patient_role, allow_destroy: true
  accepts_nested_attributes_for :genetic_counselor_role, allow_destroy: true
  accepts_nested_attributes_for :nurse_role,  reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :practice_manager_role,  reject_if: :all_blank, allow_destroy: true

  accepts_nested_attributes_for :person_category_classifications, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :person_statuses, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :person_marital_statuses, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :person_languages, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :person_trainings, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :person_contact_mechanism_postals, :reject_if => proc { |pcp| pcp['role_type_id'].blank? }, allow_destroy: true
  accepts_nested_attributes_for :person_contact_mechanism_telecommunications, :reject_if => proc { |pcp| pcp['role_type_id'].blank? }, allow_destroy: true
  accepts_nested_attributes_for :person_contact_mechanism_electronic_addresses, :reject_if => proc { |pcp| pcp['role_type_id'].blank? }, allow_destroy: true



  before_create :set_uuid

  def current_full_name                      # return the full name of a person (Used in heading of person detail view)
    if current_middle_name.present?
      current_middle_initial = "#{current_middle_name[0].capitalize}."
      [current_first_name, current_middle_initial, current_last_name].join(' ')
    else
      [current_first_name, current_last_name].join(' ')
    end
  end


private

  def set_uuid
    self.uuid = UUIDTools::UUID.timestamp_create().to_s    # generate the UUID code
    qr = RQRCode::QRCode.new( self.uuid, :size => 5, :level => :h )
    qr.to_img.resize(90, 90).save("#{Rails.root}/public/system/barcodes/people/qr/#{self.uuid}.png")
    # TODO create a background generator to generate different sizes/formats for different purposes.
    # 80x80 is barely readable from the screen using a smartphone...
  end

  def is_south_african?                      # return true if person is South African
    passport_nationality == "South Africa"   # define requirement for returning true
  end
end

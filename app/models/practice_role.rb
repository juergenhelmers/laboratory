class PracticeRole < ActiveRecord::Base

	belongs_to :organization

	validates :organization_id, presence: true

  has_many :doctor_practice_relationships
  has_many :doctor_roles, -> { uniq },  through: :doctor_practice_relationships

  has_many :nurse_practice_relationships
  has_many :nurse_roles, -> { uniq },  through: :nurse_practice_relationships

  has_many :genetic_counselor_practice_relationships
  has_many :genetic_counselor_roles, -> { uniq },  through: :genetic_counselor_practice_relationships

  has_many :practice_manager_practice_relationships
  has_many :practice_manager_roles, -> { uniq },  through: :practice_manager_practice_relationships

  accepts_nested_attributes_for :doctor_practice_relationships, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :nurse_practice_relationships, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :genetic_counselor_practice_relationships, allow_destroy: true, reject_if: :all_blank

	
end

class PersonContactMechanismPurposeElectronicAddress < ActiveRecord::Base

  belongs_to :contact_mechanism_purpose_type
  belongs_to :person_contact_mechanism_electronic_address

end

class FamilyRole < ActiveRecord::Base

	belongs_to :organization

	validates :organization_id, presence: true


  has_many :parent_family_relationships
  has_many :parent_roles, -> { uniq },  through: :parent_family_relationships

  has_many :child_family_relationships
  has_many :child_roles, -> { uniq },  through: :child_family_relationships

  accepts_nested_attributes_for :child_family_relationships, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :parent_family_relationships, allow_destroy: true, reject_if: :all_blank

end

class DoctorRole < ActiveRecord::Base

  belongs_to :person
  has_many :patient_doctor_relationships
  has_many :patient_roles, -> { uniq },  through: :patient_doctor_relationships

  has_many :doctor_contact_relationships
  has_many :contact_roles, -> { uniq },  through: :doctor_contact_relationships

  has_many :doctor_practice_relationships
  has_many :practice_roles, -> { uniq },  through: :doctor_practice_relationships

  accepts_nested_attributes_for :patient_doctor_relationships, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :doctor_contact_relationships, allow_destroy: true, reject_if: :all_blank

  validates :medical_id,  presence: true #, length: { minimum: 4 }

end

class Category < ActiveRecord::Base
  has_many :person_category_classifications
  has_many :people, -> { uniq }, through: :person_category_classifications

  belongs_to :category_type

  validates :name, presence: true
  validates :category_type_id, presence: true

end

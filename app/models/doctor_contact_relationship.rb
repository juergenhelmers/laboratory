class DoctorContactRelationship < ActiveRecord::Base
  belongs_to :doctor_role
  belongs_to :contact_role

  validates :from_date, presence: true
end

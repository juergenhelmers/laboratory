class ContactMechanismPurposeType < ActiveRecord::Base

  has_many :person_contact_mechanism_purposes

  validates :name, presence: true, uniqueness: true

end

class ParentRole < ActiveRecord::Base

  belongs_to :person

  has_many :parent_child_relationships
  has_many :child_roles, -> { uniq },  through: :parent_child_relationships

  has_many :parent_family_relationships
  has_many :family_roles, -> { uniq },  through: :parent_family_relationships

end

class GeographicBoundaryAssociation < ActiveRecord::Base

  belongs_to :parent, class_name: "GeographicBoundary"
  belongs_to :child,  class_name: "GeographicBoundary"

  belongs_to :geographic_boundary_association_type

  #validates :geographic_boundary_association_type_id, presence: true

end

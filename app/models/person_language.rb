class PersonLanguage < ActiveRecord::Base

  belongs_to :person
  belongs_to :language

  validates_uniqueness_of :language_id, :scope => :person_id, message: "has already been assigned."
  #validates :competency_level, presence: true

end

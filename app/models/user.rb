class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, :registerable, :rememberable
  devise :database_authenticatable, :recoverable, :trackable, :validatable

  # we might want to make this a has_many through relationship as so a user can have many roles...
  belongs_to :application_user_role

  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :email, presence: true, uniqueness: true
  validates :company_id, presence: true, uniqueness: true
  validates :application_user_role_id, presence: true

  def fullname
    [firstname, lastname].join(' ')
  end

  def admin?
    self.application_user_role.name == "Admin"
  end

  def ceo?
    self.application_user_role.name == "CEO"
  end

  def cso?
    self.application_user_role.name == "CSO"
  end

  def csr?
    self.application_user_role.name == "CSR"
  end

  def data_steward
    self.application_user_role.name == "Data Steward"
  end

  def lab_tech?
    self.application_user_role.name == "LaboratoryTtechnician"
  end

  def on_boarder?
    self.application_user_role.name == "On-Boarder"
  end

end

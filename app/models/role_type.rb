class RoleType < ActiveRecord::Base

  belongs_to :parent, :class_name => "RoleType"
  has_many :role_types, :foreign_key => "parent_id"

  has_many :person_contact_mechanism_electronic_addresses
  has_many :person_contact_mechanism_telecommunications
  has_many :person_contact_mechanism_postals

  validates :name,        presence: true, uniqueness: true
  validates :parent_id,   presence: true, inclusion: {:in => [1,2]}, if: :is_a_child

  scope :parent_statuses, -> {where(parent_id: nil).where("id <= ?", 2)}

  def is_a_child
    RoleType.where(name: I18n.t('role_type.person_role')).present? && RoleType.where(name: I18n.t('role_type.organization_role')).present?
  end

end

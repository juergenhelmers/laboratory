class PracticeManagerRole < ActiveRecord::Base

  belongs_to :person

  has_many :practice_manager_practice_relationships
  has_many :practice_roles, -> { uniq },  through: :practice_manager_practice_relationships


  validates :person_id, presence: true

end

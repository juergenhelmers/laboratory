class Language < ActiveRecord::Base

  has_many :person_languages
  has_many :people, through: :person_languages

  validates :name, presence: true, uniqueness: true
  validates :abbreviation,  presence: true, length: { in: 2..2 }

  before_save :upcase_language_code

  private
  def upcase_language_code
    self.abbreviation = self.abbreviation.upcase
  end
end

class Ability
  include CanCan::Ability

  def initialize(user)
    if user.admin?
      can :manage, :all
    elsif user.ceo?
      can :read, Person
      #can :read, Sample
      #can :read, SampleBatch
    elsif user.cso?
      can :read, Person
      can :update, Person
      #can :manage, Sample
      #can :manage, SampleBatch
    elsif user.csr?
      can [:create, :update, :read], Person
      can :manage, Organization
      # TODO: how to handle the deletion of records? Should a CSR be able to do that?
    elsif user.data_steward?
      can :manage, Category
      can :manage, CategoryType
      can :manage, ContactMechanismPurposeType
      can :manage, ContactMechanismType
      can :manage, DiagnosisType
      can :manage, Diagnosis
      can :manage, GenderType
      can :manage, GeographicBoundary
      can :manage, GeographicBoundaryAssociationType
      can :manage, Language
      can :manage, MaritalStatusType
      can :manage, PhysicalCharacterisitic
      can :manage, PhysicalCharacterisiticType
      can :manage, RoleType
      can :manage, TrainingClassType
      can :manage, UnitOfMeasure
    elsif user.lab_tech?
      #can :manage, Sample
      #can :manage, SampleBatch
    elsif user.on_boarder?
      can :manage, Person
      can :manage, Organization
    end

  end
end
class UnitOfMeasure < ActiveRecord::Base

  has_many :physical_characteristics

  validates :name, presence: true, uniqueness:true
  validates :abbreviation, presence: true, uniqueness:true

end

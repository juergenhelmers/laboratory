class PatientRole < ActiveRecord::Base

  belongs_to :person

  has_many :diagnoses
  has_many :diagnosis_types, -> { uniq }, through: :diagnosis_types

  has_many :physical_characteristics
  has_many :physical_characteristic_types, -> { uniq }, through: :physical_characteristics

  has_many :patient_doctor_relationships
  has_many :doctor_roles, -> { uniq },  through: :patient_doctor_relationships

  has_many :patient_nurse_relationships
  has_many :nurse_roles, -> { uniq },  through: :patient_nurse_relationships

  has_many :patient_genetic_counselor_relationships
  has_many :genetic_counselor_roles, -> { uniq },  through: :patient_genetic_counselor_relationships

  accepts_nested_attributes_for :diagnoses, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :physical_characteristics, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :patient_doctor_relationships, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :patient_nurse_relationships, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :patient_genetic_counselor_relationships, allow_destroy: true, reject_if: :all_blank

  # TODO make this work with other associations like patient_nurse_relationship
  #validates :patient_doctor_relationships, presence: { :message => 'there needs to be at least one doctor' }


end

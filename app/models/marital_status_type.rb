class MaritalStatusType < ActiveRecord::Base

  # see category
  has_many :person_marital_statuses
  has_many :people, through: :person_marital_statuses

  # validations
  validates :name, presence: true, uniqueness: true
end

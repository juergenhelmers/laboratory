class PersonMaritalStatus < ActiveRecord::Base
  belongs_to :person
  belongs_to :marital_status_type

  # validations
  validates_uniqueness_of :marital_status_type_id, :scope => :person_id, message: "has already been assigned."


  before_save :set_sequence_id


private

  def set_sequence_id
    if self.person.person_marital_statuses.present?
      if self.from_date.blank?
        last_pms = self.person.person_marital_statuses.order('marital_status_seq_id ASC').last

        self.from_date = Time.now.to_date
        self.marital_status_seq_id = last_pms.marital_status_seq_id + 1
        update_sequence_id(last_pms.id)
      else
        self.thru_date = Time.now.to_date - 1.day
      end
    else
      self.marital_status_seq_id = 1      # only executed on first record
      self.from_date = Time.now.to_date

    end
  end

  def update_sequence_id(id)
    pms = PersonMaritalStatus.find(id)
    pms.thru_date = Time.now.to_date - 1.day
    pms.save!
  end
end

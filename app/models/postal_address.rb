class PostalAddress < ActiveRecord::Base

  belongs_to :contact_mechanism_postal

  has_one :postal_address_postal_code_boundary
  has_one :geographic_boundary, -> { uniq }, through: :postal_address_postal_code_boundary

  has_one :postal_address_city_boundary
  has_one :geographic_boundary, -> { uniq }, through: :postal_address_city_boundary

  has_one :postal_address_suburb_boundary
  has_one :geographic_boundary, -> { uniq }, through: :postal_address_suburb_boundary

  has_one :postal_address_country_boundary
  has_one :geographic_boundary, -> { uniq }, through: :postal_address_country_boundary

  validates :address1, presence: true

  accepts_nested_attributes_for :postal_address_postal_code_boundary
  accepts_nested_attributes_for :postal_address_city_boundary
  accepts_nested_attributes_for :postal_address_suburb_boundary
  accepts_nested_attributes_for :postal_address_country_boundary

  after_initialize :add_country

  def add_country
    if self.new_record?
      self.postal_address_postal_code_boundary ||= self.build_postal_address_postal_code_boundary
      self.postal_address_city_boundary ||= self.build_postal_address_city_boundary
      self.postal_address_suburb_boundary ||= self.build_postal_address_suburb_boundary
      self.postal_address_country_boundary ||= self.build_postal_address_country_boundary
    end
  end

end

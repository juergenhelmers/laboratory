class Organization < ActiveRecord::Base
  
  has_one :family_role
  has_one :practice_role

  validates :current_name, presence: true

end

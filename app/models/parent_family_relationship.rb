class ParentFamilyRelationship < ActiveRecord::Base

  belongs_to :parent_role
  belongs_to :family_role

  validates :from_date, presence: true

  before_validation :set_from_date


  private

  def set_from_date
    self.from_date ||= Time.now.to_date
  end

end

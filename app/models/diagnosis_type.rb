class DiagnosisType < ActiveRecord::Base

  has_many :diagnoses
  has_many :patient_roles, -> { uniq }, through: :diagnosis_types

  validates :name, presence: true
  validates :code, presence: true, uniqueness: true
end

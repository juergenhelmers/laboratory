class NursePracticeRelationship < ActiveRecord::Base

  belongs_to :nurse_role
  belongs_to :practice_role

  validates :from_date, presence: true
  
  before_validation :set_from_date


  private

  def set_from_date
    self.from_date ||= Time.now.to_date
  end

end

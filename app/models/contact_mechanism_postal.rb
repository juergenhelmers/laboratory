class ContactMechanismPostal < ActiveRecord::Base

  belongs_to :contact_mechanism_type

  has_one :postal_address

  has_many :person_contact_mechanism_postals
  has_many :people, -> { uniq }, through: :person_contact_mechanism_postals


  validates :contact_mechanism_type_id, presence: true

  accepts_nested_attributes_for :postal_address,  reject_if: proc { |attribute| attribute['address1'].blank? }, allow_destroy: true

  after_initialize :add_child

   def add_child
     if self.new_record?
       self.postal_address ||= self.build_postal_address
     end
   end
end

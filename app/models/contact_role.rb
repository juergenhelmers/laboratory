class ContactRole < ActiveRecord::Base

  belongs_to :person

  has_many :doctor_contact_relationships
  has_many :doctor_roles, -> { uniq },  through: :doctor_contact_relationships

end

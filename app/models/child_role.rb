class ChildRole < ActiveRecord::Base

  belongs_to :person

  has_many :parent_child_relationships
  has_many :parent_roles, -> { uniq },  through: :parent_child_relationships

  has_many :child_family_relationships
  has_many :family_roles, -> { uniq },  through: :child_family_relationships

  accepts_nested_attributes_for :child_family_relationships, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :parent_child_relationships, allow_destroy: true, reject_if: :all_blank
end

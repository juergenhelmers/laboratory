class PersonCategoryClassification < ActiveRecord::Base

  belongs_to :category
  belongs_to :person

  validates_uniqueness_of :category_id, :scope => :person_id, message: "has already been assigned."

end

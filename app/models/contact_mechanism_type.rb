class ContactMechanismType < ActiveRecord::Base

  has_many :contact_mechanism_postals

  validates :name, presence: true, uniqueness: true

  scope :electronic, -> {where.not(name: "#{I18n.t('contact_mechanism_type.phone')}").where.not(name: "#{I18n.t('contact_mechanism_type.postal_address')}")}

end

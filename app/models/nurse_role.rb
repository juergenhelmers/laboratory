class NurseRole < ActiveRecord::Base

  belongs_to :person
  
  has_many :patient_nurse_relationships
  has_many :patient_roles, -> { uniq },  through: :patient_nurse_relationships

  has_many :nurse_practice_relationships
  has_many :practice_roles, -> { uniq },  through: :nurse_practice_relationships

  validates :medical_id,  presence: true

end

class PersonStatus < ActiveRecord::Base

  belongs_to :person
  belongs_to :person_status_type

  # make sure to not add the same person_status_type twice
  validates_uniqueness_of :person_status_type_id, :scope => :person_id, message: "has already been assigned."

  # so far the set_status_date is hardcoded to the time the new record is created and therefore redundant
  # as this information is already saved in created_at. Could set the field manually in the form.
  before_save :set_status_date



  private
    def set_status_date
      if self.person_status_type_id.present?
        self.person_status_date = Time.now.to_date   # set the status date if status present
      end
    end
end

class ContactMechanismTelecommunication < ActiveRecord::Base

  belongs_to :contact_mechanism_type

  has_one :telecommunications_number

  has_many :person_contact_mechanism_telecommunications
  has_many :people, -> { uniq }, through: :person_contact_mechanism_telecommunications


  validates :contact_mechanism_type_id, presence: true

  accepts_nested_attributes_for :telecommunications_number,  reject_if: proc { |attribute| attribute['contact_number'].blank? }, allow_destroy: true

  after_initialize :add_child

   def add_child
     if self.new_record?
       self.telecommunications_number ||= self.build_telecommunications_number
     end
   end

end

class GeneticCounselorRole < ActiveRecord::Base

  belongs_to :person

  has_many :patient_genetic_counselor_relationships
  has_many :patient_roles, -> { uniq },  through: :patient_genetic_counselor_relationships

  has_many :genetic_counselor_practice_relationships
  has_many :practice_roles, -> { uniq },  through: :genetic_counselor_practice_relationships

end

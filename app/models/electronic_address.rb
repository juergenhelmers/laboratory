class ElectronicAddress < ActiveRecord::Base

  belongs_to :contact_mechanism_electronic_address
  validates :electronic_address_string, presence: true


  def self.tokens(query)
    electronic_address = where("electronic_address_string LIKE ?", "%#{query}%").order('electronic_address_string asc')
    if electronic_address.empty?
      [{id: "<<<#{query}>>>", electronic_address_string: "#{query}"}]
    else
      electronic_address
    end
  end

end

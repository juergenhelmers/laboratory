class PersonTraining < ActiveRecord::Base
  belongs_to :person
  belongs_to :training_class_type

  # validations
  validates_presence_of :from_date, :training_class_type_id
  validates_uniqueness_of :training_class_type_id, :scope => :person_id, message: "has already been assigned."

end

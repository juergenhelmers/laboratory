class PersonDecorator < Draper::Decorator
  delegate_all

  def show_title
    full_name = []
    full_name << model.current_personal_title   if model.current_personal_title.present?
    full_name << model.current_full_name
    full_name = full_name.join(" ")
    full_name += ", #{model.current_suffix}"    if model.current_suffix.present?
    return full_name
  end

  def uuid_popover(model)
    qr_code_file = "#{Rails.root}/public/system/barcodes/people/qr/#{model.uuid}.png"
    File.exist?(qr_code_file) ? h.image_tag("/system/barcodes/people/qr/#{model.uuid}.png") : "No QR code has been generated."
  end

  def first_name
    dyn_list(model.current_first_name, "current_first_name")
  end

  def middle_name
    dyn_list(model.current_middle_name, "current_middle_name")
  end

  def last_name
    dyn_list(model.current_last_name, "current_last_name")
  end

  def title
    dyn_list(model.current_personal_title, "current_personal_title")
  end

  def suffix
    dyn_list(model.current_suffix, "current_suffix")
  end

  def nickname
    dyn_list(model.current_nickname, "current_nickname")
  end

  def birth_date
    dyn_list(model.birth_date, "birth_date")
  end

  def citizen_number
    dyn_list(model.citizen_number, "citizen_number")
  end

  def nationality
    dyn_list(model.passport_nationality, "passport_nationality")
  end

  def ethnicity
    dyn_list(model.ethnicity, "ethnicity")
  end

  def maiden_name
    dyn_list(model.mothers_maiden_name, "mothers_maiden_name")
  end

  def comment
    dyn_list(model.comment, "comment")
  end

  def gender
    h.content_tag(:dt, "#{I18n.t('gender_type.name')}") + h.content_tag(:dd, model.gender_type.name)
  end

  def nurse
    if model.nurse_role.present?
      h.content_tag(:dt, "#{I18n.t('nurse_role.medical_id')}") + h.content_tag(:dd, model.nurse_role.medical_id)
    end
  end

  def doctor
    if model.doctor_role.present?
      string = h.content_tag(:dt, "#{I18n.t('doctor_role.medical_id')}") + h.content_tag(:dd, model.doctor_role.medical_id)
      string = list_associated_people(model.doctor_role.contact_roles, string, "doctor_role.contacts")
      return string
    end
  end

  # refactored to use private function as all return values are based on model based I18n strings
  def genetic_counselor
    is_role(model.genetic_counselor_role, "genetic_counselor")
  end

  def patient
    return if model.patient_role.blank?
    h.content_tag(:dt, I18n.t('patient_role.model_name.')) +
    h.content_tag(:dd, I18n.t('patient_role.is_patient'))
  end

  def patient_doctor
    return if model.patient_role.blank?
    if model.patient_role.doctor_roles.present?
      h.content_tag(:dt, I18n.t('person.doctor')) +
      h.content_tag(:dd) do
        h.content_tag(:ul) do
          model.patient_role.doctor_roles.map do |doc|
            associated_role(doc)
          end.join.html_safe
        end
      end
    end
  end

  def patient_nurse
    return if model.patient_role.blank?
    if model.patient_role.nurse_roles.present?
      h.content_tag(:dt, I18n.t('person.nurse')) +
      h.content_tag(:dd) do
        h.content_tag(:ul) do
          model.patient_role.nurse_roles.map do |nurse|
            associated_role(nurse)
          end.join.html_safe
        end
      end
    end
  end

  def patient_genetic_counselor
    return if model.patient_role.blank?
    if model.patient_role.genetic_counselor_roles.present?
      h.content_tag(:dt, I18n.t('person.genetic_counselor')) +
      h.content_tag(:dd) do
        h.content_tag(:ul) do
          model.patient_role.genetic_counselor_roles.map do |genetic_counselor|
            associated_role(genetic_counselor)
          end.join.html_safe
        end
      end
    end
  end

  def patient_diagnoses
    return if model.patient_role.blank?
    if model.patient_role.diagnoses.present?
      h.content_tag(:dt, I18n.t('person.diagnosis').pluralize) +
      h.content_tag(:dd) do
        h.content_tag(:ul) do
          model.patient_role.diagnoses.map do |diagnosis|
            h.content_tag(:li, diagnosis.diagnosis_type.name)
          end.join.html_safe
        end
      end
    end
  end

  def patient_physical_characteristics
    return if model.patient_role.blank?
    if model.patient_role.physical_characteristics.present?
      h.content_tag(:dt, I18n.t('person.physical_characteristic').pluralize) +
      h.content_tag(:dd) do
        h.content_tag(:ul) do
          model.patient_role.physical_characteristics.map do |phys_char|
            if phys_char.unit_of_measure.blank?
              h.content_tag(:li, "#{phys_char.physical_characteristic_type.name} - #{phys_char.value}")
            else
              h.content_tag(:li, "#{phys_char.physical_characteristic_type.name} - #{phys_char.value} #{phys_char.unit_of_measure.abbreviation}")
            end
          end.join.html_safe
        end
      end
    end
  end


  def electronic_addresses
    return if model.contact_mechanism_electronic_addresses.blank?
    h.content_tag(:dt, I18n.t('person.electronic_address').pluralize) +
    h.content_tag(:dd) do
      model.contact_mechanism_electronic_addresses.map do |cmea|
        format_electronic_addresses(cmea)
      end.join("</br>").html_safe
    end
  end

  def postal_addresses
    return if model.contact_mechanism_postals.blank?
    h.content_tag(:dt, I18n.t('person.postal_addresses').pluralize) +
    h.content_tag(:dd) do
      model.contact_mechanism_postals.map do |cmp|
        h.content_tag(:address) do
          format_address(cmp.postal_address) +
          h.content_tag(:br) +
          format_geographic_boundaries(cmp.postal_address)
        end
      end.join.html_safe
    end
  end


  def telecommunications_numbers
    return if model.person_contact_mechanism_telecommunications.blank?
    h.content_tag(:dt, I18n.t('person.telecommunications_number').pluralize) +
    h.content_tag(:dd) do
     format_phone_numbers(model.person_contact_mechanism_telecommunications)
    end
  end

  def child
    string = is_role(model.child_role, "child")
    if model.child_role.present?
      string = list_associated_people(model.child_role.parent_roles, string, "parent_role.parents")
    end
    return string
  end

  def parent_gender(parent)
    if parent.person.gender_type.name == "male"
      "father"
    elsif parent.person.gender_type.name == "female"
      "mother"
    else
      parent.person.gender_type.name
    end
  end

  def parent
    is_role(model.parent_role, "parent")
  end


  def contact
    is_role(model.contact_role, "contact")
  end
  
  def customer
    is_role(model.customer_role, "customer")
  end
  
  def payor
    is_role(model.payor_role, "payor")
  end

  def employee
    is_role(model.employee_role, "employee")
  end

  def uuid
    if model.uuid.present?
      h.content_tag(:dt, I18n.t('person.uuid'))  +
      h.content_tag(:dd, class: "uuid") do
        h.content_tag(:button, model.uuid , class: "btn btn-mini", id: "uuid", data: {title: I18n.t('person.qr_code'),content: "#{h.raw uuid_popover(model)}"})
      end
    end
  end

  def marital_statuses(status)
    mar_display = ""
    if status.from_date.present?
      mar_display << "(start: #{status.from_date}"
      if status.thru_date.present?
        mar_display << " to: "
        mar_display << status.thru_date.to_s
        mar_display << ")"
      else
        mar_display << ")"
      end
    end
  end
  def language_level(language)
    lan_display = "(#{language.competency_level}"
    if language.from_date.present?
      lan_display << ", start: #{language.from_date}"
      if language.thru_date.present?
        lan_display << " to: "
        lan_display << language.thru_date.to_s
        lan_display << ")"
      else
        lan_display << ")"
      end
    else
    lan_display << ")"
    end
  end

  def training_dates(training)
    display = "(start: #{training.from_date}"
    if training.thru_date.present?
      display << " to: "
      display << training.thru_date.to_s
      display << ")"
    else
      display << ")"
    end
  end

  def categories
    return if model.categories.empty?
    list_associations(model.categories, "category.model_name")
  end

  def person_statuses
    return if model.person_statuses.empty?
    h.content_tag(:dt, I18n.t('person_status_type.model_name').pluralize) +
    h.content_tag(:dd) do
      h.content_tag(:ul) do
        model.person_statuses.map do |cat|
          h.content_tag(:li, "#{cat.person_status_type.name}")
        end.join.html_safe
      end
    end
  end

  def languages
    return if model.person_languages.empty?
    h.content_tag(:dt, I18n.t('language.model_name').pluralize) +
    h.content_tag(:dd) do
      h.content_tag(:ul) do
        model.person_languages.map do |lang|
          h.content_tag(:li, "#{lang.language.name} #{language_level(lang)}")
        end.join.html_safe
      end
    end
  end

  def trainings
    return if model.person_trainings.empty?
    h.content_tag(:dt, I18n.t('training_class_type.model_name').pluralize) +
    h.content_tag(:dd) do
      h.content_tag(:ul) do
        model.person_trainings.map do |training|
          h.content_tag(:li, "#{training.training_class_type.name} #{training_dates(training)}")
        end.join.html_safe
      end
    end
  end

  def marriages
    return if model.person_marital_statuses.empty?
    h.content_tag(:dt, I18n.t('marital_status_type.model_name').pluralize) +
    h.content_tag(:dd) do
      h.content_tag(:ul) do
        model.person_marital_statuses.order("marital_status_seq_id ASC").map do |mariage|
          h.content_tag(:li, "#{mariage.marital_status_type.name} #{marital_statuses(mariage)}")
        end.join.html_safe
      end
    end
  end



private

  def format_electronic_addresses(ea)
    if ea.contact_mechanism_type.name == "Email"
      h.mail_to "#{ea.electronic_address.electronic_address_string}", "#{ea.electronic_address.electronic_address_string} (Email)"
    elsif ea.contact_mechanism_type.name == "Twitter Handle"
      h.link_to "@#{ea.electronic_address.electronic_address_string} (Twitter)", "http://www.twitter.com/#{ea.electronic_address.electronic_address_string}", target: "_blank"
    elsif ea.contact_mechanism_type.name == "Facebook Handle"
      h.link_to "#{ea.electronic_address.electronic_address_string} (Facebook)", "http://www.facebook.com/#{ea.electronic_address.electronic_address_string}", target: "_blank"
    else
      h.content_tag(:div) do
        "#{ea.electronic_address.electronic_address_string} (Other)"
      end
    end
  end

  def format_phone_numbers(pcmt)
    phone_numbers = []
    pcmt.map do |phone|
      pn = ""
      if phone.contact_mechanism_telecommunication.telecommunications_number.country_code.present?
        pn += "+#{phone.contact_mechanism_telecommunication.telecommunications_number.country_code}"
      end
      if phone.contact_mechanism_telecommunication.telecommunications_number.area_code.present?
        ac = phone.contact_mechanism_telecommunication.telecommunications_number.area_code.to_s
        if ac[0] == "0"
          pn += "(#{ac[0]})#{ac[1..-1]}"
        else
          pn += phone.contact_mechanism_telecommunication.telecommunications_number.area_code
        end
      end
      if phone.contact_mechanism_telecommunication.telecommunications_number.contact_number.present?
        pn += " #{phone.contact_mechanism_telecommunication.telecommunications_number.contact_number}"
      end
      pn += " (#{phone.role_type.name})"
      phone_numbers.push(pn)
    end
    return phone_numbers.join("</br>").html_safe
  end

  def format_address(pa)
    string = []
    if pa.address1.present?
      string.push(pa.address1)
    end
    if pa.address2.present?
      string.push(pa.address2)
    end
    if pa.address3.present?
      string.push(pa.address3)
    end
    return string.join("</br>").html_safe
  end

  def format_geographic_boundaries(pa)
    string = []
    if pa.postal_address_suburb_boundary.geographic_boundary.present?
      string.push(pa.postal_address_suburb_boundary.geographic_boundary.name)
    end
    if pa.postal_address_postal_code_boundary.geographic_boundary.present?
      string.push(pa.postal_address_postal_code_boundary.geographic_boundary.name)
    end
    if pa.postal_address_city_boundary.geographic_boundary.present?
      string.push(pa.postal_address_city_boundary.geographic_boundary.name)
    end
    if pa.postal_address_country_boundary.geographic_boundary.present?
      string.push(pa.postal_address_country_boundary.geographic_boundary.name)
    end
    return string.join("</br>").html_safe
  end


  def associated_role(parent)
    h.content_tag(:li, Person.find(parent.person_id).current_full_name)
  end

  def list_associations(association, header)
    h.content_tag(:dt, "#{I18n.t(header).pluralize}") +
    h.content_tag(:dd) do
      h.content_tag(:ul) do
        association.map do |cat|
          h.content_tag(:li, "#{cat.name}")
        end.join.html_safe
      end
    end
  end

  def list_associated_people(association, string, header)
    if association.length > 0
      string +=
      h.content_tag(:dt, "#{I18n.t("#{header}")}") +
      h.content_tag(:dd) do
        h.content_tag(:ul) do
          association.map do |a|
            h.content_tag(:li, "#{a.person.current_full_name}")
          end.join.html_safe
        end
      end
    end
    return string
  end


  def none_given(field)
    field.present? ? field : h.content_tag(:span, "none given", class: "dec_grey")
  end

  def dyn_list(attribute, field_name)
    h.content_tag(:dt, I18n.t("person.#{field_name}")) + h.content_tag(:dd, none_given(attribute))
  end

  def is_role(model,role)
    if model.present?
      h.content_tag(:dt, I18n.t("#{role}_role.model_name")) + h.content_tag(:dd, I18n.t("#{role}_role.is_#{role}"))
    end
  end

end

class GeographicBoundaryDecorator < Draper::Decorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end
  def type_filters
    GeographicBoundaryType.order('name asc').map(&:name).each do |filter|
      h.link_to I18n.t("geographic_boundary.filter.#{filter}"), geographic_boundaries_path(:filter => "#{filter}".titleize)
    end
  end
end

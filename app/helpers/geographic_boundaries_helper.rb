module GeographicBoundariesHelper

  def type_filters
    string = "| "
    string += link_to t("geographic_boundary.filter.all"), geographic_boundaries_path
    string += " |"
    GeographicBoundaryType.order('name asc').map(&:name).each do |filter|
      string += link_to t("geographic_boundary.filter.#{filter.downcase.gsub(" ", "_")}"), geographic_boundaries_path(:filter => "#{filter}".titleize)
      string += " |"
    end
    string.html_safe
  end

end

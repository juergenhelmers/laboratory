module PeopleHelper

  def postal_address_id
    if ContactMechanismType.where(name: "Postal Address").present?
      ContactMechanismType.where(name: "Postal Address").first.id
    else
      return "1"
    end
  end
  def phone_id
    if ContactMechanismType.where(name: "Phone").present?
      ContactMechanismType.where(name: "Phone").first.id
    else
      return "2"
    end
  end
end

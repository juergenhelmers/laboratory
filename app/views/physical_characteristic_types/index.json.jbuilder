json.array!(@physical_characteristic_types) do |physical_characteristic_type|
  json.extract! physical_characteristic_type, :name, :description
  json.url physical_characteristic_type_url(physical_characteristic_type, format: :json)
end
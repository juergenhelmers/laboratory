json.array!(@gender_types) do |gender_type|
  json.extract! gender_type, :name, :description
  json.url gender_type_url(gender_type, format: :json)
end
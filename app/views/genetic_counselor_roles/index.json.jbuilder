json.array!(@genetic_counselor_roles) do |genetic_counselor_role|
  json.extract! genetic_counselor_role, :person_id
  json.url genetic_counselor_role_url(genetic_counselor_role, format: :json)
end
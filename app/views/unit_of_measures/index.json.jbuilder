json.array!(@unit_of_measures) do |unit_of_measure|
  json.extract! unit_of_measure, :name, :description, :abbreviation
  json.url unit_of_measure_url(unit_of_measure, format: :json)
end
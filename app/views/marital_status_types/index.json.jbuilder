json.array!(@marital_status_types) do |marital_status_type|
  json.extract! marital_status_type, :name, :description
  json.url marital_status_type_url(marital_status_type, format: :json)
end
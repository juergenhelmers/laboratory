json.array!(@payor_roles) do |payor_role|
  json.extract! payor_role, :person_id
  json.url payor_role_url(payor_role, format: :json)
end
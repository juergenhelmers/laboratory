json.array!(@nurse_roles) do |nurse_role|
  json.extract! nurse_role, :medical_id, :person_id
  json.url nurse_role_url(nurse_role, format: :json)
end
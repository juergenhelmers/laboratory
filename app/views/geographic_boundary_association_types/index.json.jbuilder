json.array!(@geographic_boundary_association_types) do |geographic_boundary_association_type|
  json.extract! geographic_boundary_association_type, :name, :description
  json.url geographic_boundary_association_type_url(geographic_boundary_association_type, format: :json)
end
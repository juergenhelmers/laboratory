json.array!(@customer_roles) do |customer_role|
  json.extract! customer_role, :person_id
  json.url customer_role_url(customer_role, format: :json)
end
json.array!(@training_class_types) do |training_class_type|
  json.extract! training_class_type, :name, :description
  json.url training_class_type_url(training_class_type, format: :json)
end
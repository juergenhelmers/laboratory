json.array!(@geographic_boundaries) do |geographic_boundary|
  json.extract! geographic_boundary, :name, :geo_code, :abbreviation, :geographic_boundary_type_id
  json.url geographic_boundary_url(geographic_boundary, format: :json)
end
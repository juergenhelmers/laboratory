json.array!(@geographic_boundaries) do |geographic_boundary|
  json.extract! geographic_boundary, :id, :name
end
json.array!(@child_roles) do |child_role|
  json.extract! child_role, :person_id
  json.url child_role_url(child_role, format: :json)
end
json.array!(@organizations) do |organization|
  json.extract! organization, :current_name
  json.url organization_url(organization, format: :json)
end
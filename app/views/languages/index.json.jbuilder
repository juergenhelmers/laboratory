json.array!(@languages) do |language|
  json.extract! language, :name, :abbreviation, :description
  json.url language_url(language, format: :json)
end
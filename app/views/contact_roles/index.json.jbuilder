json.array!(@contact_roles) do |contact_role|
  json.extract! contact_role, :person_id
  json.url contact_role_url(contact_role, format: :json)
end
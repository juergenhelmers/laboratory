json.array!(@person_status_types) do |person_status_type|
  json.extract! person_status_type, :name, :description
  json.url person_status_type_url(person_status_type, format: :json)
end
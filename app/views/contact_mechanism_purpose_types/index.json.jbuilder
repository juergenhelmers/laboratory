json.array!(@contact_mechanism_purpose_types) do |contact_mechanism_purpose_type|
  json.extract! contact_mechanism_purpose_type, :name, :description
  json.url contact_mechanism_purpose_type_url(contact_mechanism_purpose_type, format: :json)
end
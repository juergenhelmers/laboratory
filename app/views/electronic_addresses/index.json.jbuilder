json.array!(@electronic_addresses) do |electronic_address|
  json.extract! electronic_address, :id, :electronic_address_string
end
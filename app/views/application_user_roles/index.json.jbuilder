json.array!(@application_user_roles) do |application_user_role|
  json.extract! application_user_role, :name, :description
  json.url application_user_role_url(application_user_role, format: :json)
end
json.array!(@patient_roles) do |patient_role|
  json.extract! patient_role, :person_id
  json.url patient_role_url(patient_role, format: :json)
end
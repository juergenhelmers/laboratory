json.array!(@parent_roles) do |parent_role|
  json.extract! parent_role, :person_id
  json.url parent_role_url(parent_role, format: :json)
end
json.array!(@geographic_boundary_types) do |geographic_boundary_type|
  json.extract! geographic_boundary_type, :name, :description, :is_code
  json.url geographic_boundary_type_url(geographic_boundary_type, format: :json)
end
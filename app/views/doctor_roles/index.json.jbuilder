json.array!(@doctor_roles) do |doctor_role|
  json.extract! doctor_role, :medical_id, :person_id
  json.url doctor_role_url(doctor_role, format: :json)
end
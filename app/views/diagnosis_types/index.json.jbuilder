json.array!(@diagnosis_types) do |diagnosis_type|
  json.extract! diagnosis_type, :id, :name, :code
end
class NurseRolesController < ApplicationController
  before_action :set_nurse_role, only: [:show, :edit, :update, :destroy]

  # GET /nurse_roles
  # GET /nurse_roles.json
  def index
    @nurse_roles = NurseRole.all
  end

  # GET /nurse_roles/1
  # GET /nurse_roles/1.json
  def show
  end

  # GET /nurse_roles/new
  def new
    @nurse_role = NurseRole.new
  end

  # GET /nurse_roles/1/edit
  def edit
  end

  # POST /nurse_roles
  # POST /nurse_roles.json
  def create
    @nurse_role = NurseRole.new(nurse_role_params)

    respond_to do |format|
      if @nurse_role.save
        format.html { redirect_to @nurse_role, notice: "#{I18n.t('nurse_role.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @nurse_role }
      else
        format.html { render action: 'new' }
        format.json { render json: @nurse_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nurse_roles/1
  # PATCH/PUT /nurse_roles/1.json
  def update
    respond_to do |format|
      if @nurse_role.update(nurse_role_params)
        format.html { redirect_to @nurse_role, notice: "#{I18n.t('nurse_role.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @nurse_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nurse_roles/1
  # DELETE /nurse_roles/1.json
  def destroy
    @nurse_role.destroy
    respond_to do |format|
      format.html { redirect_to nurse_roles_url, notice: "#{I18n.t('nurse_role.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nurse_role
      @nurse_role = NurseRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nurse_role_params
      params.require(:nurse_role).permit(:medical_id, :person_id)
    end
end

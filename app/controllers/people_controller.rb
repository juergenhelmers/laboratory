class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :edit, :update, :destroy]

  # GET /people
  # GET /people.json
  def index
    @people = Person.page(params[:page])
  end

  # GET /people/1
  # GET /people/1.json
  def show
    @person = Person.find(params[:id]).decorate
  end

  # GET /people/new
  def new
    @person = Person.new
  end

  # GET /people/1/edit
  def edit
  end

  # POST /people
  # POST /people.json
  def create
    @person = Person.new(person_params)

    respond_to do |format|
      if @person.save
        format.html { redirect_to @person, notice: "#{I18n.t('person.model_name').titleize} #{I18n.t('forms.flash_created')}." }
        format.json { render action: 'show', status: :created, location: @person }
      else
        format.html { render action: 'new' }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to @person, notice: "#{I18n.t('person.model_name').titleize} #{I18n.t('forms.flash_updated')}." }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    @person.destroy
    respond_to do |format|
      format.html { redirect_to people_url, notice: "#{I18n.t('person.model_name').titleize} #{I18n.t('forms.flash_destroyed')}." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:current_first_name,
                                    :current_middle_name,
                                    :current_last_name,
                                    :current_personal_title,
                                    :current_suffix,
                                    :current_nickname,
                                    :gender_type_id,
                                    :birth_date,
                                    :mothers_maiden_name,
                                    :passport_nationality,
                                    :citizen_number,
                                    :ethnicity,
                                    :uuid,
                                    :comment,
                                    nurse_role_attributes: [:id, :_destroy, :medical_id, :person_id],
                                    child_role_attributes: [:id, :_destroy, :person_id,
                                      parent_child_relationships_attributes: [:id, :_destroy, :parent_role_id, :child_role_id, :from_date, :thru_date]
                                    ],
                                    contact_role_attributes: [:id, :_destroy, :person_id],
                                    customer_role_attributes: [:id, :_destroy, :person_id],
                                    employee_role_attributes: [:id, :_destroy, :person_id],
                                    genetic_counselor_role_attributes: [:id, :_destroy, :person_id],
                                    parent_role_attributes: [:id, :_destroy, :person_id],
                                    payor_role_attributes: [:id, :_destroy, :person_id],
                                    patient_role_attributes: [:id, :_destroy, :person_id,
                                      diagnoses_attributes: [:id, :_destroy, :diagnosis_date, :diagnosis_type_id, :patient_role_id, :health_care_episode_id],
                                      physical_characteristics_attributes: [:id, :_destroy, :patient_role_id, :physical_characteristic_type_id, :unit_of_measure_id, :from_date, :thru_date, :value, :person_id, ],
                                      patient_doctor_relationships_attributes: [:id, :_destroy, :patient_role_id, :doctor_role_id, :from_date, :thru_date],
                                      patient_genetic_counselor_relationships_attributes: [:id, :_destroy, :patient_role_id, :genetic_counselor_role_id, :from_date, :thru_date],
                                      patient_nurse_relationships_attributes: [:id, :_destroy, :patient_role_id, :nurse_role_id, :from_date, :thru_date]
                                    ],
                                    doctor_role_attributes: [:id, :_destroy, :medical_id, :person_id,
                                      doctor_contact_relationships_attributes: [:id, :_destroy, :doctor_role_id, :contact_role_id, :from_date, :thru_date]
                                    ],
                                    person_category_classifications_attributes: [:id, :_destroy, :person_id, :category_id ],
                                    person_statuses_attributes: [:id, :_destroy, :person_id, :person_status_type_id ],
                                    person_marital_statuses_attributes: [:id, :_destroy, :person_id, :marital_status_type_id ],
                                    person_languages_attributes: [:id, :_destroy, :person_id,:language_id, :thru_date, :from_date, :competency_level],
                                    person_trainings_attributes: [:id, :_destroy, :person_id,:training_class_type_id, :thru_date, :from_date],
                                    person_contact_mechanism_electronic_addresses_attributes: [
                                        :id,
                                        :_destroy,
                                        :from_date,
                                        :thru_date,
                                        :role_type_id,
                                        :non_solicitation_ind,
                                        person_contact_mechanism_purpose_electronic_addresses_attributes: [:id, :_destroy, :contact_mechanism_purpose_type_id],
                                        contact_mechanism_electronic_address_attributes: [:id, :contact_mechanism_type_id,
                                            electronic_address_attributes: [:id, :electronic_address_string]
                                          ]
                                    ],
                                    person_contact_mechanism_postals_attributes: [
                                        :id,
                                        :_destroy,
                                        :from_date,
                                        :thru_date,
                                        :role_type_id,
                                        :non_solicitation_ind,
                                          person_contact_mechanism_purpose_postals_attributes: [:id, :_destroy, :contact_mechanism_purpose_type_id],
                                          contact_mechanism_postal_attributes: [:id, :contact_mechanism_type_id,
                                              postal_address_attributes: [:id, :address1, :address2, :address3,
                                                postal_address_postal_code_boundary_attributes: [:id,:geographic_boundary_id],
                                                postal_address_city_boundary_attributes: [:id, :geographic_boundary_id],
                                                postal_address_suburb_boundary_attributes: [:id, :geographic_boundary_id],
                                                postal_address_country_boundary_attributes: [:id, :geographic_boundary_id]
                                              ]
                                          ]
                                        ],
                                    person_contact_mechanism_telecommunications_attributes: [
                                        :id,
                                        :_destroy,
                                        :from_date,
                                        :thru_date,
                                        :role_type_id,
                                        :non_solicitation_ind,
                                        :extension,
                                        person_contact_mechanism_purpose_telecommunications_attributes: [:id, :_destroy, :contact_mechanism_purpose_type_id],
                                        contact_mechanism_telecommunication_attributes: [:id, :contact_mechanism_type_id,
                                          telecommunications_number_attributes: [:country_code, :area_code, :contact_number]
                                        ]
                                    ]
                                    )
    end
end

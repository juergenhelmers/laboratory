class DoctorRolesController < ApplicationController
  before_action :set_doctor_role, only: [:show, :edit, :update, :destroy]

  # GET /doctor_roles
  # GET /doctor_roles.json
  def index
    @doctor_roles = DoctorRole.all
  end

  # GET /doctor_roles/1
  # GET /doctor_roles/1.json
  def show
  end

  # GET /doctor_roles/new
  def new
    @doctor_role = DoctorRole.new
  end

  # GET /doctor_roles/1/edit
  def edit
  end

  # POST /doctor_roles
  # POST /doctor_roles.json
  def create
    @doctor_role = DoctorRole.new(doctor_role_params)

    respond_to do |format|
      if @doctor_role.save
        format.html { redirect_to @doctor_role, notice: "#{I18n.t('doctor_role.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @doctor_role }
      else
        format.html { render action: 'new' }
        format.json { render json: @doctor_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /doctor_roles/1
  # PATCH/PUT /doctor_roles/1.json
  def update
    respond_to do |format|
      if @doctor_role.update(doctor_role_params)
        format.html { redirect_to @doctor_role, notice: "#{I18n.t('doctor_role.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @doctor_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /doctor_roles/1
  # DELETE /doctor_roles/1.json
  def destroy
    @doctor_role.destroy
    respond_to do |format|
      format.html { redirect_to doctor_roles_url, notice: "#{I18n.t('doctor_role.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_doctor_role
      @doctor_role = DoctorRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def doctor_role_params
      params.require(:doctor_role).permit(:medical_id, :person_id)
    end
end

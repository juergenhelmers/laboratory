class CustomerRolesController < ApplicationController
  before_action :set_customer_role, only: [:show, :edit, :update, :destroy]

  # GET /customer_roles
  # GET /customer_roles.json
  def index
    @customer_roles = CustomerRole.all
  end

  # GET /customer_roles/1
  # GET /customer_roles/1.json
  def show
  end

  # GET /customer_roles/new
  def new
    @customer_role = CustomerRole.new
  end

  # GET /customer_roles/1/edit
  def edit
  end

  # POST /customer_roles
  # POST /customer_roles.json
  def create
    @customer_role = CustomerRole.new(customer_role_params)

    respond_to do |format|
      if @customer_role.save
        format.html { redirect_to @customer_role, notice: "#{I18n.t('customer_role.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @customer_role }
      else
        format.html { render action: 'new' }
        format.json { render json: @customer_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customer_roles/1
  # PATCH/PUT /customer_roles/1.json
  def update
    respond_to do |format|
      if @customer_role.update(customer_role_params)
        format.html { redirect_to @customer_role, notice: "#{I18n.t('customer_role.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @customer_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_roles/1
  # DELETE /customer_roles/1.json
  def destroy
    @customer_role.destroy
    respond_to do |format|
      format.html { redirect_to customer_roles_url, notice: "#{I18n.t('customer_role.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_role
      @customer_role = CustomerRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_role_params
      params.require(:customer_role).permit(:person_id)
    end
end

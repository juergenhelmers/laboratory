class GeographicBoundaryAssociationTypesController < ApplicationController
  before_action :set_geographic_boundary_association_type, only: [:show, :edit, :update, :destroy]

  # GET /geographic_boundary_association_types
  # GET /geographic_boundary_association_types.json
  def index
    @geographic_boundary_association_types = GeographicBoundaryAssociationType.all
  end

  # GET /geographic_boundary_association_types/1
  # GET /geographic_boundary_association_types/1.json
  def show
  end

  # GET /geographic_boundary_association_types/new
  def new
    @geographic_boundary_association_type = GeographicBoundaryAssociationType.new
  end

  # GET /geographic_boundary_association_types/1/edit
  def edit
  end

  # POST /geographic_boundary_association_types
  # POST /geographic_boundary_association_types.json
  def create
    @geographic_boundary_association_type = GeographicBoundaryAssociationType.new(geographic_boundary_association_type_params)

    respond_to do |format|
      if @geographic_boundary_association_type.save
        format.html { redirect_to @geographic_boundary_association_type, notice: "#{I18n.t('geographic_boundary_association_type.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @geographic_boundary_association_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @geographic_boundary_association_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /geographic_boundary_association_types/1
  # PATCH/PUT /geographic_boundary_association_types/1.json
  def update
    respond_to do |format|
      if @geographic_boundary_association_type.update(geographic_boundary_association_type_params)
        format.html { redirect_to @geographic_boundary_association_type, notice: "#{I18n.t('geographic_boundary_association_type.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @geographic_boundary_association_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /geographic_boundary_association_types/1
  # DELETE /geographic_boundary_association_types/1.json
  def destroy
    @geographic_boundary_association_type.destroy
    respond_to do |format|
      format.html { redirect_to geographic_boundary_association_types_url, notice: "#{I18n.t('geographic_boundary_association_type.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_geographic_boundary_association_type
      @geographic_boundary_association_type = GeographicBoundaryAssociationType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def geographic_boundary_association_type_params
      params.require(:geographic_boundary_association_type).permit(:name, :description)
    end
end

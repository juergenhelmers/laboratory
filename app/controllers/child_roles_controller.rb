class ChildRolesController < ApplicationController
  before_action :set_child_role, only: [:show, :edit, :update, :destroy]

  # GET /child_roles
  # GET /child_roles.json
  def index
    @child_roles = ChildRole.all
  end

  # GET /child_roles/1
  # GET /child_roles/1.json
  def show
  end

  # GET /child_roles/new
  def new
    @child_role = ChildRole.new
  end

  # GET /child_roles/1/edit
  def edit
  end

  # POST /child_roles
  # POST /child_roles.json
  def create
    @child_role = ChildRole.new(child_role_params)

    respond_to do |format|
      if @child_role.save
        format.html { redirect_to @child_role, notice: "#{I18n.t('child_role.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @child_role }
      else
        format.html { render action: 'new' }
        format.json { render json: @child_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /child_roles/1
  # PATCH/PUT /child_roles/1.json
  def update
    respond_to do |format|
      if @child_role.update(child_role_params)
        format.html { redirect_to @child_role, notice: "#{I18n.t('child_role.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @child_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /child_roles/1
  # DELETE /child_roles/1.json
  def destroy
    @child_role.destroy
    respond_to do |format|
      format.html { redirect_to child_roles_url, notice: "#{I18n.t('child_role.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_child_role
      @child_role = ChildRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def child_role_params
      params.require(:child_role).permit(:person_id)
    end
end

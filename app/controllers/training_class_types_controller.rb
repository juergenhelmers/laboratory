class TrainingClassTypesController < ApplicationController
  before_action :set_training_class_type, only: [:show, :edit, :update, :destroy]

  # GET /training_class_types
  # GET /training_class_types.json
  def index
    @training_class_types = TrainingClassType.all.order('name asc')
  end

  # GET /training_class_types/1
  # GET /training_class_types/1.json
  def show
  end

  # GET /training_class_types/new
  def new
    @training_class_type = TrainingClassType.new
  end

  # GET /training_class_types/1/edit
  def edit
  end

  # POST /training_class_types
  # POST /training_class_types.json
  def create
    @training_class_type = TrainingClassType.new(training_class_type_params)

    respond_to do |format|
      if @training_class_type.save
        format.html { redirect_to @training_class_type, notice: "#{I18n.t('training_class_type.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @training_class_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @training_class_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /training_class_types/1
  # PATCH/PUT /training_class_types/1.json
  def update
    respond_to do |format|
      if @training_class_type.update(training_class_type_params)
        format.html { redirect_to @training_class_type, notice: "#{I18n.t('training_class_type.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @training_class_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /training_class_types/1
  # DELETE /training_class_types/1.json
  def destroy
    @training_class_type.destroy
    respond_to do |format|
      format.html { redirect_to training_class_types_url, notice: "#{I18n.t('training_class_type.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_training_class_type
      @training_class_type = TrainingClassType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def training_class_type_params
      params.require(:training_class_type).permit(:name, :description)
    end
end

class GeographicBoundaryTypesController < ApplicationController
  before_action :set_geographic_boundary_type, only: [:show, :edit, :update, :destroy]

  # GET /geographic_boundary_types
  # GET /geographic_boundary_types.json
  def index
    @geographic_boundary_types = GeographicBoundaryType.all
  end

  # GET /geographic_boundary_types/1
  # GET /geographic_boundary_types/1.json
  def show
  end

  # GET /geographic_boundary_types/new
  def new
    @geographic_boundary_type = GeographicBoundaryType.new
  end

  # GET /geographic_boundary_types/1/edit
  def edit
  end

  # POST /geographic_boundary_types
  # POST /geographic_boundary_types.json
  def create
    @geographic_boundary_type = GeographicBoundaryType.new(geographic_boundary_type_params)

    respond_to do |format|
      if @geographic_boundary_type.save
        format.html { redirect_to @geographic_boundary_type, notice: "#{I18n.t('geographic_boundary_type.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @geographic_boundary_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @geographic_boundary_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /geographic_boundary_types/1
  # PATCH/PUT /geographic_boundary_types/1.json
  def update
    respond_to do |format|
      if @geographic_boundary_type.update(geographic_boundary_type_params)
        format.html { redirect_to @geographic_boundary_type, notice: "#{I18n.t('geographic_boundary_type.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @geographic_boundary_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /geographic_boundary_types/1
  # DELETE /geographic_boundary_types/1.json
  def destroy
    @geographic_boundary_type.destroy
    respond_to do |format|
      format.html { redirect_to geographic_boundary_types_url, notice: "#{I18n.t('geographic_boundary_type.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_geographic_boundary_type
      @geographic_boundary_type = GeographicBoundaryType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def geographic_boundary_type_params
      params.require(:geographic_boundary_type).permit(:name, :description, :is_code)
    end
end

class ElectronicAddressesController < ApplicationController

  def index
    @electronic_addresses = ElectronicAddress.tokens(params[:q])
  end

end
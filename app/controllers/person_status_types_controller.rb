class PersonStatusTypesController < ApplicationController
  before_action :set_person_status_type, only: [:show, :edit, :update, :destroy]

  # GET /person_status_types
  # GET /person_status_types.json
  def index
    @person_status_types = PersonStatusType.page(params[:page]).order('name asc')
  end

  # GET /person_status_types/1
  # GET /person_status_types/1.json
  def show
  end

  # GET /person_status_types/new
  def new
    @person_status_type = PersonStatusType.new
  end

  # GET /person_status_types/1/edit
  def edit
  end

  # POST /person_status_types
  # POST /person_status_types.json
  def create
    @person_status_type = PersonStatusType.new(person_status_type_params)

    respond_to do |format|
      if @person_status_type.save
        format.html { redirect_to @person_status_type, notice: "#{I18n.t('person_status_type.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @person_status_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @person_status_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /person_status_types/1
  # PATCH/PUT /person_status_types/1.json
  def update
    respond_to do |format|
      if @person_status_type.update(person_status_type_params)
        format.html { redirect_to @person_status_type, notice: "#{I18n.t('person_status_type.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @person_status_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /person_status_types/1
  # DELETE /person_status_types/1.json
  def destroy
    @person_status_type.destroy
    respond_to do |format|
      format.html { redirect_to person_status_types_url, notice: "#{I18n.t('person_status_type.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person_status_type
      @person_status_type = PersonStatusType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_status_type_params
      params.require(:person_status_type).permit(:name, :description)
    end
end

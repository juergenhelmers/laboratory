class DiagnosisTypesController < ApplicationController
  before_action :set_diagnosis_type, only: [:show, :edit, :update, :destroy]

  # GET /diagnosis_types
  # GET /diagnosis_types.json
  def index
    if params[:q]
      @diagnosis_types = DiagnosisType.where("name LIKE ? OR code LIKE ?", "%#{params[:q]}%", "%#{params[:q]}%").order('name asc')
    else
      @diagnosis_types = DiagnosisType.page(params[:page]).order("code ASC")
    end
  end

  # GET /diagnosis_types/1
  # GET /diagnosis_types/1.json
  def show
  end

  # GET /diagnosis_types/new
  def new
    @diagnosis_type = DiagnosisType.new
  end

  # GET /diagnosis_types/1/edit
  def edit
  end

  # POST /diagnosis_types
  # POST /diagnosis_types.json
  def create
    @diagnosis_type = DiagnosisType.new(diagnosis_type_params)

    respond_to do |format|
      if @diagnosis_type.save
        format.html { redirect_to @diagnosis_type, notice: "#{I18n.t('diagnosis_type.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @diagnosis_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @diagnosis_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /diagnosis_types/1
  # PATCH/PUT /diagnosis_types/1.json
  def update
    respond_to do |format|
      if @diagnosis_type.update(diagnosis_type_params)
        format.html { redirect_to @diagnosis_type, notice: "#{I18n.t('diagnosis_type.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @diagnosis_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diagnosis_types/1
  # DELETE /diagnosis_types/1.json
  def destroy
    @diagnosis_type.destroy
    respond_to do |format|
      format.html { redirect_to diagnosis_types_url, notice: "#{I18n.t('diagnosis_type.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diagnosis_type
      @diagnosis_type = DiagnosisType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diagnosis_type_params
      params.require(:diagnosis_type).permit(:name, :description, :code)
    end
end

class GeographicBoundariesController < ApplicationController
  before_action :set_geographic_boundary, only: [:show, :edit, :update, :destroy]

  # GET /geographic_boundaries
  # GET /geographic_boundaries.json
  def index
    if params[:filter]
      @geographic_boundaries = GeographicBoundary.includes(:geographic_boundary_type).where(:geographic_boundary_types => { name: "#{params[:filter]}" }).page(params[:page]).order('geographic_boundaries.name')
    else
      @geographic_boundaries = GeographicBoundary.includes(:geographic_boundary_type).page(params[:page]).order('geographic_boundaries.name')
    end
  end

  def countries
    @geographic_boundaries = GeographicBoundary.includes(:geographic_boundary_type).where(:geographic_boundary_types => { name: "Country" }).where("geographic_boundaries.name LIKE ?", "%#{params[:q]}%" ).order('geographic_boundaries.name')
  end

  def postal_codes
    @geographic_boundaries = GeographicBoundary.includes(:geographic_boundary_type).where(:geographic_boundary_types => { name: "Postal Code" }).where("geographic_boundaries.name LIKE ?", "%#{params[:q]}%" ).order('geographic_boundaries.name')
  end

  def cities
    @geographic_boundaries = GeographicBoundary.includes(:geographic_boundary_type).where(:geographic_boundary_types => { name: "City" }).where("geographic_boundaries.name LIKE ?", "%#{params[:q]}%" ).order('geographic_boundaries.name')
  end

  def suburbs
    @geographic_boundaries = GeographicBoundary.includes(:geographic_boundary_type).where(:geographic_boundary_types => { name: "Suburb" }).where("geographic_boundaries.name LIKE ?", "%#{params[:q]}%" ).order('geographic_boundaries.name')
  end


  # GET /geographic_boundaries/1
  # GET /geographic_boundaries/1.json
  def show
  end

  # GET /geographic_boundaries/new
  def new
    @geographic_boundary = GeographicBoundary.new
  end

  # GET /geographic_boundaries/1/edit
  def edit
  end

  # POST /geographic_boundaries
  # POST /geographic_boundaries.json
  def create
    @geographic_boundary = GeographicBoundary.new(geographic_boundary_params)

    respond_to do |format|
      if @geographic_boundary.save
        format.html { redirect_to @geographic_boundary, notice: "#{I18n.t('geographic_boundary.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @geographic_boundary }
      else
        format.html { render action: 'new' }
        format.json { render json: @geographic_boundary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /geographic_boundaries/1
  # PATCH/PUT /geographic_boundaries/1.json
  def update
    respond_to do |format|
      if @geographic_boundary.update(geographic_boundary_params)
        format.html { redirect_to @geographic_boundary, notice: "#{I18n.t('geographic_boundary.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @geographic_boundary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /geographic_boundaries/1
  # DELETE /geographic_boundaries/1.json
  def destroy
    @geographic_boundary.destroy
    respond_to do |format|
      format.html { redirect_to geographic_boundaries_url, notice: "#{I18n.t('geographic_boundary.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_geographic_boundary
      @geographic_boundary = GeographicBoundary.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def geographic_boundary_params
      params.require(:geographic_boundary).permit(:name, :geo_code, :abbreviation, :geographic_boundary_type_id)
    end
end

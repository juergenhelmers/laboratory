class PatientRolesController < ApplicationController
  before_action :set_patient_role, only: [:show, :edit, :update, :destroy]

  # GET /patient_roles
  # GET /patient_roles.json
  def index
    @patient_roles = PatientRole.all
  end

  # GET /patient_roles/1
  # GET /patient_roles/1.json
  def show
  end

  # GET /patient_roles/new
  def new
    @patient_role = PatientRole.new
  end

  # GET /patient_roles/1/edit
  def edit
  end

  # POST /patient_roles
  # POST /patient_roles.json
  def create
    @patient_role = PatientRole.new(patient_role_params)

    respond_to do |format|
      if @patient_role.save
        format.html { redirect_to @patient_role, notice: "#{I18n.t('patient_role.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @patient_role }
      else
        format.html { render action: 'new' }
        format.json { render json: @patient_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patient_roles/1
  # PATCH/PUT /patient_roles/1.json
  def update
    respond_to do |format|
      if @patient_role.update(patient_role_params)
        format.html { redirect_to @patient_role, notice: "#{I18n.t('patient_role.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @patient_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patient_roles/1
  # DELETE /patient_roles/1.json
  def destroy
    @patient_role.destroy
    respond_to do |format|
      format.html { redirect_to patient_roles_url, notice: "#{I18n.t('patient_role.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient_role
      @patient_role = PatientRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patient_role_params
      params.require(:patient_role).permit(:person_id)
    end
end

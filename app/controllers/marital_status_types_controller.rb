class MaritalStatusTypesController < ApplicationController
  before_action :set_marital_status_type, only: [:show, :edit, :update, :destroy]

  # GET /marital_status_types
  # GET /marital_status_types.json
  def index
    @marital_status_types = MaritalStatusType.page(params[:page]).order('name asc')
  end

  # GET /marital_status_types/1
  # GET /marital_status_types/1.json
  def show
  end

  # GET /marital_status_types/new
  def new
    @marital_status_type = MaritalStatusType.new
  end

  # GET /marital_status_types/1/edit
  def edit
  end

  # POST /marital_status_types
  # POST /marital_status_types.json
  def create
    @marital_status_type = MaritalStatusType.new(marital_status_type_params)

    respond_to do |format|
      if @marital_status_type.save
        format.html { redirect_to @marital_status_type, notice: "#{I18n.t('marital_status_type.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @marital_status_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @marital_status_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /marital_status_types/1
  # PATCH/PUT /marital_status_types/1.json
  def update
    respond_to do |format|
      if @marital_status_type.update(marital_status_type_params)
        format.html { redirect_to @marital_status_type, notice: "#{I18n.t('marital_status_type.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @marital_status_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /marital_status_types/1
  # DELETE /marital_status_types/1.json
  def destroy
    @marital_status_type.destroy
    respond_to do |format|
      format.html { redirect_to marital_status_types_url, notice: "#{I18n.t('marital_status_type.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_marital_status_type
      @marital_status_type = MaritalStatusType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def marital_status_type_params
      params.require(:marital_status_type).permit(:name, :description)
    end
end

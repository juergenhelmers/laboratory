class PhysicalCharacteristicTypesController < ApplicationController
  before_action :set_physical_characteristic_type, only: [:show, :edit, :update, :destroy]

  # GET /physical_characteristic_types
  # GET /physical_characteristic_types.json
  def index
    @physical_characteristic_types = PhysicalCharacteristicType.all
  end

  # GET /physical_characteristic_types/1
  # GET /physical_characteristic_types/1.json
  def show
  end

  # GET /physical_characteristic_types/new
  def new
    @physical_characteristic_type = PhysicalCharacteristicType.new
  end

  # GET /physical_characteristic_types/1/edit
  def edit
  end

  # POST /physical_characteristic_types
  # POST /physical_characteristic_types.json
  def create
    @physical_characteristic_type = PhysicalCharacteristicType.new(physical_characteristic_type_params)

    respond_to do |format|
      if @physical_characteristic_type.save
        format.html { redirect_to @physical_characteristic_type, notice: "#{I18n.t('physical_characteristic_type.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @physical_characteristic_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @physical_characteristic_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /physical_characteristic_types/1
  # PATCH/PUT /physical_characteristic_types/1.json
  def update
    respond_to do |format|
      if @physical_characteristic_type.update(physical_characteristic_type_params)
        format.html { redirect_to @physical_characteristic_type, notice: "#{I18n.t('physical_characteristic_type.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @physical_characteristic_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /physical_characteristic_types/1
  # DELETE /physical_characteristic_types/1.json
  def destroy
    @physical_characteristic_type.destroy
    respond_to do |format|
      format.html { redirect_to physical_characteristic_types_url, notice: "#{I18n.t('physical_characteristic_type.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_physical_characteristic_type
      @physical_characteristic_type = PhysicalCharacteristicType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def physical_characteristic_type_params
      params.require(:physical_characteristic_type).permit(:name, :description)
    end
end

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # add cancan controlled access authorization to all controllers but devise to enable login/logout
  before_filter do |controller|
    unless controller.devise_controller?
      controller.class.cancan_resource_class.new(controller).authorize_resource
    end
  end
  # require authorized users for all controllers
  before_filter :authenticate_user!

private
  rescue_from CanCan::AccessDenied do |exception|
    # TODO: remove after debugging
    Rails.logger.debug "Access denied on #{exception.action} #{exception.subject.inspect}"
    #
    flash[:error] = "Access denied!"
    redirect_to root_url
  end

end

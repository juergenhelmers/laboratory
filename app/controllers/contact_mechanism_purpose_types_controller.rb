class ContactMechanismPurposeTypesController < ApplicationController
  before_action :set_contact_mechanism_purpose_type, only: [:show, :edit, :update, :destroy]

  # GET /contact_mechanism_purpose_types
  # GET /contact_mechanism_purpose_types.json
  def index
    @contact_mechanism_purpose_types = ContactMechanismPurposeType.order('name asc')
  end

  # GET /contact_mechanism_purpose_types/1
  # GET /contact_mechanism_purpose_types/1.json
  def show
  end

  # GET /contact_mechanism_purpose_types/new
  def new
    @contact_mechanism_purpose_type = ContactMechanismPurposeType.new
  end

  # GET /contact_mechanism_purpose_types/1/edit
  def edit
  end

  # POST /contact_mechanism_purpose_types
  # POST /contact_mechanism_purpose_types.json
  def create
    @contact_mechanism_purpose_type = ContactMechanismPurposeType.new(contact_mechanism_purpose_type_params)

    respond_to do |format|
      if @contact_mechanism_purpose_type.save
        format.html { redirect_to @contact_mechanism_purpose_type, notice: "#{I18n.t('contact_mechanism_purpose_type.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @contact_mechanism_purpose_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @contact_mechanism_purpose_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contact_mechanism_purpose_types/1
  # PATCH/PUT /contact_mechanism_purpose_types/1.json
  def update
    respond_to do |format|
      if @contact_mechanism_purpose_type.update(contact_mechanism_purpose_type_params)
        format.html { redirect_to @contact_mechanism_purpose_type, notice: "#{I18n.t('contact_mechanism_purpose_type.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @contact_mechanism_purpose_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contact_mechanism_purpose_types/1
  # DELETE /contact_mechanism_purpose_types/1.json
  def destroy
    @contact_mechanism_purpose_type.destroy
    respond_to do |format|
      format.html { redirect_to contact_mechanism_purpose_types_url, notice: "#{I18n.t('contact_mechanism_purpose_type.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact_mechanism_purpose_type
      @contact_mechanism_purpose_type = ContactMechanismPurposeType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_mechanism_purpose_type_params
      params.require(:contact_mechanism_purpose_type).permit(:name, :description)
    end
end

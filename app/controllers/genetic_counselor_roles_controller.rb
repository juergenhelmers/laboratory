class GeneticCounselorRolesController < ApplicationController
  before_action :set_genetic_counselor_role, only: [:show, :edit, :update, :destroy]

  # GET /genetic_counselor_roles
  # GET /genetic_counselor_roles.json
  def index
    @genetic_counselor_roles = GeneticCounselorRole.all
  end

  # GET /genetic_counselor_roles/1
  # GET /genetic_counselor_roles/1.json
  def show
  end

  # GET /genetic_counselor_roles/new
  def new
    @genetic_counselor_role = GeneticCounselorRole.new
  end

  # GET /genetic_counselor_roles/1/edit
  def edit
  end

  # POST /genetic_counselor_roles
  # POST /genetic_counselor_roles.json
  def create
    @genetic_counselor_role = GeneticCounselorRole.new(genetic_counselor_role_params)

    respond_to do |format|
      if @genetic_counselor_role.save
        format.html { redirect_to @genetic_counselor_role, notice: "#{I18n.t('genetic_counselor_role.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @genetic_counselor_role }
      else
        format.html { render action: 'new' }
        format.json { render json: @genetic_counselor_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /genetic_counselor_roles/1
  # PATCH/PUT /genetic_counselor_roles/1.json
  def update
    respond_to do |format|
      if @genetic_counselor_role.update(genetic_counselor_role_params)
        format.html { redirect_to @genetic_counselor_role, notice: "#{I18n.t('genetic_counselor_role.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @genetic_counselor_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /genetic_counselor_roles/1
  # DELETE /genetic_counselor_roles/1.json
  def destroy
    @genetic_counselor_role.destroy
    respond_to do |format|
      format.html { redirect_to genetic_counselor_roles_url, notice: "#{I18n.t('genetic_counselor_role.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_genetic_counselor_role
      @genetic_counselor_role = GeneticCounselorRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def genetic_counselor_role_params
      params.require(:genetic_counselor_role).permit(:person_id)
    end
end

class ApplicationUserRolesController < ApplicationController
  before_action :set_application_user_role, only: [:show, :edit, :update, :destroy]

  # GET /application_user_roles
  # GET /application_user_roles.json
  def index
    @application_user_roles = ApplicationUserRole.order('name asc')
  end

  # GET /application_user_roles/1
  # GET /application_user_roles/1.json
  def show
  end

  # GET /application_user_roles/new
  def new
    @application_user_role = ApplicationUserRole.new
  end

  # GET /application_user_roles/1/edit
  def edit
  end

  # POST /application_user_roles
  # POST /application_user_roles.json
  def create
    @application_user_role = ApplicationUserRole.new(application_user_role_params)

    respond_to do |format|
      if @application_user_role.save
        format.html { redirect_to @application_user_role, notice: "#{I18n.t('application_user_role.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @application_user_role }
      else
        format.html { render action: 'new' }
        format.json { render json: @application_user_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /application_user_roles/1
  # PATCH/PUT /application_user_roles/1.json
  def update
    respond_to do |format|
      if @application_user_role.update(application_user_role_params)
        format.html { redirect_to application_user_roles_path, notice: "#{I18n.t('application_user_role.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @application_user_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /application_user_roles/1
  # DELETE /application_user_roles/1.json
  def destroy
    @application_user_role.destroy
    respond_to do |format|
      format.html { redirect_to application_user_roles_url, notice: "#{I18n.t('application_user_role.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_application_user_role
      @application_user_role = ApplicationUserRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def application_user_role_params
      params.require(:application_user_role).permit(:name, :description)
    end
end

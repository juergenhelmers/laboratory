class ParentRolesController < ApplicationController
  before_action :set_parent_role, only: [:show, :edit, :update, :destroy]

  # GET /parent_roles
  # GET /parent_roles.json
  def index
    @parent_roles = ParentRole.all
  end

  # GET /parent_roles/1
  # GET /parent_roles/1.json
  def show
  end

  # GET /parent_roles/new
  def new
    @parent_role = ParentRole.new
  end

  # GET /parent_roles/1/edit
  def edit
  end

  # POST /parent_roles
  # POST /parent_roles.json
  def create
    @parent_role = ParentRole.new(parent_role_params)

    respond_to do |format|
      if @parent_role.save
        format.html { redirect_to @parent_role, notice: "#{I18n.t('parent_role.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @parent_role }
      else
        format.html { render action: 'new' }
        format.json { render json: @parent_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /parent_roles/1
  # PATCH/PUT /parent_roles/1.json
  def update
    respond_to do |format|
      if @parent_role.update(parent_role_params)
        format.html { redirect_to @parent_role, notice: "#{I18n.t('parent_role.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @parent_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /parent_roles/1
  # DELETE /parent_roles/1.json
  def destroy
    @parent_role.destroy
    respond_to do |format|
      format.html { redirect_to parent_roles_url, notice: "#{I18n.t('parent_role.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_parent_role
      @parent_role = ParentRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def parent_role_params
      params.require(:parent_role).permit(:person_id)
    end
end

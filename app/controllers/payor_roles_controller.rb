class PayorRolesController < ApplicationController
  before_action :set_payor_role, only: [:show, :edit, :update, :destroy]

  # GET /payor_roles
  # GET /payor_roles.json
  def index
    @payor_roles = PayorRole.all
  end

  # GET /payor_roles/1
  # GET /payor_roles/1.json
  def show
  end

  # GET /payor_roles/new
  def new
    @payor_role = PayorRole.new
  end

  # GET /payor_roles/1/edit
  def edit
  end

  # POST /payor_roles
  # POST /payor_roles.json
  def create
    @payor_role = PayorRole.new(payor_role_params)

    respond_to do |format|
      if @payor_role.save
        format.html { redirect_to @payor_role, notice: "#{I18n.t('payor_role.model_name').titleize} #{I18n.t('forms.flash_created')}" }
        format.json { render action: 'show', status: :created, location: @payor_role }
      else
        format.html { render action: 'new' }
        format.json { render json: @payor_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payor_roles/1
  # PATCH/PUT /payor_roles/1.json
  def update
    respond_to do |format|
      if @payor_role.update(payor_role_params)
        format.html { redirect_to @payor_role, notice: "#{I18n.t('payor_role.model_name').titleize} #{I18n.t('forms.flash_updated')}" }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @payor_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payor_roles/1
  # DELETE /payor_roles/1.json
  def destroy
    @payor_role.destroy
    respond_to do |format|
      format.html { redirect_to payor_roles_url, notice: "#{I18n.t('payor_role.model_name').titleize} #{I18n.t('forms.flash_destroyed')}" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payor_role
      @payor_role = PayorRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payor_role_params
      params.require(:payor_role).permit(:person_id)
    end
end

class WelcomeController < ApplicationController
  skip_authorization_check
  skip_authorize_resource :only => :index

  def index
  end

end

# validation is based on http://regexlib.com/REDetails.aspx?regexp_id=1573
# TODO validate the regular expression!

class SaIdNumberValidator < ActiveModel::EachValidator
  def validate_each(object, attribute, value)
    unless value =~ /^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))$/i
      object.errors[attribute] << (options[:message] || "is not a valid South African ID number")
    end
  end
end





#require 'fileutils'



namespace :barcode do
  desc "check for QR code image file"
  task :check_qr_code => :environment do

  Person.all.each do |person|
    qr_code_file = "#{Rails.root}/public/system/barcodes/people/qr/#{person.uuid}.png"
    unless File.exist?(qr_code_file)
      qr = RQRCode::QRCode.new( person.uuid, :size => 5, :level => :h )
      qr.to_img.resize(90, 90).save("#{Rails.root}/public/system/barcodes/people/qr/#{person.uuid}.png")
    end

  end





  end
end

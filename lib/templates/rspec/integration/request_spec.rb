require 'spec_helper'

describe "<%= class_name.pluralize %>" do
  describe "GET /<%= table_name %>" do
    it "does return code 200 when accessing the index page" do
      visit(<%= index_helper %>_path)
      expect(page.status_code).to be(200)
    end

    it "lists the <%= singular_table_name %> name on index page" do
      <%= singular_table_name %> = create(:<%= singular_table_name %>)
      visit(<%= index_helper %>_path)
      expect(page).to have_content(<%= singular_table_name %>.name)
    end

  end

  describe "GET /<%= singular_table_name %>/new" do
    it "can create a new <%= singular_table_name %> record" do
      visit(<%= index_helper %>_path)
      click_link("New #{I18n.t('<%= singular_table_name %>.model_name')}")
      fill_in I18n.t('<%= singular_table_name %>.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("<%= class_name.titleize %> was successfully created")
      }
      expect(page).to have_content("MyString")
    end

    it "cannot create a new <%= singular_table_name %> record without a name" do
      visit(<%= index_helper %>_path)
      click_link("New #{I18n.t('<%= singular_table_name %>.model_name')}")
      click_button I18n.t('actions.save')
      within('.alert.alert-error') do
        expect(page).to have_content("Please review the problems below:")
      end
    end
  end

  describe "GET /<%= singular_table_name %>/edit" do
    it "can update an existing <%= singular_table_name %>" do
      <%= singular_table_name %> = create(:<%= singular_table_name %>)
      visit(edit_<%= singular_table_name %>_path(<%= singular_table_name %>))
      fill_in I18n.t('<%= singular_table_name %>.name'), with: "MyString"
      click_button I18n.t('actions.save')
      within('div.alert.alert-success') {
        expect(page).to have_content("<%= class_name.titleize %> was successfully updated")
      }
      expect(page).to have_content("MyString")
    end
  end

  describe "GET /<%= singular_table_name %>/:id" do
    it "can display the details of a <%= singular_table_name %>" do
      <%= singular_table_name %> = create(:<%= singular_table_name %>)
      visit(<%= singular_table_name %>_path(<%= singular_table_name %>))
      expect(page).to have_content(<%= singular_table_name %>.name)
    end

    it "Deletes a <%= singular_table_name %>" do
      DatabaseCleaner.clean
      <%= singular_table_name %> = create(:<%= singular_table_name %>)
      visit(<%= table_name %>_path)
      expect{
        within "tr#<%= singular_table_name %>_#{<%= singular_table_name %>.id}" do
          click_link I18n.t('actions.destroy')
        end
      }.to change(<%= class_name%>,:count).by(-1)
      within('div.alert.alert-success') {
        expect(page).to have_content("<%= class_name.titleize %> was successfully deleted")
      }
      expect(page).to have_content "#{I18n.t('<%= singular_table_name %>.index_heading')}"
      expect(page).to_not have_content <%= singular_table_name %>.name
    end

  end

end

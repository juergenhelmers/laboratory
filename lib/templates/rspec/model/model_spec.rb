require 'spec_helper'

<% module_namespacing do -%>
describe <%= class_name %> do
  it "has a valid fixture" do
    expect(create(:<%= singular_table_name %>)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:<%= singular_table_name %>, name: nil)).to_not be_valid
  end

end
<% end -%>
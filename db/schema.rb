# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131021100131) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "application_user_roles", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "category_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "child_family_relationships", force: true do |t|
    t.integer  "child_role_id"
    t.integer  "family_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "child_roles", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contact_mechanism_electronic_addresses", force: true do |t|
    t.integer  "contact_mechanism_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contact_mechanism_postals", force: true do |t|
    t.integer  "contact_mechanism_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contact_mechanism_purpose_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contact_mechanism_telecommunications", force: true do |t|
    t.integer  "contact_mechanism_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contact_mechanism_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contact_roles", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customer_roles", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "diagnoses", force: true do |t|
    t.integer  "patient_role_id"
    t.integer  "diagnosis_type_id"
    t.date     "diagnosis_date"
    t.integer  "health_care_episode_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "diagnosis_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "doctor_contact_relationships", force: true do |t|
    t.integer  "contact_role_id"
    t.integer  "doctor_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "doctor_practice_relationships", force: true do |t|
    t.integer  "doctor_role_id"
    t.integer  "practice_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "doctor_roles", force: true do |t|
    t.string   "medical_id"
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "electronic_addresses", force: true do |t|
    t.string   "electronic_address_string"
    t.integer  "contact_mechanism_electronic_address_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employee_roles", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "family_roles", force: true do |t|
    t.integer  "organization_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "gender_types", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "genetic_counselor_practice_relationships", force: true do |t|
    t.integer  "genetic_counselor_role_id"
    t.integer  "practice_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "genetic_counselor_roles", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "geographic_boundaries", force: true do |t|
    t.string   "name"
    t.string   "geo_code"
    t.string   "abbreviation"
    t.integer  "geographic_boundary_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "geographic_boundary_association_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "geographic_boundary_associations", force: true do |t|
    t.integer  "parent_id"
    t.integer  "child_id"
    t.integer  "geographic_boundary_association_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "geographic_boundary_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "is_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "languages", force: true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "marital_status_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "nurse_practice_relationships", force: true do |t|
    t.integer  "nurse_role_id"
    t.integer  "practice_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "nurse_roles", force: true do |t|
    t.string   "medical_id"
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "organizations", force: true do |t|
    t.string   "current_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parent_child_relationships", force: true do |t|
    t.integer  "parent_role_id"
    t.integer  "child_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parent_family_relationships", force: true do |t|
    t.integer  "parent_role_id"
    t.integer  "family_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parent_roles", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patient_doctor_relationships", force: true do |t|
    t.integer  "patient_role_id"
    t.integer  "doctor_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patient_genetic_counselor_relationships", force: true do |t|
    t.integer  "patient_role_id"
    t.integer  "genetic_counselor_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patient_nurse_relationships", force: true do |t|
    t.integer  "patient_role_id"
    t.integer  "nurse_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patient_roles", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payor_roles", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "people", force: true do |t|
    t.string   "current_first_name"
    t.string   "current_middle_name"
    t.string   "current_last_name"
    t.string   "current_personal_title"
    t.string   "current_suffix"
    t.string   "current_nickname"
    t.integer  "gender_type_id"
    t.date     "birth_date"
    t.string   "mothers_maiden_name"
    t.string   "passport_nationality"
    t.string   "citizen_number"
    t.string   "ethnicity"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uuid"
  end

  create_table "person_category_classifications", force: true do |t|
    t.integer  "person_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "person_contact_mechanism_electronic_addresses", force: true do |t|
    t.integer  "person_id"
    t.integer  "contact_mechanism_electronic_address_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.boolean  "non_solicitation_ind"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role_type_id"
  end

  create_table "person_contact_mechanism_postals", force: true do |t|
    t.integer  "person_id"
    t.integer  "contact_mechanism_postal_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.boolean  "non_solicitation_ind"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role_type_id"
  end

  create_table "person_contact_mechanism_purpose_electronic_addresses", force: true do |t|
    t.integer  "person_contact_mechanism_electronic_address_id"
    t.integer  "contact_mechanism_purpose_type_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "person_contact_mechanism_purpose_postals", force: true do |t|
    t.integer  "person_contact_mechanism_postal_id"
    t.integer  "contact_mechanism_purpose_type_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "person_contact_mechanism_purpose_telecommunications", force: true do |t|
    t.integer  "person_contact_mechanism_telecommunication_id"
    t.integer  "contact_mechanism_purpose_type_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "person_contact_mechanism_telecommunications", force: true do |t|
    t.integer  "person_id"
    t.integer  "contact_mechanism_telecommunication_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.boolean  "non_solicitation_ind"
    t.text     "comment"
    t.string   "extension"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role_type_id"
  end

  create_table "person_languages", force: true do |t|
    t.integer  "person_id"
    t.integer  "language_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.string   "competency_level"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "person_marital_statuses", force: true do |t|
    t.integer  "person_id"
    t.integer  "marital_status_type_id"
    t.integer  "marital_status_seq_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "person_status_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "person_statuses", force: true do |t|
    t.integer  "person_id"
    t.integer  "person_status_type_id"
    t.date     "person_status_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "person_trainings", force: true do |t|
    t.integer  "person_id"
    t.integer  "training_class_type_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "physical_characteristic_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "physical_characteristics", force: true do |t|
    t.integer  "patient_role_id"
    t.integer  "physical_characteristic_type_id"
    t.integer  "unit_of_measure_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.string   "value"
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "postal_address_city_boundaries", force: true do |t|
    t.integer  "postal_address_id"
    t.integer  "geographic_boundary_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "postal_address_country_boundaries", force: true do |t|
    t.integer  "postal_address_id"
    t.integer  "geographic_boundary_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "postal_address_postal_code_boundaries", force: true do |t|
    t.integer  "postal_address_id"
    t.integer  "geographic_boundary_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "postal_address_suburb_boundaries", force: true do |t|
    t.integer  "postal_address_id"
    t.integer  "geographic_boundary_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "postal_addresses", force: true do |t|
    t.string   "address1"
    t.string   "address2"
    t.string   "address3"
    t.float    "longitude"
    t.float    "laditude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "contact_mechanism_postal_id"
  end

  create_table "practice_manager_practice_relationships", force: true do |t|
    t.integer  "practice_manager_role_id"
    t.integer  "practice_role_id"
    t.date     "from_date"
    t.date     "thru_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "practice_manager_roles", force: true do |t|
    t.integer  "person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "practice_roles", force: true do |t|
    t.integer  "organization_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "role_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "parent_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "telecommunications_numbers", force: true do |t|
    t.string   "country_code"
    t.string   "area_code"
    t.string   "contact_number"
    t.integer  "contact_mechanism_telecommunication_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "training_class_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "unit_of_measures", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "abbreviation"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                    default: "", null: false
    t.string   "encrypted_password",       default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "company_id"
    t.date     "date_of_birth"
    t.integer  "application_user_role_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

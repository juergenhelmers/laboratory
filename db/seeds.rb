## This file should contain all the record creation needed to seed the database with its default values.
## The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#

# create application_user_roles
admin_role = ApplicationUserRole.create(name: "#{I18n.t('application.user_roles.admin')}")
ApplicationUserRole.create(name: "#{I18n.t('application.user_roles.csr')}")
ApplicationUserRole.create(name: "#{I18n.t('application.user_roles.lab_technician')}")
ApplicationUserRole.create(name: "#{I18n.t('application.user_roles.on_boarder')}")
ApplicationUserRole.create(name: "#{I18n.t('application.user_roles.data_steward')}")
ApplicationUserRole.create(name: "#{I18n.t('application.user_roles.cso')}")
ApplicationUserRole.create(name: "#{I18n.t('application.user_roles.ceo')}")

# create default admin user
User.create(firstname: "Juergen", lastname: "Helmers", company_id: "123456",
            date_of_birth: "1969/12/24", password: "IOP999jkl", email: "juergen.helmers@zettagenomics.com",
            application_user_role_id: admin_role.id )

# create languages for language select menu
Language.create(name: 'Afrikaans', abbreviation: 'AF')
Language.create(name: 'English', abbreviation: 'EN')
Language.create(name: 'French', abbreviation: 'FR')
Language.create(name: 'German', abbreviation: 'DE')

#create ContactMechanismPurposeTypes
ContactMechanismPurposeType.create(name: 'Business')
ContactMechanismPurposeType.create(name: 'Personal')
ContactMechanismPurposeType.create(name: 'Emergency')


# create contact_mechanism_types
ContactMechanismType.create(name: "#{I18n.t('contact_mechanism_type.phone')}")
ContactMechanismType.create(name: "#{I18n.t('contact_mechanism_type.postal_address')}")
ContactMechanismType.create(name: "#{I18n.t('contact_mechanism_type.email')}")
ContactMechanismType.create(name: "#{I18n.t('contact_mechanism_type.facebook')}")
ContactMechanismType.create(name: "#{I18n.t('contact_mechanism_type.twitter')}")
ContactMechanismType.create(name: "#{I18n.t('contact_mechanism_type.other')}")

# create diagnosis_types when NOT using postgresql
def parse_diagnosis_csv(input_file)
  #DiagnosisType.destroy_all
  CSV.foreach(input_file, headers: true, :col_sep => ";") do |row|
    DiagnosisType.create!(name: row['SHORT_DESCRIPTION'], description: row['LONG_DESCRIPTION'], code: row['DIAGNOSIS_CODE'])
    puts "#{row['DIAGNOSIS_CODE']}"
  end
end

# create diagnosis_types when using postgresql (much faster)
def parse_diagnosis_csv_sql(input_file)
  DiagnosisType.destroy_all
  inserts = []
  CSV.foreach(input_file, headers: true, :col_sep => ";") do |row|
    inserts.push("('now()', 'now()', '#{row['DIAGNOSIS_CODE']}','#{row['SHORT_DESCRIPTION']}','#{row['LONG_DESCRIPTION']}')")
  end
  # the insert statement is postgresql specific. Use the activerecord function (parse_diagnosis_csv) if not using postgres
  sql_query = "INSERT INTO diagnosis_types (created_at,updated_at,code,name,description) VALUES #{inserts.join(", ")}"
  ActiveRecord::Base.connection.execute sql_query
end

# NOTE:  make sure the csv file does not contain any single quotes. They have to be relace by '' for a postgres insert!
input_diagnosis_csv_file="#{Rails.root}/db/support_files/diagnosis_types_with_codes.csv"
parse_diagnosis_csv_sql(input_diagnosis_csv_file)

# create gender_types
GenderType.create(name: "#{I18n.t('gender_type.female')}")
GenderType.create(name: "#{I18n.t('gender_type.male')}")
GenderType.create(name: "#{I18n.t('gender_type.unknown')}")

# create geographic_boundary_types
@city =         GeographicBoundaryType.create(name: 'City')
province =      GeographicBoundaryType.create(name: 'Province')
country =       GeographicBoundaryType.create(name: 'Country')
region =        GeographicBoundaryType.create(name: 'Region')
district =      GeographicBoundaryType.create(name: 'District')
@postal_code =  GeographicBoundaryType.create(name: 'Postal Code')
@suburb =       GeographicBoundaryType.create(name: 'Suburb')

# creating geographic_boundary_association_types
paa = GeographicBoundaryAssociationType.create(name: "Postal Address Association")
GeographicBoundaryAssociationType.create(name: "Sales Boundary Association")
GeographicBoundaryAssociationType.create(name: "Health Boundary Association")

# create geographic_boundaries
# African countries
GeographicBoundary.create(name: "Algeria", abbreviation: "DZ", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Angola", abbreviation: "AO", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Ascension", abbreviation: "SH", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Benin", abbreviation: "BJ", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Botswana", abbreviation: "BW", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Burkina Faso", abbreviation: "BF", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Burundi", abbreviation: "BI", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Cameroon", abbreviation: "CM", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Cape Verde Islands", abbreviation: "CV", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Central African Republic", abbreviation: "CF", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Chad Republic", abbreviation: "TD", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Comoros", abbreviation: "KM", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Congo", abbreviation: "CG", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Djibouti", abbreviation: "DJ", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Egypt", abbreviation: "EG", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Equatorial Guinea", abbreviation: "GQ", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Eritrea", abbreviation: "ER", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Ethiopia", abbreviation: "ET", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Gabon Republic", abbreviation: "GA", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Gambia", abbreviation: "GM", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Ghana", abbreviation: "GH", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Guinea-Bissau", abbreviation: "GW", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Guinea", abbreviation: "GN", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Ivory Coast", abbreviation: "CI", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Kenya", abbreviation: "KE", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Lesotho", abbreviation: "LS", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Liberia", abbreviation: "LR", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Libya", abbreviation: "LY", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Madagascar", abbreviation: "MG", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Malawi", abbreviation: "MW", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Mali Republic", abbreviation: "ML", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Mauritania", abbreviation: "MR", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Mauritius", abbreviation: "MU", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Mayotte Island", abbreviation: "YT", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Morocco", abbreviation: "MA", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Mozambique", abbreviation: "MZ", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Namibia", abbreviation: "NA", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Niger Republic", abbreviation: "NE", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Nigeria", abbreviation: "NG", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Principe", abbreviation: "ST", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Reunion Island", abbreviation: "RE", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Rwanda", abbreviation: "RW", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Sao Tome", abbreviation: "ST", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Senegal Republic", abbreviation: "SN", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Seychelles", abbreviation: "SC", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Sierra Leone", abbreviation: "SL", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Somalia Republic", abbreviation: "SO", geographic_boundary_type_id: country.id)
south_africa = GeographicBoundary.create(name: "South Africa", abbreviation: "ZA", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "St. Helena", abbreviation: "SH", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Sudan", abbreviation: "SD", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Swaziland", abbreviation: "SZ", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Tanzania", abbreviation: "TZ", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Togo", abbreviation: "TG", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Tunisia", abbreviation: "TN", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Uganda", abbreviation: "UG", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Zaire", abbreviation: "CD", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Zambia", abbreviation: "ZM", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Zanzibar", abbreviation: "TZ", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Zimbabwe", abbreviation: "ZW", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "South Sudan", abbreviation: "SS", geographic_boundary_type_id: country.id)
GeographicBoundary.create(name: "Dem. Republic of the Congo", abbreviation: "CD", geographic_boundary_type_id: country.id)

# South African Provinces
@za_ec = south_africa.children.create(name: "Eastern Cape", abbreviation: "ZA-EC", geographic_boundary_type_id: province.id)
@za_fs = south_africa.children.create(name: "Free State", abbreviation: "ZA-FS", geographic_boundary_type_id: province.id)
@za_gt = south_africa.children.create(name: "Gauteng", abbreviation: "ZA-GT", geographic_boundary_type_id: province.id)
@za_nl = south_africa.children.create(name: "KwaZulu-Natal", abbreviation: "ZA-NL", geographic_boundary_type_id: province.id)
@za_lp = south_africa.children.create(name: "Limpopo", abbreviation: "ZA-LP", geographic_boundary_type_id: province.id)
@za_mp = south_africa.children.create(name: "Mpumalanga", abbreviation: "ZA-MP", geographic_boundary_type_id: province.id)
@za_nw = south_africa.children.create(name: "North West", abbreviation: "ZA-NW", geographic_boundary_type_id: province.id)
@za_nc = south_africa.children.create(name: "Northern Cape", abbreviation: "ZA-NC", geographic_boundary_type_id: province.id)
@za_wc = south_africa.children.create(name: "Western Cape", abbreviation: "ZA-WC", geographic_boundary_type_id: province.id)

#set the association type to postal for all provinces
GeographicBoundaryAssociation.where(parent_id: south_africa.id).each do |gba|
  gba.geographic_boundary_association_type_id = paa.id
  gba.save
end

def return_province(i)
# based on http://en.wikipedia.org/wiki/List_of_postal_codes_in_South_Africa
# note 9000..9299 was used in occupied South West Africa (Namibia) and is not in use
  if i.to_i.between?(1, 299)
    return @za_gt.id
  elsif i.to_i.between?(300, 499)
    return @za_nw.id
  elsif i.to_i.between?(500, 999)
    return @za_lp.id
  elsif i.to_i.between?(1000, 1399)
    return @za_mp.id
  elsif i.to_i.between?(1400, 2199)
    return @za_gt.id
  elsif i.to_i.between?(2200, 2499)
    return @za_mp.id
  elsif i.to_i.between?(2500, 2899)
    return @za_nw.id
  elsif i.to_i.between?(2900, 4730)
    return @za_nl.id
  elsif i.to_i.between?(4731, 6499)
    return @za_ec.id
  elsif i.to_i.between?(6500, 8099)
    return @za_wc.id
  elsif i.to_i.between?(8100, 8999)
    return @za_nc.id
  elsif i.to_i.between?(9300, 9999)
    return @za_fs.id
  end
end

#def create_geographic_boundary(row, key)
#  za_province = GeographicBoundary.find(return_province(row[key]))
#  # TODO fix bug as a new area is created every time. Check if area exists and if the postal code is the same then reuse the one being found
#  str_box_code = za_province.children.find_or_create_by!(name: row["#{key}"], abbreviation: row["#{key}"], geographic_boundary_type_id: @postal_code.id  )
#  unless row['AREA'].blank?
#    area = str_box_code.children.create!(name: row['AREA'].titleize, geographic_boundary_type_id: @city.id )
#    sub = area.children.create(name: row['SUBURB'].titleize, geographic_boundary_type_id: @suburb.id )
#    puts "#{sub.name}"
#  else
#    sub = str_box_code.children.create!(name: row['SUBURB'].titleize, geographic_boundary_type_id: @suburb.id )
#    puts "#{sub.name}"
#  end
#end


#def parse_postal_csv(csv_file)
#  CSV.foreach(csv_file, headers: true, :col_sep => ";") do |row|
#    if row['BOX-CODE'].present? &&  row['STR-CODE'].present?
#      if row['BOX-CODE'] ==  row['STR-CODE']
#        create_geographic_boundary(row, "BOX-CODE")
#      else
#        create_geographic_boundary(row, "STR-CODE")
#        create_geographic_boundary(row, "BOX-CODE")
#      end
#    elsif row['STR-CODE'].present?
#      create_geographic_boundary(row, "STR-CODE")
#    elsif row["BOX-CODE"].present?
#      create_geographic_boundary(row, "BOX-CODE")
#    end
#  end
#end

def create_city(row)  # create a new city record if no existing match is found
  GeographicBoundary.create(name: row["AREA"].titleize, geographic_boundary_type_id: @city.id)
end

def check_postal_range(cities,row,key)
  rc = ""
  re = 200  # define the range extesion for the postal_code check. Note its +- the extension
  cities.each do |id|
    c = GeographicBoundary.find(id)             # get the record with city id that is in the right province
    range = c.children.map(&:name).minmax       # get the range of postal_codes associated witht the city
    if ((range[0].to_i-re)..(range[1].to_i+re)).include?(row[key].to_i)    # check +- 30 if the postal_code is included in the range
      return rc = c                                                        # if in range assign the record to rc and return from function
    end
  end
  return rc || ""
end

def determine_city(row,key,province)  # determine if a city record exists that matches the parent province and postal_code range
  cities = GeographicBoundary.where(name: row['AREA'].titleize).where(geographic_boundary_type_id: @city.id)  # find all cities by name
  if cities.present?
    prov_cities = []
    cities.each do |city|                                               # loop over cities being found by name and type
      prov_cities.push(city.id) if city.parent.name == province.name    # and check if they are located the correct province
    end
    if prov_cities.length > 0
      # loop over cities from the correct province and check if they are in the same postal_code vicinity
      rc = check_postal_range(prov_cities,row,key)

      if rc.present?
        return rc
      else
        return city = create_city(row)
      end
    else
      # if no city of the name in the correct province is found, create one.
      return city = create_city(row)
    end
  else
    # in case no match is found create a new city
    return city = create_city(row)
  end
end

def create_geographic_boundary_sql(row, key) # cannot use SQL statement unless there is a postgres create_or_find...

  za_province = GeographicBoundary.find(return_province(row[key]))
  city = determine_city(row,key,za_province)
  za_province.children << city
  code = GeographicBoundary.where(name: row["#{key}"]).where(abbreviation: row["#{key}"]).where(geographic_boundary_type_id: @postal_code.id).first_or_create
  city.children << code
  suburb = GeographicBoundary.where(name: row['SUBURB'].titleize).where(geographic_boundary_type_id: @suburb.id).first_or_create
  code.children << suburb
  puts "#{za_province.name}>#{city.name}>#{code.name}>#{suburb.name}"

end


# parse the input csv file and generate geographic_boundaries according to each rows content
# in case of missing city use the suburbs name for city entry
@count = 0
def parse_postal_csv_with_sql(csv_file)
  CSV.foreach(csv_file, headers: true, :col_sep => ";") do |row|
    @count += 1
    if row['BOX-CODE'].present? &&  row['STR-CODE'].present?
      if row['BOX-CODE'] ==  row['STR-CODE']
        create_geographic_boundary_sql(row, "BOX-CODE")
      else
        create_geographic_boundary_sql(row, "STR-CODE")
        create_geographic_boundary_sql(row, "BOX-CODE")
      end
    elsif row['STR-CODE'].present?
      create_geographic_boundary_sql(row, "STR-CODE")
    elsif row["BOX-CODE"].present?
      create_geographic_boundary_sql(row, "BOX-CODE")
    end
  end
end

# had to split into two files due to unexpected failures (memory leak?)
# TODO in case of other situations like this we would have to create a raw SQL statement and then call the DB directly for inserting the data

input_csv_file="#{Rails.root}/db/support_files/postcodes_clean_all.csv"
parse_postal_csv_with_sql(input_csv_file)

# make sure the GeographicBoundaryAssociationType is set to "Postal Association" on all records
GeographicBoundaryAssociation.update_all(:geographic_boundary_association_type_id => paa.id)
#
##create HealthCareCategories
#HealthCareCategory.create(name: "Healthcare Category 1")
#HealthCareCategory.create(name: "Healthcare Category 2")
#HealthCareCategory.create(name: "Healthcare Category 3")
#
# create Marital Status Types
MaritalStatusType.create(name: "Single")
MaritalStatusType.create(name: "Married")
MaritalStatusType.create(name: "Divorced")
MaritalStatusType.create(name: "Widowed")
MaritalStatusType.create(name: "Unknown")

## create Medical Conditions Types
#MedicalConditionType.create(name: "HIV")
#MedicalConditionType.create(name: "Noonan")
#MedicalConditionType.create(name: "Tay-Sachs")
#MedicalConditionType.create(name: "Sickle Cell Syndrome")
#
##create person_relationship_types
#PersonRelationshipType.create(name: "patient - individual healthcare provider relationship")
#PersonRelationshipType.create(name: "provider - hospital relationship")
#PersonRelationshipType.create(name: "family relationship")
#PersonRelationshipType.create(name: "doctor - practice membership")
#PersonRelationshipType.create(name: "doctor - clinic membership")
#PersonRelationshipType.create(name: "doctor - hospital membership")
#PersonRelationshipType.create(name: "clinic - hospital association")
#PersonRelationshipType.create(name: "practice - hospital association")
#PersonRelationshipType.create(name: "contact for association")
#
##create  Person Role Status Types
#PersonRoleStatusType.create(name: "Person Role Status Type 1")
#PersonRoleStatusType.create(name: "Person Role Status Type 2")
#PersonRoleStatusType.create(name: "Person Role Status Type 3")
#
#create  Person Role Types
person = RoleType.create(name: I18n.t('person_role_type.person_role'), parent_id: nil)
organization = RoleType.create(name: I18n.t('person_role_type.organization_role'), parent_id: nil)
RoleType.create(name: "Patient", parent_id: person.id)
RoleType.create(name: "Individual Healthcare Practitioner", parent_id: person.id)
RoleType.create(name: "Contact", parent_id: person.id)
RoleType.create(name: "Parent", parent_id: person.id)
RoleType.create(name: "V.I.P", parent_id: person.id)
RoleType.create(name: "Healthcare Practice", parent_id: organization.id)
RoleType.create(name: "Healthcare Clinic", parent_id: organization.id)
RoleType.create(name: "Healthcare Hospital", parent_id: organization.id)


# create person_status_types
PersonStatusType.create(name: "valid")
PersonStatusType.create(name: "invalid")
PersonStatusType.create(name: "deceased")
PersonStatusType.create(name: "alive")

#create PhysicalCharacteristicsTypes
PhysicalCharacteristicType.create(name: "Weight")
PhysicalCharacteristicType.create(name: "Height")
PhysicalCharacteristicType.create(name: "Eye Color")
PhysicalCharacteristicType.create(name: "Hair Color")
PhysicalCharacteristicType.create(name: "Any Other")

#create Training Class Types
TrainingClassType.create(name: "Sequencer Hardware")
TrainingClassType.create(name: "Sequencer Software")
TrainingClassType.create(name: "Data Governance")
TrainingClassType.create(name: "Data Security")
TrainingClassType.create(name: "Password Security")
TrainingClassType.create(name: "Laboratory Workflows")

##create SanctionTypes
#SanctionType.create(name: "License expired")
#SanctionType.create(name: "License revoked")
#SanctionType.create(name: "Outstanding bills")
#
# create units of measure
UnitOfMeasure.create(name: "kilogram", abbreviation: "kg", description: "mass or weight")
UnitOfMeasure.create(name: "meter", abbreviation: "m", description: "distance or length")
UnitOfMeasure.create(name: "second", abbreviation: "s", description: "time")
UnitOfMeasure.create(name: "minute", abbreviation: "min", description: "time")
UnitOfMeasure.create(name: "hour", abbreviation: "h", description: "time")
UnitOfMeasure.create(name: "day", abbreviation: "d", description: "time")
UnitOfMeasure.create(name: "year", abbreviation: "yr", description: "time")
UnitOfMeasure.create(name: "pascal", abbreviation: "Pa", description: "pressure")
UnitOfMeasure.create(name: "joule", abbreviation: "J", description: "energy")
UnitOfMeasure.create(name: "joule", abbreviation: "C", description: "temperature")
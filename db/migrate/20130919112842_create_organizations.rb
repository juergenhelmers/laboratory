class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :current_name

      t.timestamps
    end
  end
end

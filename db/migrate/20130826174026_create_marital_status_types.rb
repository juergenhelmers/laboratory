class CreateMaritalStatusTypes < ActiveRecord::Migration
  def change
    create_table :marital_status_types do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

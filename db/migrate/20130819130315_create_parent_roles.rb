class CreateParentRoles < ActiveRecord::Migration
  def change
    create_table :parent_roles do |t|
      t.integer :person_id

      t.timestamps
    end
  end
end

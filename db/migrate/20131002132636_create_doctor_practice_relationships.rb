class CreateDoctorPracticeRelationships < ActiveRecord::Migration
  def change
    create_table :doctor_practice_relationships do |t|
      t.integer :doctor_role_id
      t.integer :practice_role_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

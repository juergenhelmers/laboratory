class CreatePersonStatusTypes < ActiveRecord::Migration
  def change
    create_table :person_status_types do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

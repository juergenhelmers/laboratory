class CreateDoctorRoles < ActiveRecord::Migration
  def change
    create_table :doctor_roles do |t|
      t.string :medical_id
      t.integer :person_id

      t.timestamps
    end
  end
end

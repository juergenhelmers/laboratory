class CreatePostalAddresses < ActiveRecord::Migration
  def change
    create_table :postal_addresses do |t|
      t.string :address1
      t.string :address2
      t.string :address3
      t.float :longitude
      t.float :laditude

      t.timestamps
    end
  end
end

class CreatePracticeRoles < ActiveRecord::Migration
  def change
    create_table :practice_roles do |t|
      t.integer :organization_id

      t.timestamps
    end
  end
end

class CreateTrainingClassTypes < ActiveRecord::Migration
  def change
    create_table :training_class_types do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

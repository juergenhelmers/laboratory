class CreatePersonContactMechanismPurposeTelecommunications < ActiveRecord::Migration
  def change
    create_table :person_contact_mechanism_purpose_telecommunications do |t|
      t.integer :person_contact_mechanism_telecommunication_id
      t.integer :contact_mechanism_purpose_type_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

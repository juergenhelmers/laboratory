class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string  :current_first_name
      t.string  :current_middle_name
      t.string  :current_last_name
      t.string  :current_personal_title
      t.string  :current_suffix
      t.string  :current_nickname
      t.integer :gender_type_id
      t.date    :birth_date
      t.string  :mothers_maiden_name
      t.string  :passport_nationality
      t.string  :citizen_number
      t.string  :ethnicity
      t.text    :comment

      t.timestamps
    end
  end
end

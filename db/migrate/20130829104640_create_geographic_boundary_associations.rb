class CreateGeographicBoundaryAssociations < ActiveRecord::Migration
  def change
    create_table :geographic_boundary_associations do |t|
      t.integer :parent_id
      t.integer :child_id
      t.integer :geographic_boundary_association_type_id

      t.timestamps
    end
  end
end

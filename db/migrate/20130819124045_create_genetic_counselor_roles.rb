class CreateGeneticCounselorRoles < ActiveRecord::Migration
  def change
    create_table :genetic_counselor_roles do |t|
      t.integer :person_id

      t.timestamps
    end
  end
end

class CreatePatientNurseRelationships < ActiveRecord::Migration
  def change
    create_table :patient_nurse_relationships do |t|
      t.integer :patient_role_id
      t.integer :nurse_role_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

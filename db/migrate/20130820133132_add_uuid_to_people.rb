class AddUuidToPeople < ActiveRecord::Migration
  def change
    add_column :people, :uuid, :string
  end
end

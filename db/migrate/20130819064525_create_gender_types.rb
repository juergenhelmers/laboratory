class CreateGenderTypes < ActiveRecord::Migration
  def change
    create_table :gender_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end

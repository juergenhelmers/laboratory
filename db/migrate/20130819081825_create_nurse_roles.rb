class CreateNurseRoles < ActiveRecord::Migration
  def change
    create_table :nurse_roles do |t|
      t.string :medical_id
      t.integer :person_id

      t.timestamps
    end
  end
end

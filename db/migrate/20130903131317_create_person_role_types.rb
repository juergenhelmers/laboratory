class CreatePersonRoleTypes < ActiveRecord::Migration
  def change
    create_table :person_role_types do |t|
      t.string :name
      t.text :description
      t.integer :parent_id

      t.timestamps
    end
  end
end

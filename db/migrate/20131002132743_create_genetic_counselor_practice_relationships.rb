class CreateGeneticCounselorPracticeRelationships < ActiveRecord::Migration
  def change
    create_table :genetic_counselor_practice_relationships do |t|
      t.integer :genetic_counselor_role_id
      t.integer :practice_role_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

class CreateUnitOfMeasures < ActiveRecord::Migration
  def change
    create_table :unit_of_measures do |t|
      t.string :name
      t.text :description
      t.string :abbreviation

      t.timestamps
    end
  end
end

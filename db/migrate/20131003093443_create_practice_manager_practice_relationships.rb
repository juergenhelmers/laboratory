class CreatePracticeManagerPracticeRelationships < ActiveRecord::Migration
  def change
    create_table :practice_manager_practice_relationships do |t|
      t.integer :practice_manager_role_id
      t.integer :practice_role_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

class CreateGeographicBoundaryTypes < ActiveRecord::Migration
  def change
    create_table :geographic_boundary_types do |t|
      t.string :name
      t.text :description
      t.boolean :is_code

      t.timestamps
    end
  end
end

class AddApplicationUserRoleIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :application_user_role_id, :integer
  end
end

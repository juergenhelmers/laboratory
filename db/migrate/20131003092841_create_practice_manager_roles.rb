class CreatePracticeManagerRoles < ActiveRecord::Migration
  def change
    create_table :practice_manager_roles do |t|
      t.integer :person_id

      t.timestamps
    end
  end
end

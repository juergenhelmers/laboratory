class CreateDoctorContactRelationships < ActiveRecord::Migration
  def change
    create_table :doctor_contact_relationships do |t|
      t.integer :contact_role_id
      t.integer :doctor_role_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

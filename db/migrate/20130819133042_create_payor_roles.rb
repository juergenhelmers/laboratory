class CreatePayorRoles < ActiveRecord::Migration
  def change
    create_table :payor_roles do |t|
      t.integer :person_id

      t.timestamps
    end
  end
end

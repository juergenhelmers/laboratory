class CreateNursePracticeRelationships < ActiveRecord::Migration
  def change
    create_table :nurse_practice_relationships do |t|
      t.integer :nurse_role_id
      t.integer :practice_role_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

class CreatePatientDoctorRelationships < ActiveRecord::Migration
  def change
    create_table :patient_doctor_relationships do |t|
      t.integer :patient_role_id
      t.integer :doctor_role_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

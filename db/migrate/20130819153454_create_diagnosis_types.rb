class CreateDiagnosisTypes < ActiveRecord::Migration
  def change
    create_table :diagnosis_types do |t|
      t.string :name
      t.text :description
      t.string :code

      t.timestamps
    end
  end
end

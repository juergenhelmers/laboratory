class CreateContactMechanismTelecommunications < ActiveRecord::Migration
  def change
    create_table :contact_mechanism_telecommunications do |t|
      t.integer :contact_mechanism_type_id

      t.timestamps
    end
  end
end

class CreateContactRoles < ActiveRecord::Migration
  def change
    create_table :contact_roles do |t|
      t.integer :person_id

      t.timestamps
    end
  end
end

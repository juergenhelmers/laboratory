class CreatePersonTrainings < ActiveRecord::Migration
  def change
    create_table :person_trainings do |t|
      t.integer :person_id
      t.integer :training_class_type_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

class CreateParentChildRelationships < ActiveRecord::Migration
  def change
    create_table :parent_child_relationships do |t|
      t.integer :parent_role_id
      t.integer :child_role_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

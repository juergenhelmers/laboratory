class CreatePersonLanguages < ActiveRecord::Migration
  def change
    create_table :person_languages do |t|
      t.integer :person_id
      t.integer :language_id
      t.date :from_date
      t.date :thru_date
      t.string :competency_level

      t.timestamps
    end
  end
end

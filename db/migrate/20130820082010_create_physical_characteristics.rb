class CreatePhysicalCharacteristics < ActiveRecord::Migration
  def change
    create_table :physical_characteristics do |t|
      t.integer :patient_role_id
      t.integer :physical_characteristic_type_id
      t.integer :unit_of_measure_id
      t.date    :from_date
      t.date    :thru_date
      t.string  :value
      t.integer :person_id

      t.timestamps
    end
  end
end

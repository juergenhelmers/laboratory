class CreateChildRoles < ActiveRecord::Migration
  def change
    create_table :child_roles do |t|
      t.integer :person_id

      t.timestamps
    end
  end
end

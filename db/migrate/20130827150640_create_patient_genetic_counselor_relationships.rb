class CreatePatientGeneticCounselorRelationships < ActiveRecord::Migration
  def change
    create_table :patient_genetic_counselor_relationships do |t|
      t.integer :patient_role_id
      t.integer :genetic_counselor_role_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

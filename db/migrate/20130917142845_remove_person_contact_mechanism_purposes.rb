class RemovePersonContactMechanismPurposes < ActiveRecord::Migration
  def change
    drop_table :person_contact_mechanism_purposes if self.table_exists?("person_contact_mechanism_purposes")
  end
end

class CreateGeographicBoundaryAssociationTypes < ActiveRecord::Migration
  def change
    create_table :geographic_boundary_association_types do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

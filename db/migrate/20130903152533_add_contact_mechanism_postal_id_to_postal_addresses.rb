class AddContactMechanismPostalIdToPostalAddresses < ActiveRecord::Migration
  def change
    add_column :postal_addresses, :contact_mechanism_postal_id, :integer
  end
end

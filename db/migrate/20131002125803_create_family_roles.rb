class CreateFamilyRoles < ActiveRecord::Migration
  def change
    create_table :family_roles do |t|
      t.integer :organization_id

      t.timestamps
    end
  end
end

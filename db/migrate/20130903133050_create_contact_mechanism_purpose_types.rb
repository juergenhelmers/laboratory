class CreateContactMechanismPurposeTypes < ActiveRecord::Migration
  def change
    create_table :contact_mechanism_purpose_types do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

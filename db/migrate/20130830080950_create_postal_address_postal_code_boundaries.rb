class CreatePostalAddressPostalCodeBoundaries < ActiveRecord::Migration
  def change
    create_table :postal_address_postal_code_boundaries do |t|
      t.integer :postal_address_id
      t.integer :geographic_boundary_id

      t.timestamps
    end
  end
end

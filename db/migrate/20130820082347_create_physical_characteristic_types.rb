class CreatePhysicalCharacteristicTypes < ActiveRecord::Migration
  def change
    create_table :physical_characteristic_types do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

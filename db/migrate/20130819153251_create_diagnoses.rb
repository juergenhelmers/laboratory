class CreateDiagnoses < ActiveRecord::Migration
  def change
    create_table :diagnoses do |t|
      t.integer :patient_role_id
      t.integer :diagnosis_type_id
      t.date    :diagnosis_date
      t.integer :health_care_episode_id

      t.timestamps
    end
  end
end

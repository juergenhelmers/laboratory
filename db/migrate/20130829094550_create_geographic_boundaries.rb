class CreateGeographicBoundaries < ActiveRecord::Migration
  def change
    create_table :geographic_boundaries do |t|
      t.string :name
      t.string :geo_code
      t.string :abbreviation
      t.integer :geographic_boundary_type_id

      t.timestamps
    end
  end
end

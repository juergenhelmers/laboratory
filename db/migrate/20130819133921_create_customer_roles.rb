class CreateCustomerRoles < ActiveRecord::Migration
  def change
    create_table :customer_roles do |t|
      t.integer :person_id

      t.timestamps
    end
  end
end

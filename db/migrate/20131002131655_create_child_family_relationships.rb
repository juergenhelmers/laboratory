class CreateChildFamilyRelationships < ActiveRecord::Migration
  def change
    create_table :child_family_relationships do |t|
      t.integer :child_role_id
      t.integer :family_role_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

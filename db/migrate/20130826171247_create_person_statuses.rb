class CreatePersonStatuses < ActiveRecord::Migration
  def change
    create_table :person_statuses do |t|
      t.integer :person_id
      t.integer :person_status_type_id
      t.date :person_status_date

      t.timestamps
    end
  end
end

class CreatePersonMaritalStatuses < ActiveRecord::Migration
  def change
    create_table :person_marital_statuses do |t|
      t.integer :person_id
      t.integer :marital_status_type_id
      t.integer :marital_status_seq_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

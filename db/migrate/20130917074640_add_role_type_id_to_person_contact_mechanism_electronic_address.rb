class AddRoleTypeIdToPersonContactMechanismElectronicAddress < ActiveRecord::Migration
  def change
    remove_column :person_contact_mechanism_electronic_addresses, :person_role_type_id
    remove_column :person_contact_mechanism_postals, :person_role_type_id
    remove_column :person_contact_mechanism_telecommunications, :person_role_type_id


    add_column :person_contact_mechanism_electronic_addresses, :role_type_id, :integer
    add_column :person_contact_mechanism_postals, :role_type_id, :integer
    add_column :person_contact_mechanism_telecommunications, :role_type_id, :integer
  end
end

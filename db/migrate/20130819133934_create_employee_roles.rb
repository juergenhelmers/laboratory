class CreateEmployeeRoles < ActiveRecord::Migration
  def change
    create_table :employee_roles do |t|
      t.integer :person_id

      t.timestamps
    end
  end
end

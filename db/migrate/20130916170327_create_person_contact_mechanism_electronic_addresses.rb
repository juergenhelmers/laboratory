class CreatePersonContactMechanismElectronicAddresses < ActiveRecord::Migration
  def change
    create_table :person_contact_mechanism_electronic_addresses do |t|
      t.integer :person_id
      t.integer :contact_mechanism_electronic_address_id
      t.integer :person_role_type_id
      t.date :from_date
      t.date :thru_date
      t.boolean :non_solicitation_ind
      t.text :comment

      t.timestamps
    end
  end
end

class CreatePersonContactMechanismPurposePostals < ActiveRecord::Migration
  def change
    create_table :person_contact_mechanism_purpose_postals do |t|
      t.integer :person_contact_mechanism_postal_id
      t.integer :contact_mechanism_purpose_type_id
      t.date :from_date
      t.date :thru_date

      t.timestamps
    end
  end
end

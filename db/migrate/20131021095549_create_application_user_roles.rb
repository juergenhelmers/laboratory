class CreateApplicationUserRoles < ActiveRecord::Migration
  def change
    create_table :application_user_roles do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end

class CreatePatientRoles < ActiveRecord::Migration
  def change
    create_table :patient_roles do |t|
      t.integer :person_id

      t.timestamps
    end
  end
end

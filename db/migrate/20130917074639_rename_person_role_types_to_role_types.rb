class RenamePersonRoleTypesToRoleTypes < ActiveRecord::Migration
  def change
    rename_table :person_role_types, :role_types
  end
end

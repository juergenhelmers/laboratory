class CreateTelecommunicationsNumbers < ActiveRecord::Migration
  def change
    create_table :telecommunications_numbers do |t|
      t.string :country_code
      t.string :area_code
      t.string :contact_number
      t.integer :contact_mechanism_telecommunication_id

      t.timestamps
    end
  end
end

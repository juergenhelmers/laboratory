Laboratory::Application.routes.draw do

  resources :application_user_roles

  devise_for :users

  namespace :admin do
    resources :users
  end

  resources :organizations

  resources :contact_mechanism_purpose_types

  resources :role_types

  resources :contact_mechanism_types

  resources :postal_addresses

  resources :geographic_boundary_association_types

  resources :geographic_boundaries do
    collection do
      get :countries
      get :postal_codes
      get :cities
      get :suburbs
    end
  end

  resources :electronic_addresses, :only => [:index]

  resources :geographic_boundary_types

  resources :training_class_types

  resources :languages

  resources :marital_status_types

  resources :person_status_types

  resources :categories

  resources :category_types

  resources :physical_characteristic_types

  resources :unit_of_measures

  resources :diagnosis_types

  resources :patient_roles

  resources :employee_roles

  resources :customer_roles

  resources :payor_roles

  resources :contact_roles

  resources :child_roles

  resources :parent_roles

  resources :genetic_counselor_roles

  resources :doctor_roles

  resources :nurse_roles

  resources :gender_types

  resources :genders

  resources :people

  get "welcome/index"     # generate the route for the welcome page
  root 'welcome#index'    # make the welcome page the root of the application "/"

end

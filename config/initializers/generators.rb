Rails.application.config.generators do |g|
  g.template_engine :haml
  g.stylesheets false
  g.test_framework :rspec,
      :fixture => true,
      :views => false,
      :helper_specs => false,
      :routing_specs => false,
      :controller_specs => true,
      :request_specs => true,
      :view_specs => false
  g.fixture_replacement :factory_girl, :dir => "spec/factories"
end
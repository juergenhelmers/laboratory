source 'https://rubygems.org'

gem 'rails', '4.0.2'              # rails version used in this application

gem 'pg'                          # Use postgresql as the database for Active Record
gem 'haml'                        # use haml for view generation
gem 'haml-rails'
gem 'sass-rails', '~> 4.0.0.rc1'  # Use SCSS for stylesheets
gem 'bootstrap-sass'              # Use Twitter-Bootstrap
                                  # use the latest simple_form code to get better twitter-bootstrap integration
gem 'simple_form', :git => 'git://github.com/plataformatec/simple_form.git'
gem 'country_select'              # adds an automated country selector to simple_form
gem 'cocoon', '~> 1.2.0'          # cocoon for dynamically add nested fields
gem 'uglifier', '>= 1.3.0'        # Use Uglifier as compressor for JavaScript assets
gem 'coffee-rails', '~> 4.0.0'    # Use CoffeeScript for .js.coffee assets and views
gem 'jquery-rails'                # Use jquery as the JavaScript library
#gem 'turbolinks'                  # Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'jbuilder', '~> 1.0.1'        # Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'bcrypt-ruby', '~> 3.0.0'   # Use ActiveModel has_secure_password
# gem 'unicorn'                   # Use unicorn as the app server
gem 'will_paginate', '~> 3.0'     # use will_paginate for pagination of index views
gem 'uuidtools'                   # to add a Universal Unique IDentifier to party
gem 'draper', '~> 1.0'            # Draper adds an object-oriented layer of presentation logic to your Rails application.
gem 'rqrcode_png'                 # Generate your own QR code images
gem 'devise', '~> 3.1'            # Devise is a flexible authentication solution for Rails based on Warden
gem "cancan", '~> 1.6'            # An authorization library for Ruby on Rails which restricts what resources a given user is allowed to access

group :development, :test do
  gem 'quiet_assets'
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'html2haml'
end

group :test do
  gem 'faker'
  gem 'capybara'
  gem 'selenium-webdriver'
  gem 'poltergeist'
  gem 'guard-rspec', '~> 2.5.0'
  gem 'guard-cucumber'
  gem 'cucumber-rails', :require => false
  gem 'guard-spork', '~> 1.5.0'
  gem 'launchy'
  gem 'rb-fsevent',   :require => false
  gem 'database_cleaner'
  gem 'rb-inotify'
  gem 'spork-rails', :github => 'sporkrb/spork-rails'
end

group :doc do
  gem 'sdoc', require: false      # bundle exec rake doc:rails generates the API under doc/api.
end
